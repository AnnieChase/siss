﻿using System.Data.SqlClient;

namespace Database
{
    /// <summary>
    ///     Clase envoltoria para el manejo de las transacciones SQL.
    /// </summary>
    public class TransacciónSQL
    {
        /// <summary>
        ///     Transacción SQL que maneja los comandos SQL.
        /// </summary>
        private readonly SqlTransaction _sqlTransaction;

        /// <summary>
        ///     Constructor de TransacciónSQL. Crea una transacción SQL nueva.
        /// </summary>
        public TransacciónSQL()
        {
            _sqlTransaction = BaseDeDatos.Instancia.CrearSQLTransaction();
        }

        /// <summary>
        ///     Crea un comando SQL asignado a la transacción SQL actual.
        /// </summary>
        /// <param name="query">Consulta a realizar</param>
        /// <returns>Un comando SQL asignado a la transacción</returns>
        public ComandoSQL CrearComandoSQL(string query)
        {
            return new ComandoSQL(query, _sqlTransaction);
        }

        /// <summary>
        ///     Confirma la transacción a la base de datos.
        /// </summary>
        public void Confirmar()
        {
            _sqlTransaction.Commit();
        }

        /// <summary>
        ///     Revierte los cambios de la transacción actual.
        /// </summary>
        public void Revertir()
        {
            _sqlTransaction.Rollback();
        }
    }
}