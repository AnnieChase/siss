using System;
using System.Data;
using System.Data.SqlClient;

namespace Database
{
    /// <summary>
    ///     Clase envoltoria (wrapper) que maneja los commandos SQL.
    /// </summary>
    public class ComandoSQL
    {
        /// <summary>
        ///     Comando SQL que realiza las consultas e instrucciones sobre la base de datos.
        /// </summary>
        private SqlCommand _sqlCommand;

        /// <summary>
        ///     Constructor de ComandoSQL. Crea un comando SQL con una consulta.
        /// </summary>
        /// <param name="consulta">Consulta a realizar</param>
        public ComandoSQL(string consulta)
        {
            _sqlCommand = BaseDeDatos.Instancia.CrearSQLCommand(consulta);
        }

        /// <summary>
        ///     Constructor de ComandoSQL. Crea un comando SQL con una consulta, asignado a una transacción SQL.
        /// </summary>
        /// <param name="consulta">Consulta a realizar</param>
        /// <param name="sqlTransaction">Transacción SQL para asignar</param>
        internal ComandoSQL(string consulta, SqlTransaction sqlTransaction)
        {
            _sqlCommand = BaseDeDatos.Instancia.CrearSQLCommand(consulta, sqlTransaction);
        }

        /// <summary>
        ///     Agrega un parámetro al comando SQL.
        /// </summary>
        /// <param name="nombreParámetro">Nombre del parámetro</param>
        /// <param name="valor">Valor del parámetro</param>
        public void AgregarParámetro(string nombreParámetro, object valor)
        {
            _sqlCommand.Parameters.AddWithValue(nombreParámetro, valor);
        }

        /// <summary>
        ///     Devuelve el id del último elemento insertado.
        /// </summary>
        /// <returns>Id del último elemento insertado</returns>
        public int GetÚltimoIdInsertado()
        {
            _sqlCommand = BaseDeDatos.Instancia.CrearSQLCommand("SELECT @@IDENTITY", _sqlCommand.Connection,
                _sqlCommand.Transaction);
            return Convert.ToInt32(_sqlCommand.ExecuteScalar());
        }

        /// <summary>
        ///     Realiza una consulta a la base de datos y devuelve una tabla de datos correspondiente a la consulta.
        /// </summary>
        /// <returns>Tabla de datos correspondiente a la consulta</returns>
        public DataTable Consultar()
        {
            var sqLiteDataAdapter = new SqlDataAdapter(_sqlCommand);
            var dataTable = new DataTable();
            sqLiteDataAdapter.Fill(dataTable);
            return dataTable;
        }

        /// <summary>
        ///     Ejecuta una instrucción INSERT, UPDATE o DELETE en la base de datos.
        /// </summary>
        /// <returns>El número de filas afectadas por la instrucción</returns>
        /// <exception cref="SqlException"></exception>
        public int Ejecutar()
        {
            return _sqlCommand.ExecuteNonQuery();
        }
    }
}