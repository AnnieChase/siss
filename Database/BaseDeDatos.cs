﻿using System.Configuration;
using System.Data.SqlClient;

// ReSharper disable MemberCanBeMadeStatic.Global

namespace Database

{
    /// <summary>
    ///     La clase Database es un Singleton que se encarga de realizar la conexión SQL al servidor SQL Server
    ///     del SISS
    /// </summary>
    internal class BaseDeDatos
    {
        private BaseDeDatos()
        {
        }

        /// <summary>
        ///     Única instancia de la clase DB
        /// </summary>
        public static BaseDeDatos Instancia { get; } = new BaseDeDatos();

        /// <summary>
        ///     Devuelve la única conexión SQL de la base de datos SQL Server. La inicializa si es necesario.
        /// </summary>
        /// <returns>Conexión SQL de la base de datos SQL Server.</returns>
        private static SqlConnection CrearConexiónSQL()
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings["connectionString"];
            string connectionString;

            if (connectionStringSettings != null)
            {
                connectionString = connectionStringSettings.ConnectionString;
            }
            else
            {
                connectionString = "Server=localhost;Connection Timeout=1";
            }

            var sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            return sqlConnection;
        }

        public SqlCommand CrearSQLCommand(string query)
        {
            return new SqlCommand(query, CrearConexiónSQL());
        }

        public SqlCommand CrearSQLCommand(string query, SqlTransaction sqlTransaction)
        {
            return new SqlCommand(query, sqlTransaction.Connection, sqlTransaction);
        }

        public SqlCommand CrearSQLCommand(string query, SqlConnection sqlConnection, SqlTransaction sqlTransaction)
        {
            return new SqlCommand(query, sqlConnection, sqlTransaction);
        }

        public SqlTransaction CrearSQLTransaction()
        {
            return CrearConexiónSQL().BeginTransaction();
        }
    }
}