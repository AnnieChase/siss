﻿using System;
using System.Windows.Threading;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class VentanaPrincipal
    {
        public VentanaPrincipal()
        {
            AdministradorPáginas.RegistrarVentana(this);
            InitializeComponent();
            if (HayAlMenosUnDirectorRegistrado())
            {
                AdministradorPáginas.Mostrar(Página.INICIO_DE_SESIÓN);
            }
            else
            {
                AdministradorPáginas.Mostrar(Página.PRIMER_REGISTRAR_DIRECTOR);
            }

            var liveTime = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(10)};
            liveTime.Tick += OnTick;
            liveTime.Start();

            SourceInitialized += (s, e) =>
            {
                MinWidth = ActualWidth;
                MinHeight = ActualHeight;
            };
        }

        private void OnTick(object source, EventArgs e)
        {
            Title = strings.TítuloSistema + " - " + DateTime.Now.ToLongTimeString() + " - " + DateTime.Now.Millisecond;
        }

        private static bool HayAlMenosUnDirectorRegistrado()
        {
            var directorDao = new DirectorDAO();
            try
            {
                return directorDao.HayAlMenosUnDirectorActivo();
            }
            catch (BusinessLogicException)
            {
                return true;
            }
        }
    }
}