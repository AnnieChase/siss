﻿using System;
using System.Windows;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Controles
{
    /// <summary>
    ///     Lógica de interacción para Saludo.xaml
    /// </summary>
    public partial class Saludo
    {
        public Saludo()
        {
            InitializeComponent();
        }

        private void OnSaludoLoaded(object sender, RoutedEventArgs e)
        {
            SaludoLabel.Content = GetSaludo();
            NombreLabel.Content = Sesión.GetNombreCompletoUsuario();

            var tipoUsuario = Sesión.GetTipoUsuario();
            switch (tipoUsuario)
            {
                case TipoUsuario.Coordinador:
                    if (Sesión.GetUsuarioActual() is Coordinador coordinador)
                    {
                        var programaEducativo = coordinador.ProgramaEducativo;
                        TipoUsuarioLabel.Content =
                            strings.SaludoCoordinadorTipoUsuario.Replace("{0}", programaEducativo.ToString());
                    }

                    break;
                case TipoUsuario.Director:
                    TipoUsuarioLabel.Content = strings.DirectorFacultad;
                    break;
                case TipoUsuario.Estudiante:
                    if (Sesión.GetUsuarioActual() is Estudiante estudiante)
                    {
                        var programaEducativo = estudiante.ProgramaEducativo;
                        TipoUsuarioLabel.Content =
                            strings.SaludoEstudianteTipoUsuario.Replace("{0}", programaEducativo.ToString());
                    }

                    break;
                case TipoUsuario.TécnicoAcadémico:
                    if (Sesión.GetUsuarioActual() is TécnicoAcadémico técnicoAcadémico)
                    {
                        var programaEducativo = técnicoAcadémico.ProgramaEducativo;
                        TipoUsuarioLabel.Content =
                            strings.SaludoTécnicoAcadémicoTipoUsuario.Replace("{0}", programaEducativo.ToString());
                    }

                    break;
            }
        }

        private static string GetSaludo()
        {
            var hora = DateTime.Now.Hour;
            string saludo;
            if (hora >= 6 && hora < 12)
            {
                saludo = strings.BuenosDías;
            }
            else if (hora >= 12 && hora < 19)
            {
                saludo = strings.BuenasTardes;
            }
            else
            {
                saludo = strings.BuenasNoches;
            }

            return saludo;
        }
    }
}