﻿using System.Windows;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Controles
{
    /// <summary>
    ///     Lógica de interacción para Cabecera.xaml
    /// </summary>
    public partial class Cabecera
    {
        public static readonly DependencyProperty OcultarInformaciónUsuarioProperty =
            DependencyProperty.Register("OcultarInformaciónUsuario", typeof(bool), typeof(Cabecera),
                new PropertyMetadata(false));

        public static readonly DependencyProperty OcultarBotónInicioProperty =
            DependencyProperty.Register("OcultarBotónInicio", typeof(bool), typeof(Cabecera),
                new PropertyMetadata(false));

        public Cabecera()
        {
            InitializeComponent();
        }

        public bool OcultarInformaciónUsuario
        {
            private get => (bool) GetValue(OcultarInformaciónUsuarioProperty);
            set => SetValue(OcultarInformaciónUsuarioProperty, value);
        }

        public bool OcultarBotónInicio
        {
            private get => (bool) GetValue(OcultarBotónInicioProperty);
            set => SetValue(OcultarBotónInicioProperty, value);
        }

        private void OnCabeceraLoad(object sender, RoutedEventArgs e)
        {
            if (OcultarInformaciónUsuario || !Sesión.HayUnUsuarioLogeado())
            {
                InformaciónUsuarioGrid.Visibility = Visibility.Hidden;
            }
            else
            {
                InformaciónUsuarioGrid.Visibility = Visibility.Visible;
                TipoUsuarioLabel.Content = Sesión.GetTipoUsuario() + ":";
                NombreUsuarioLabel.Content = Sesión.GetNombreCompletoUsuario();
                if (OcultarBotónInicio)
                {
                    InicioButton.Visibility = Visibility.Hidden;
                }
            }
        }

        private void OnInicioButtonClick(object sender, RoutedEventArgs e)
        {
            var tipoUsuario = Sesión.GetTipoUsuario();
            switch (tipoUsuario)
            {
                case TipoUsuario.Coordinador:
                    AdministradorPáginas.Mostrar(Página.INICIO_COORDINADOR);
                    break;
                case TipoUsuario.Director:
                    AdministradorPáginas.Mostrar(Página.INICIO_DIRECTOR);
                    break;
                case TipoUsuario.Estudiante:
                    AdministradorPáginas.Mostrar(Página.INICIO_ESTUDIANTE);
                    break;
                case TipoUsuario.TécnicoAcadémico:
                    AdministradorPáginas.Mostrar(Página.INICIO_TÉCNICO_ACADÉMICO);
                    break;
            }
        }

        private void OnModificaPerfilButtonClick(object sender, RoutedEventArgs e)
        {
            var tipoUsuario = Sesión.GetTipoUsuario();
            switch (tipoUsuario)
            {
                case TipoUsuario.Coordinador:
                    if (Sesión.GetUsuarioActual() is Coordinador coordinador)
                    {
                        var idCoordinador = coordinador.IdCoordinador;
                        var registroCoordinador = new RegistroCoordinador(idCoordinador);
                        AdministradorPáginas.Mostrar(Página.REGISTRAR_COORDINADOR, registroCoordinador);
                    }

                    break;
                case TipoUsuario.Director:
                    if (Sesión.GetUsuarioActual() is Director director)
                    {
                        var idDirector = director.IdDirector;
                        var registroDirector = new RegistroDirector(idDirector);
                        AdministradorPáginas.Mostrar(Página.REGISTRAR_DIRECTOR, registroDirector);
                    }

                    break;
                case TipoUsuario.Estudiante:
                    if (Sesión.GetUsuarioActual() is Estudiante estudiante)
                    {
                        var idEstudiante = estudiante.IdEstudiante;
                        var registroEstudiante = new RegistroEstudiante(idEstudiante);
                        AdministradorPáginas.Mostrar(Página.REGISTRAR_ESTUDIANTE, registroEstudiante);
                    }

                    break;
                case TipoUsuario.TécnicoAcadémico:
                    if (Sesión.GetUsuarioActual() is TécnicoAcadémico técnicoAcadémico)
                    {
                        var idTécnicoAcadémico = técnicoAcadémico.IdTécnicoAcadémico;
                        var registroTécnicoAcadémico = new RegistroTécnicoAcadémico(idTécnicoAcadémico);
                        AdministradorPáginas.Mostrar(Página.REGISTRAR_TÉCNICO_ACADÉMICO, registroTécnicoAcadémico);
                    }

                    break;
            }
        }

        private void OnCerrarSesiónButtonClick(object sender, RoutedEventArgs e)
        {
            var option = MessageBox.Show(strings.PreguntaCierreSesión,
                strings.ConfirmaciónDeCierreSesión, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (option == MessageBoxResult.Yes)
            {
                Sesión.CerrarSesión();
                AdministradorPáginas.Mostrar(Página.INICIO_DE_SESIÓN);
            }
        }
    }
}