using System.Collections;
using System.Windows;
using System.Windows.Controls;
using InterfazGráfica.PantallaInicios;
using InterfazGráfica.Pantallas.Inicio;
using InterfazGráfica.Pantallas.Listas;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Recursos.Localización;

namespace InterfazGráfica.Utilidad
{
    public static class AdministradorPáginas
    {
        private static Window _window;
        private static Stack _backstack;
        private static Página? _páginaActual;

        public static void RegistrarVentana(Window window)
        {
            _window = window;
            _backstack = new Stack();
        }

        // ReSharper disable once CyclomaticComplexity
        public static void Mostrar(Página páginaNueva, bool agregarPáginaActualAlHistorial = true)
        {
            Page page;
            switch (páginaNueva)
            {
                case Página.INICIO_DE_SESIÓN:
                    page = new InicioDeSesión();
                    LimpiarHistorial();
                    break;
                case Página.INICIO_DIRECTOR:
                    page = new InicioDirector();
                    break;
                case Página.INICIO_COORDINADOR:
                    page = new InicioCoordinador();
                    break;
                case Página.INICIO_TÉCNICO_ACADÉMICO:
                    page = new InicioTécnicoAcadémico();
                    break;
                case Página.INICIO_ESTUDIANTE:
                    page = new InicioEstudiante();
                    break;
                case Página.LISTA_DIRECTORES:
                    page = new ListaDirectores();
                    break;
                case Página.LISTA_COORDINADORES:
                    page = new ListaCoordinador();
                    break;
                case Página.LISTA_TÉCNICOS_ACADÉMICOS:
                    page = new ListaTécnicoAcadémico();
                    break;
                case Página.LISTA_ESTUDIANTES:
                    page = new ListaEstudiantes();
                    break;
                case Página.LISTA_ORGANIZACIONES:
                    page = new ListaOrganizaciones();
                    break;
                case Página.LISTA_ENCARGADOS:
                    page = new ListaEncargados();
                    break;
                case Página.LISTA_PROYECTOS:
                    page = new ListaProyectos();
                    break;
                case Página.REGISTRAR_DIRECTOR:
                    page = new RegistroDirector();
                    break;
                case Página.PRIMER_REGISTRAR_DIRECTOR:
                    var registrarDirector = new RegistroDirector();
                    registrarDirector.SetModoPrimerRegistroDirector();
                    page = registrarDirector;
                    break;
                case Página.REGISTRAR_COORDINADOR:
                    page = new RegistroCoordinador();
                    break;
                case Página.REGISTRAR_TÉCNICO_ACADÉMICO:
                    page = new RegistroTécnicoAcadémico();
                    break;
                case Página.REGISTRAR_ESTUDIANTE:
                    page = new RegistroEstudiante();
                    break;
                case Página.REGISTRAR_ORGANIZACIÓN:
                    page = new RegistroOrganización();
                    break;
                case Página.REGISTRAR_ENCARGADO:
                    page = new RegistroEncargado();
                    break;
                case Página.REGISTRAR_PROYECTO:
                    page = new RegistroProyecto();
                    break;
                default:
                    MessageBox.Show(strings.ErrorPáginaNoImplementada.Replace("{0}", páginaNueva.ToString()),
                        strings.PáginaNoImplementada,
                        MessageBoxButton.OK);
                    return;
            }

            if (agregarPáginaActualAlHistorial)
            {
                AgregarPáginaActualAHistorial();
            }

            CambiarPáginaDeVentana(page);
            _páginaActual = páginaNueva;
        }

        public static void Mostrar(Página páginaNueva, Page página)
        {
            AgregarPáginaActualAHistorial();
            CambiarPáginaDeVentana(página);
            _páginaActual = páginaNueva;
        }

        public static void Regresar()
        {
            var página = (Página) _backstack.Pop();
            Mostrar(página, false);
        }

        private static void AgregarPáginaActualAHistorial()
        {
            if (_páginaActual != null)
            {
                _backstack.Push(_páginaActual);
            }
        }

        private static void CambiarPáginaDeVentana(Page página)
        {
            if (_window != null)
            {
                _window.Content = página;
            }
        }

        private static void LimpiarHistorial()
        {
            _páginaActual = null;
            _backstack.Clear();
        }
    }
}