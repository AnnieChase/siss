using InterfazGráfica.Recursos.Localización;

namespace InterfazGráfica.Utilidad
{
    public static class ValidadorCampos
    {
        public static string ValidarCamposUsuario(bool modoEditarRegistro, string usuarioText, string contraseñaText,
            string repetirContraseñaText)
        {
            string error = null;

            if (string.IsNullOrEmpty(usuarioText))
            {
                error = strings.ErrorUsuarioVacío;
            }
            else
            {
                if (modoEditarRegistro)
                {
                    if (!string.IsNullOrEmpty(contraseñaText) && contraseñaText != repetirContraseñaText)
                    {
                        error = strings.ErrorNuevasContraseñasNoCoinciden;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(contraseñaText))
                    {
                        error = strings.ErrorContraseñaVacía;
                    }

                    if (string.IsNullOrEmpty(repetirContraseñaText))
                    {
                        error = strings.ErrorRepetirContraseñaVacía;
                    }

                    if (contraseñaText != repetirContraseñaText)
                    {
                        error = strings.ErrorContraseñasNoCoinciden;
                    }
                }
            }

            return error;
        }
    }
}