using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace InterfazGráfica.Utilidad
{
    public static class Sesión
    {
        private static Persona _usuarioActual;
        private static TipoUsuario _tipoUsuario;

        public static void CrearNuevaSesión(Persona persona, TipoUsuario tipoUsuario)
        {
            _usuarioActual = persona;
            _tipoUsuario = tipoUsuario;
        }

        public static Persona GetUsuarioActual()
        {
            return _usuarioActual;
        }

        public static TipoUsuario GetTipoUsuario()
        {
            return _tipoUsuario;
        }

        public static string GetNombreCompletoUsuario()
        {
            return _usuarioActual.GetNombreCompleto();
        }

        public static void CerrarSesión()
        {
            _usuarioActual = null;
        }

        public static bool HayUnUsuarioLogeado()
        {
            return _usuarioActual != null;
        }
    }
}