﻿using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace InterfazGráfica
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class Aplicación
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            EventManager.RegisterClassHandler(typeof(TextBox), UIElement.PreviewMouseLeftButtonDownEvent,
                new MouseButtonEventHandler(SelectivelyHandleMouseButton), true);
            EventManager.RegisterClassHandler(typeof(TextBox), UIElement.GotKeyboardFocusEvent,
                new RoutedEventHandler(SelectAllText), true);

            var defaultCulture = ConfigurationManager.AppSettings["DefaultCulture"] ?? "es_MX";
            var culture = new CultureInfo(defaultCulture);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            base.OnStartup(e);
        }

        private static void SelectivelyHandleMouseButton(object sender, MouseButtonEventArgs e)
        {
            if (!(sender is TextBox textbox) || textbox.IsKeyboardFocusWithin)
            {
                return;
            }

            if (e.OriginalSource.GetType().Name != "TextBoxView")
            {
                return;
            }

            e.Handled = true;
            textbox.Focus();
        }

        private static void SelectAllText(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is TextBox textBox)
            {
                textBox.SelectAll();
            }
        }
    }
}