﻿using System.Windows;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Inicio
{
    /// <summary>
    ///     Lógica de interacción para PInicioCoordinador.xaml
    /// </summary>
    public partial class InicioCoordinador
    {
        public InicioCoordinador()
        {
            InitializeComponent();
        }

        private void OnConsultarTécnicosAcadémicosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_TÉCNICOS_ACADÉMICOS);
        }

        private void OnConsultarEstudiantesButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ESTUDIANTES);
        }

        private void OnConsultarOrganizacionesButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ORGANIZACIONES);
        }

        private void OnConsultarEncargadosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ENCARGADOS);
        }

        private void OnConsultarProyectosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_PROYECTOS);
        }

        private void OnRegistrarTécnicoAcadémicoButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_TÉCNICO_ACADÉMICO);
        }

        private void OnRegistrarOrganizaciónButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ORGANIZACIÓN);
        }

        private void OnRegistrarEncargadoButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ENCARGADO);
        }

        private void OnRegistrarProyectoButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_PROYECTO);
        }
    }
}