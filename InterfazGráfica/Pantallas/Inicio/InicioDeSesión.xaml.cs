﻿using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Properties;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Inicio
{
    /// <summary>
    ///     Lógica de interacción para InicioDeSesión.xaml
    /// </summary>
    public partial class InicioDeSesión
    {
        public InicioDeSesión()
        {
            InitializeComponent();
            var usuario = Settings.Default.NombreUsuario;
            if (!string.IsNullOrEmpty(usuario))
            {
                UsuarioTextBox.Text = usuario;
                RecuérdameChecBox.IsChecked = true;
            }
        }

        private void OnIniciarSesiónClick(object sender, RoutedEventArgs e)
        {
            IniciarSesiónButton.IsEnabled = false;
            var usuarioText = UsuarioTextBox.Text;
            var contraseñaText = ContraseñaPasswordBox.Password;

            if (usuarioText == string.Empty)
            {
                MessageBox.Show(strings.UsuarioVacío, strings.ErrorIniciarSesión, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            if (contraseñaText == string.Empty)
            {
                MessageBox.Show(strings.ContraseñaVacía, strings.ErrorIniciarSesión, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            IniciarSesión(usuarioText, contraseñaText);
        }

        private async void IniciarSesión(string usuarioText, string contraseñaText)
        {
            var usuarioDao = new UsuarioDAO();
            Usuario usuario;
            try
            {
                usuario = await Task.Run(() =>
                    usuarioDao.GetUsuarioConCredenciales(usuarioText, contraseñaText));
            }
            catch (BusinessLogicException exception)
            {
                var code = exception.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.CONTRASEÑA_INVÁLIDA:
                    case BusinessLogicExceptionCode.CREDENCIALES_INVÁLIDAS:
                    case BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONEXIÓN_CON_BASE_DE_DATOS:
                        MessageBox.Show(exception.Message, strings.ErrorIniciarSesión, MessageBoxButton.OK,
                            MessageBoxImage.Warning);
                        ContraseñaPasswordBox.Clear();
                        break;
                    default:
                        MessageBox.Show(strings.HuboErrorIniciarSesión, strings.ErrorIniciarSesión, MessageBoxButton.OK,
                            MessageBoxImage.Warning);
                        break;
                }

                return;
            }

            MostrarPantallaInicioUsuario(usuario);
        }

        private void MostrarPantallaInicioUsuario(Usuario usuario)
        {
            if (usuario.TipoUsuario == null)
            {
                return;
            }

            var tipoUsuario = (TipoUsuario) usuario.TipoUsuario;

            try
            {
                string mensajeBienvenida;
                Género? género;
                switch (tipoUsuario)
                {
                    case TipoUsuario.Coordinador:
                        var coordinadorDao = new CoordinadorDAO();
                        var coordinador = coordinadorDao.GetCoordinadorPorIdUsuario(usuario.IdUsuario);
                        género = coordinador.Género;
                        mensajeBienvenida = género == Género.Femenino
                            ? strings.BienvenidaCoordinadora
                            : strings.BienvenidoCoordinador;
                        Sesión.CrearNuevaSesión(coordinador, tipoUsuario);
                        MessageBox.Show(mensajeBienvenida.Replace("{0}", coordinador.GetNombreCompleto()),
                            strings.InicioSesiónExitoso, MessageBoxButton.OK);
                        AdministradorPáginas.Mostrar(Página.INICIO_COORDINADOR);
                        break;
                    case TipoUsuario.Director:
                        var directorDao = new DirectorDAO();
                        var director = directorDao.GetDirectorPorIdUsuario(usuario.IdUsuario);
                        género = director.Género;
                        mensajeBienvenida = género == Género.Femenino
                            ? strings.BienvenidaDirectora
                            : strings.BienvenidoDirector;
                        Sesión.CrearNuevaSesión(director, tipoUsuario);
                        MessageBox.Show(mensajeBienvenida.Replace("{0}", director.GetNombreCompleto()),
                            strings.InicioSesiónExitoso, MessageBoxButton.OK);
                        AdministradorPáginas.Mostrar(Página.INICIO_DIRECTOR);
                        break;
                    case TipoUsuario.Estudiante:
                        var estudianteDao = new EstudianteDAO();
                        var estudiante = estudianteDao.GetEstudiantePorIdUsuario(usuario.IdUsuario);
                        género = estudiante.Género;
                        mensajeBienvenida = género == Género.Femenino
                            ? strings.BienvenidaEstudiante
                            : strings.BienvenidoEstudiante;
                        Sesión.CrearNuevaSesión(estudiante, tipoUsuario);
                        MessageBox.Show(mensajeBienvenida.Replace("{0}", estudiante.GetNombreCompleto()),
                            strings.InicioSesiónExitoso, MessageBoxButton.OK);
                        AdministradorPáginas.Mostrar(Página.INICIO_ESTUDIANTE);
                        break;
                    case TipoUsuario.TécnicoAcadémico:
                        var técnicoAcadémicoDao = new TécnicoAcadémicoDAO();
                        var técnicoAcadémico = técnicoAcadémicoDao.GetTécnicoAcadémicoPorIdUsuario(usuario.IdUsuario);
                        género = técnicoAcadémico.Género;
                        mensajeBienvenida = género == Género.Femenino
                            ? strings.BienvenidaTécnicaAcadémica
                            : strings.BienvenidoTécnicoAcadémico;
                        Sesión.CrearNuevaSesión(técnicoAcadémico, tipoUsuario);
                        MessageBox.Show(mensajeBienvenida.Replace("{0}", técnicoAcadémico.GetNombreCompleto()),
                            strings.InicioSesiónExitoso, MessageBoxButton.OK);
                        AdministradorPáginas.Mostrar(Página.INICIO_TÉCNICO_ACADÉMICO);
                        break;
                }

                if (RecuérdameChecBox.IsChecked ?? false)
                {
                    Settings.Default.NombreUsuario = usuario.NombreUsuario;
                }
                else
                {
                    Settings.Default.NombreUsuario = null;
                }

                Settings.Default.Save();
            }
            catch (BusinessLogicException exception)
            {
                MessageBox.Show(exception.Message, strings.ErrorIniciarSesión, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                ContraseñaPasswordBox.Clear();
            }
        }


        private void OnRegistrarseComoEstudianteClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ESTUDIANTE);
        }

        private void OnLoginInputTextChanged(object sender, RoutedEventArgs e)
        {
            var usuario = UsuarioTextBox.Text;
            var contraseña = ContraseñaPasswordBox.Password;
            if (!(string.IsNullOrEmpty(usuario) || string.IsNullOrEmpty(contraseña)))
            {
                IniciarSesiónButton.IsEnabled = true;
            }
            else
            {
                IniciarSesiónButton.IsEnabled = false;
            }
        }
    }
}