﻿using System.Windows;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Inicio
{
    /// <summary>
    ///     Lógica de interacción para IncioTecnicoAcademico.xaml
    /// </summary>
    public partial class InicioTécnicoAcadémico
    {
        public InicioTécnicoAcadémico()
        {
            InitializeComponent();
        }

        private void OnConsultarOrganizacionesButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ORGANIZACIONES);
        }

        private void OnConsultarEncargadosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ENCARGADOS);
        }

        private void OnConsultarProyectosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_PROYECTOS);
        }

        private void OnConsultarEstudiantesButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_ESTUDIANTES);
        }

        private void OnRegistrarOrganizaciónButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ORGANIZACIÓN);
        }

        private void OnRegistrarEncargadoButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ENCARGADO);
        }

        private void OnRegistrarProyectoButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_PROYECTO);
        }
    }
}