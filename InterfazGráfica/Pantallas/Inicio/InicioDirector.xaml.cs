﻿using System.Windows;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.PantallaInicios
{
    /// <summary>
    ///     Lógica de interacción para InicioDirector.xaml
    /// </summary>
    public partial class InicioDirector
    {
        public InicioDirector()
        {
            InitializeComponent();
        }

        private void OnRegistrarDirectorButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_DIRECTOR);
        }

        private void OnRegistrarCoordinadorButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_COORDINADOR);
        }

        private void OnMostrarDirectoresButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_DIRECTORES);
        }

        private void OnMostrarCoordinadoresButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_COORDINADORES);
        }
    }
}