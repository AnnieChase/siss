﻿using System.Windows;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Inicio
{
    /// <summary>
    ///     Lógica de interacción para InicioEstudiante.xaml
    /// </summary>
    public partial class InicioEstudiante
    {
        public InicioEstudiante()
        {
            InitializeComponent();
        }

        private void OnConsultarProyectosButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Mostrar(Página.LISTA_PROYECTOS);
        }
    }
}