﻿using System;
using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarDirector.xaml
    /// </summary>
    public partial class RegistroDirector
    {
        private bool _modoPrimerRegistroDirector;

        public RegistroDirector()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarDirector;
            TítuloÉxito = strings.ÉxitoAlRegistrarDirector;
            MensajeErrorEditar = strings.ErrorEnEditarDirector;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarDirector;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarDirector;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarDirector;
        }

        /// <summary>
        ///     Constructor de RegistroDirector para modo de edición.
        /// </summary>
        /// <param name="idDirector">Id del Director a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroDirector(int idDirector) : this()
        {
            RegistrarButton.IsEnabled = false;

            ModoEditarRegistro = true;
            TítuloError = strings.ErrorAlEditarDirector;
            TítuloÉxito = strings.ÉxitoAlEditarDirector;

            TítuloLabel.Content = strings.EditandoUnDirector;
            RegistrarButton.Content = strings.Guardar;

            idElemento = idDirector;
        }

        protected sealed override async void MostrarInformaciónElemento(int idDirector)
        {
            Director director;
            try
            {
                director = await Task.Run(() => DataAccessObject.GetDirectorPorId(idDirector));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarDirector, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = director;

            UsuarioTextBox.Text = director.Usuario.NombreUsuario;
            UsuarioTextBox.IsEnabled = false;
            NúmeroPersonalTextBox.IsEnabled = false;

            NombreTextBox.Text = director.Nombre;
            ApellidoPaternoTextBox.Text = director.ApellidoPaterno;
            ApellidoMaternoTextBox.Text = director.ApellidoMaterno;
            if (director.Género != null)
            {
                GéneroComboBox.SelectedIndex = (int) director.Género;
            }

            EmailTextBox.Text = director.Email;
            NúmeroPersonalTextBox.Text = director.NúmeroPersonal;

            RegistrarButton.IsEnabled = true;
        }

        protected override void Registrar()
        {
            var usuarioText = UsuarioTextBox.Text;
            var contraseñaText = ContraseñaPasswordBox.Password;
            var repetirContraseñaText = RepetirContraseñaPasswordBox.Password;
            const TipoUsuario tipoUsuario = TipoUsuario.Director;

            var nombreText = NombreTextBox.Text;
            var apellidoPaternoText = ApellidoPaternoTextBox.Text;
            var apellidoMaternoText = ApellidoMaternoTextBox.Text;
            var géneroIndex = GéneroComboBox.SelectedIndex;
            var emailText = EmailTextBox.Text;

            var númeroPersonal = NúmeroPersonalTextBox.Text;

            Género género;

            try
            {
                género = GetEnum<Género>(géneroIndex, strings.ErrorSeleccionaUnGénero);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            var errorValidaciónUsuario = ValidadorCampos.ValidarCamposUsuario(ModoEditarRegistro, usuarioText,
                contraseñaText, repetirContraseñaText);

            if (!string.IsNullOrEmpty(errorValidaciónUsuario))
            {
                MessageBox.Show(errorValidaciónUsuario, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                ContraseñaPasswordBox.Clear();
                RepetirContraseñaPasswordBox.Clear();
                return;
            }

            Director director;
            if (ModoEditarRegistro)
            {
                director = Elemento;
            }
            else
            {
                director = new Director
                {
                    Estatus = Estatus.Activo,
                    Usuario = new Usuario()
                };
            }

            director.Nombre = nombreText;
            director.ApellidoPaterno = apellidoPaternoText;
            director.ApellidoMaterno = apellidoMaternoText;
            director.Género = género;
            director.Email = emailText;

            director.NúmeroPersonal = númeroPersonal;

            director.Usuario.NombreUsuario = usuarioText;
            director.Usuario.Contraseña = contraseñaText;
            director.Usuario.TipoUsuario = tipoUsuario;

            if (!_modoPrimerRegistroDirector && !ModoEditarRegistro)
            {
                var opción = MessageBox.Show(
                    strings.AdvertenciaNuevoDirector,
                    strings.ConfirmaciónDeRegistro, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (opción == MessageBoxResult.No)
                {
                    return;
                }
            }

            if (_modoPrimerRegistroDirector)
            {
                var hayUnDirectorRegistrado = DataAccessObject.HayAlMenosUnDirectorActivo();

                if (hayUnDirectorRegistrado)
                {
                    MessageBox.Show(strings.ErrorDirectorYaRegistradoPR, TítuloError, MessageBoxButton.OK,
                        MessageBoxImage.Warning);
                    AdministradorPáginas.Mostrar(Página.INICIO_DE_SESIÓN);
                }
            }

            RealizarRegistro(director);
        }

        protected override int AgregarElemento(Director director)
        {
            return DataAccessObject.AgregarDirector(director);
        }

        protected override int EditarElemento(Director director)
        {
            DataAccessObject.EditarDirector(director);
            return director.IdDirector;
        }

        public void SetModoPrimerRegistroDirector()
        {
            CancelarButton.IsEnabled = false;
            CancelarButton.Visibility = Visibility.Collapsed;
            _modoPrimerRegistroDirector = true;
        }
    }
}