﻿using System;
using System.Windows;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Lógica de interacción para RegistroHorario.xaml
    /// </summary>
    public partial class RegistroHorario
    {
        private readonly Horario _horario;

        private readonly bool _modoEditarRegistro;
        private readonly string _títuloError = strings.ErrorAlRegistrarHorario;
        private readonly string _títuloÉxito = strings.ÉxitoAlRegistrarHorario;

        public RegistroHorario()
        {
            InitializeComponent();
        }

        public RegistroHorario(Horario horario) : this()
        {
            _horario = horario;
            Title = strings.EditandoUnHorario;

            _modoEditarRegistro = true;
            RegistrarButton.Content = strings.Guardar;
            _títuloError = strings.ErrorAlEditarHorario;
            _títuloÉxito = strings.ÉxitoAlEditarHorario;

            DíaComboBox.SelectedIndex = (int) horario.Día;
            HoraInicioTextBox.Text = horario.HoraInicio;
            HoraFinTextBox.Text = horario.HoraFinal;
            UbicaciónTextBox.Text = horario.Ubicación;
        }

        public event EventHandler<Horario> OnNuevoHorarioRegistrado;
        public event EventHandler<Horario> OnHorarioEditado;

        private void OnCancelarButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnRegistrarButtonClick(object sender, RoutedEventArgs e)
        {
            var díaIndex = DíaComboBox.SelectedIndex;
            var horaInicioText = HoraInicioTextBox.Text;
            var horaFinText = HoraFinTextBox.Text;
            var ubicaciónText = UbicaciónTextBox.Text;
            DíaSemana día;

            try
            {
                día = (DíaSemana) Enum.GetValues(typeof(DíaSemana)).GetValue(díaIndex);
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show(strings.ErrorSeleccionaDía, _títuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            Horario horario;
            if (_modoEditarRegistro)
            {
                horario = _horario;
            }
            else
            {
                horario = new Horario();
            }

            horario.Día = día;
            horario.HoraInicio = horaInicioText;
            horario.HoraFinal = horaFinText;
            horario.Ubicación = ubicaciónText;

            if (_modoEditarRegistro)
            {
                if (OnHorarioEditado != null)
                {
                    OnHorarioEditado(this, horario);
                }
            }
            else
            {
                if (OnNuevoHorarioRegistrado != null)
                {
                    OnNuevoHorarioRegistrado(this, horario);
                }
            }

            Close();
        }
    }
}