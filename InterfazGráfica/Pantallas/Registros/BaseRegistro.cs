using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using BusinessLogic.Excepción;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Página base para todas las pantallas de Registro.
    /// </summary>
    /// <typeparam name="TDAO">DAO para el TElement</typeparam>
    /// <typeparam name="TElement">Tipo de elemento del registro.</typeparam>
    public abstract class BaseRegistro<TDAO, TElement> : Page where TDAO : new()
    {
        /// <summary>
        ///     TDAO para TElement.
        /// </summary>
        internal readonly TDAO DataAccessObject;

        /// <summary>
        ///     Indica si hay una interacción con la base de datos actualmente.
        /// </summary>
        private bool _operaciónEnProceso;

        /// <summary>
        ///     TElement a editar.
        /// </summary>
        internal TElement Elemento;

        /// <summary>
        ///     Id del elemento a editar.
        /// </summary>
        internal int idElemento;

        /// <summary>
        ///     Mensaje de error al realizar la actualización de datos para las cajas de mensaje.
        /// </summary>
        internal string MensajeErrorEditar;

        /// <summary>
        ///     Mensaje de error al realizar el registro para las cajas de mensaje.
        /// </summary>
        internal string MensajeErrorRegistrar;

        /// <summary>
        ///     Mensaje de éxito al realizar la actualización de datos para las cajas de mensaje.
        /// </summary>
        internal string MensajeÉxitoEditar;

        /// <summary>
        ///     Mensaje de éxito al realizar el registro para las cajas de mensaje.
        /// </summary>
        internal string MensajeÉxitoRegistrar;

        /// <summary>
        ///     Indica si la pantalla de registro está en modo de actualización de datos.
        /// </summary>
        internal bool ModoEditarRegistro;

        /// <summary>
        ///     Título de error para las cajas de mensaje.
        /// </summary>
        internal string TítuloError;

        /// <summary>
        ///     Título de éxito para las cajas de mensaje.
        /// </summary>
        internal string TítuloÉxito;

        /// <summary>
        ///     Constructor de BaseRegistro. Inicializa el DAO correspondiente a TElement.
        /// </summary>
        internal BaseRegistro()
        {
            DataAccessObject = new TDAO();
        }

        /// <summary>
        ///     Muestra un mensaje de éxito al usuario, dependiendo si está realizando un nuevo registro o está modificando
        ///     uno existente. Después, regresa a la pantalla anterior.
        /// </summary>
        private void MostrarMensajeÉxito()
        {
            var mensajeÉxito = ModoEditarRegistro ? MensajeÉxitoEditar : MensajeÉxitoRegistrar;
            MessageBox.Show(mensajeÉxito, TítuloÉxito, MessageBoxButton.OK);
            AdministradorPáginas.Regresar();
        }

        /// <summary>
        ///     Muestra un mensaje de error al usuario, dependiendo si está realizando un nuevo registro o está modificando
        ///     uno existente.
        /// </summary>
        private void MostrarMensajeError()
        {
            var mensajeError = ModoEditarRegistro ? MensajeErrorEditar : MensajeErrorRegistrar;
            MessageBox.Show(mensajeError, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        protected abstract void MostrarInformaciónElemento(int idElemento);

        /// <summary>
        ///     Realiza las validaciones necesarias para realizar el registro.
        /// </summary>
        protected abstract void Registrar();

        /// <summary>
        ///     Agrega el elemento a la base de datos.
        /// </summary>
        /// <param name="elemento">Elemento a agregar</param>
        /// <returns>ID del elemento agregado</returns>
        protected abstract int AgregarElemento(TElement elemento);

        /// <summary>
        ///     Modifica el elemento en la base de datos.
        /// </summary>
        /// <param name="elemento">Elemento a modificar</param>
        /// <returns>ID del elemento modificar</returns>
        protected abstract int EditarElemento(TElement elemento);

        /// <summary>
        ///     Manejador para el evento de cargado de la pantalla.
        /// </summary>
        /// <param name="sender">Elemento quien envía el evento</param>
        /// <param name="e">El evento</param>
        internal void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (idElemento > 0)
            {
                MostrarInformaciónElemento(idElemento);
            }
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón registrar. Realiza el registro o actualización de datos.
        /// </summary>
        /// <param name="sender">Elemento quien envía el evento</param>
        /// <param name="e">El evento</param>
        internal void OnRegistrarButtonClick(object sender, RoutedEventArgs e)
        {
            if (_operaciónEnProceso)
            {
                return;
            }

            Registrar();
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón cancelar. Regresa a la pantalla anterior.
        /// </summary>
        /// <param name="sender">Elemento quien envía el evento</param>
        /// <param name="e">El evento</param>
        internal void OnCancelarButtonClick(object sender, RoutedEventArgs e)
        {
            // TODO si es un registro nuevo, verificar si hay algo escrito y confirmar la salida
            // TODO si es modificación, verificar cambios y confirmar salida.
            if (_operaciónEnProceso)
            {
                return;
            }

            AdministradorPáginas.Regresar();
        }

        /// <summary>
        ///     Regresa un enum parseado del tipo TEnumType, con un índice y un mensaje de error si sucede una excepción.
        /// </summary>
        /// <param name="index">Índice del TEnumType</param>
        /// <param name="errorMessage">Mensaje de error</param>
        /// <typeparam name="TEnumType">Tipo de Enum</typeparam>
        /// <returns>El enum de tipo TEnumType parseado</returns>
        internal TEnumType GetEnum<TEnumType>(int index, string errorMessage) where TEnumType : struct
        {
            try
            {
                return (TEnumType) Enum.GetValues(typeof(TEnumType)).GetValue(index);
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show(errorMessage, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                throw;
            }
        }

        /// <summary>
        ///     Realiza la conversión de una cadena de texto a entero. Muestra un mensaje de error si sucede una excepción.
        /// </summary>
        /// <param name="text">Texto a convertir</param>
        /// <param name="errorMessage">Mensaje de error</param>
        /// <returns>Un número entero</returns>
        internal int ConvertToInt32(string text, string errorMessage)
        {
            try
            {
                return Convert.ToInt32(text);
            }
            catch (FormatException)
            {
                MessageBox.Show(errorMessage, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                throw;
            }
        }

        /// <summary>
        ///     Realiza el registro o actualización del elemento T en la base de datos.
        /// </summary>
        /// <param name="elemento">El elemento T a registrar o actualizar</param>
        /// <returns>El ID del elemento T en la base de datos.</returns>
        internal async void RealizarRegistro(TElement elemento)
        {
            if (_operaciónEnProceso)
            {
                return;
            }

            _operaciónEnProceso = true;

            try
            {
                int idElemento;
                if (ModoEditarRegistro)
                {
                    idElemento = await Task.Run(() => EditarElemento(elemento));
                }
                else
                {
                    idElemento = await Task.Run(() => AgregarElemento(elemento));
                }

                if (idElemento > 0)
                {
                    MostrarMensajeÉxito();
                }
                else
                {
                    MostrarMensajeError();
                }
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, TítuloError, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }

            _operaciónEnProceso = false;
        }
    }
}