﻿using System;
using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarCoordinador.xaml
    /// </summary>
    public partial class RegistroCoordinador
    {
        public RegistroCoordinador()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarCoordinador;
            TítuloÉxito = strings.ÉxitoAlRegistrarCoordinador;
            MensajeErrorEditar = strings.ErrorEnEditarCoordinador;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarCoordinador;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarCoordinador;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarCoordinador;
        }

        /// <summary>
        ///     Constructor de RegistroCoordinador para modo de edición.
        /// </summary>
        /// <param name="idCoordinador">Id del Coordinador a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroCoordinador(int idCoordinador) : this()
        {
            RegistrarButton.IsEnabled = false;
            ModoEditarRegistro = true;
            TítuloError = strings.ErrorAlEditarCoordinador;
            TítuloÉxito = strings.ÉxitoAlEditarCoordinador;

            TítuloLabel.Content = strings.EditandoUnCoordinador;
            RegistrarButton.Content = strings.Guardar;

            idElemento = idCoordinador;
        }

        protected sealed override async void MostrarInformaciónElemento(int idCoordinador)
        {
            Coordinador coordinador;
            try
            {
                coordinador = await Task.Run(() => DataAccessObject.GetCoordinadorPorId(idCoordinador));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarCoordinador, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = coordinador;

            UsuarioTextBox.Text = coordinador.Usuario.NombreUsuario;
            UsuarioTextBox.IsEnabled = false;

            NombreTextBox.Text = coordinador.Nombre;
            ApellidoPaternoTextBox.Text = coordinador.ApellidoPaterno;
            ApellidoMaternoTextBox.Text = coordinador.ApellidoMaterno;
            if (coordinador.Género != null)
            {
                GéneroComboBox.SelectedIndex = (int) coordinador.Género;
            }

            EmailTextBox.Text = coordinador.Email;

            ProgramaEducativoComboBox.SelectedIndex = (int) coordinador.ProgramaEducativo;
            NúmeroPersonalTextBox.Text = coordinador.NúmeroPersonal;
            NúmeroPersonalTextBox.IsEnabled = false;

            var tipoUsuario = Sesión.GetTipoUsuario();

            if (tipoUsuario != TipoUsuario.Director)
            {
                ProgramaEducativoComboBox.IsEnabled = false;
            }

            RegistrarButton.IsEnabled = true;
        }

        protected override void Registrar()
        {
            var usuarioText = UsuarioTextBox.Text;
            var contraseñaText = ContraseñaPasswordBox.Password;
            var repetirContraseñaText = RepetirContraseñaPasswordBox.Password;
            const TipoUsuario tipoUsuario = TipoUsuario.Coordinador;

            var nombreText = NombreTextBox.Text;
            var apellidoPaternoText = ApellidoPaternoTextBox.Text;
            var apellidoMaternoText = ApellidoMaternoTextBox.Text;
            var géneroIndex = GéneroComboBox.SelectedIndex;
            var emailText = EmailTextBox.Text;

            var programaEducativoIndex = ProgramaEducativoComboBox.SelectedIndex;
            var númeroPersonalText = NúmeroPersonalTextBox.Text;

            Género género;
            ProgramaEducativo programaEducativo;

            try
            {
                género = GetEnum<Género>(géneroIndex, strings.ErrorSeleccionaUnGénero);
                programaEducativo =
                    GetEnum<ProgramaEducativo>(programaEducativoIndex, strings.ErrorSeleccionaUnProgramaEducativo);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            var errorValidaciónUsuario = ValidadorCampos.ValidarCamposUsuario(ModoEditarRegistro, usuarioText,
                contraseñaText, repetirContraseñaText);

            if (!string.IsNullOrEmpty(errorValidaciónUsuario))
            {
                MessageBox.Show(errorValidaciónUsuario, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                ContraseñaPasswordBox.Clear();
                RepetirContraseñaPasswordBox.Clear();
                return;
            }

            Coordinador coordinador;
            if (ModoEditarRegistro)
            {
                coordinador = Elemento;
            }
            else
            {
                coordinador = new Coordinador
                {
                    Estatus = Estatus.Activo,
                    Usuario = new Usuario()
                };
            }

            coordinador.Nombre = nombreText;
            coordinador.ApellidoPaterno = apellidoPaternoText;
            coordinador.ApellidoMaterno = apellidoMaternoText;
            coordinador.Género = género;
            coordinador.Email = emailText;

            coordinador.NúmeroPersonal = númeroPersonalText;
            coordinador.ProgramaEducativo = programaEducativo;

            coordinador.Usuario.NombreUsuario = usuarioText;
            coordinador.Usuario.Contraseña = contraseñaText;
            coordinador.Usuario.TipoUsuario = tipoUsuario;


            if (!ModoEditarRegistro)
            {
                var hayUnCoordinadorActivoEnProgramaEducativoSeleccionado =
                    DataAccessObject.HayUnCoordinadorActivoEn(programaEducativo);

                if (hayUnCoordinadorActivoEnProgramaEducativoSeleccionado)
                {
                    var opción = MessageBox.Show(
                        strings.AdvertenciaDosCoordinadoresEnProgramaEducativo.Replace("{0}",
                            coordinador.ProgramaEducativo.ToString()),
                        strings.ConfirmaciónDeRegistro, MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (opción == MessageBoxResult.No)
                    {
                        return;
                    }
                }
            }

            RealizarRegistro(coordinador);
        }

        protected override int AgregarElemento(Coordinador coordinador)
        {
            return DataAccessObject.AgregarCoordinador(coordinador);
        }

        protected override int EditarElemento(Coordinador coordinador)
        {
            DataAccessObject.EditarCoordinador(coordinador);
            return coordinador.IdCoordinador;
        }
    }
}