﻿using System;
using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarEncargado.xaml
    /// </summary>
    public partial class RegistroEncargado
    {
        public RegistroEncargado()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarEncargado;
            TítuloÉxito = strings.ÉxitoAlRegistrarEncargado;
            MensajeErrorEditar = strings.ErrorEnEditarEncargado;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarEncargado;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarEncargado;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarEncargado;

            var organizaciónDao = new OrganizaciónDAO();
            var organizaciones = organizaciónDao.GetOrganizaciones();
            organizaciones.Sort((a, b) =>
                string.Compare(a.NombreOrganización, b.NombreOrganización, StringComparison.Ordinal));
            OrganizaciónComboBox.ItemsSource = organizaciones;

            if (organizaciones.Count == 0)
            {
                MessageBox.Show("No existen organizaciones registradas en el sistema.", TítuloError,
                    MessageBoxButton.OK);
                AdministradorPáginas.Regresar();
            }
        }

        /// <summary>
        ///     Constructor de RegistroEncargado para modo de edición.
        /// </summary>
        /// <param name="idEncargado">Id del Encargado a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroEncargado(int idEncargado) : this()
        {
            RegistrarButton.IsEnabled = false;

            ModoEditarRegistro = true;

            TítuloError = strings.ErrorAlEditarEncargado;
            TítuloÉxito = strings.ExitoAlEditarEncargado;

            TítuloLabel.Content = strings.EditandoUnEncargado;
            RegistrarButton.Content = strings.Guardar;

            idElemento = idEncargado;
        }

        protected sealed override async void MostrarInformaciónElemento(int idEncargado)
        {
            Encargado encargado;
            try
            {
                encargado = await Task.Run(() => DataAccessObject.GetEncargadosPorId(idEncargado));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarEncargado, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = encargado;

            NombreTextBox.Text = encargado.Nombre;
            ApellidoPaternoTextBox.Text = encargado.ApellidoPaterno;
            ApellidoMaternoTextBox.Text = encargado.ApellidoMaterno;
            if (encargado.Género != null)
            {
                GéneroComboBox.SelectedIndex = (int) encargado.Género;
            }

            EmailTextBox.Text = encargado.Email;

            CargoTextBox.Text = encargado.Cargo;
            ExtensiónTelefónicaTextBox.Text = encargado.ExtensiónTelefónica;

            RegistrarButton.IsEnabled = false;
        }

        protected override void Registrar()
        {
            var nombreText = NombreTextBox.Text;
            var apellidoPaternoText = ApellidoMaternoTextBox.Text;
            var apellidoMaternoText = ApellidoMaternoTextBox.Text;
            var géneroIndex = GéneroComboBox.SelectedIndex;
            var emailText = EmailTextBox.Text;

            var cargoText = CargoTextBox.Text;
            var extensionTelefonicaText = ExtensiónTelefónicaTextBox.Text;
            
            var organizacion = (Organización) OrganizaciónComboBox.SelectedItem;
            if (organizacion == null)
            {
                MessageBox.Show("No hay organizacion seleccionada", TítuloError, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            var idOrganizacion = organizacion.IdOrganización;

            Género género;

            try
            {
                género = GetEnum<Género>(géneroIndex, strings.ErrorSeleccionaUnGénero);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            Encargado encargado;
            if (ModoEditarRegistro)
            {
                encargado = Elemento;
            }
            else
            {
                encargado = new Encargado
                {
                    Estatus = Estatus.Activo
                };
            }

            encargado.Nombre = nombreText;
            encargado.ApellidoPaterno = apellidoPaternoText;
            encargado.ApellidoMaterno = apellidoMaternoText;
            encargado.Género = género;
            encargado.Email = emailText;

            encargado.Cargo = cargoText;
            encargado.ExtensiónTelefónica = extensionTelefonicaText;
            encargado.IdOrganización = idOrganizacion;

            RealizarRegistro(encargado);
        }

        protected override int AgregarElemento(Encargado encargado)
        {
            return DataAccessObject.AgregarEncargado(encargado);
        }

        protected override int EditarElemento(Encargado encargado)
        {
            DataAccessObject.EditarEncargado(encargado);
            return encargado.IdEncargado;
        }
    }
}