﻿using System;
using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarOrganizacion.xaml
    /// </summary>
    public partial class RegistroOrganización
    {
        public RegistroOrganización()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarOrganizacion;
            TítuloÉxito = strings.ÉxitoAlRegistrarOrganizacion;
            MensajeErrorEditar = strings.ErrorEnEditarOrganizacion;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarOrganizacion;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarOrganizacion;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarOrganizacion;
        }

        /// <summary>
        ///     Constructor de RegistroOrganización para modo de edición.
        /// </summary>
        /// <param name="idOrganización">Id de la Organización a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroOrganización(int idOrganización) : this()
        {
            RegistrarButton.IsEnabled = false;

            ModoEditarRegistro = true;
            TítuloError = strings.ErrorAlEditarOrganizacion;
            TítuloÉxito = strings.ÉxitoAlEditarOrganizacion;

            TítuloLabel.Content = strings.EditandoUnaOrganización;
            RegistrarButton.Content = strings.Guardar;

            idElemento = idOrganización;
        }

        protected sealed override async void MostrarInformaciónElemento(int idOrganización)
        {
            Organización organización;
            try
            {
                organización = await Task.Run(() => DataAccessObject.GetOrganizaciónPorId(idOrganización));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarOrganizacion, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = organización;

            NombreOrganizaciónTextBox.Text = organización.NombreOrganización;
            EntidadFederativaComboBox.Text = Convert.ToString(organización.EntidadFederativa);
            CiudadTextBox.Text = organización.Ciudad;
            SectorTextBox.Text = organización.Sector;
            EmailTextBox.Text = organización.Email;
            DirecciónTextBox.Text = organización.DirecciónOrganización;
            TeléfonoTextBox.Text = organización.Teléfono;

            RegistrarButton.IsEnabled = true;
        }

        protected override void Registrar()
        {
            var entidadIndex = EntidadFederativaComboBox.SelectedIndex;

            var nombreOrganizacion = NombreOrganizaciónTextBox.Text;
            var ciudad = CiudadTextBox.Text;
            var sector = SectorTextBox.Text;
            var email = EmailTextBox.Text;
            var direccionOrganizacion = DirecciónTextBox.Text;
            var telefono = TeléfonoTextBox.Text;

            EntidadFederativa entidadFederativa;

            try
            {
                entidadFederativa = GetEnum<EntidadFederativa>(entidadIndex, strings.ErrorSeleccionaEntidadFederativa);
            }
            catch (Exception)
            {
                return;
            }

            Organización organización;

            if (ModoEditarRegistro)
            {
                organización = Elemento;
            }
            else
            {
                organización = new Organización
                {
                    Estatus = Estatus.Activo
                };
            }

            organización.NombreOrganización = nombreOrganizacion;
            organización.EntidadFederativa = entidadFederativa;
            organización.Ciudad = ciudad;
            organización.Sector = sector;
            organización.Email = email;
            organización.DirecciónOrganización = direccionOrganizacion;
            organización.Teléfono = telefono;

            RealizarRegistro(organización);
        }

        protected override int AgregarElemento(Organización organización)
        {
            return DataAccessObject.AgregarOrganización(organización);
        }

        protected override int EditarElemento(Organización organización)
        {
            DataAccessObject.EditarOrganización(organización);
            return organización.IdOrganización;
        }
    }
}