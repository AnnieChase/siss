﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarTecnicoAcademico.xaml
    /// </summary>
    public partial class RegistroTécnicoAcadémico
    {
        private ObservableCollection<Horario> _listaHorarios;
        private RegistroHorario _registroHorario;

        public RegistroTécnicoAcadémico()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarTécnicoAcadémico;
            TítuloÉxito = strings.ÉxitoAlRegistrarTécnicoAcadémico;
            MensajeErrorEditar = strings.ErrorEnEditarTécnicoAcadémico;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarTécnicoAcadémico;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarTécnicoAcadémico;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarTécnicoAcadémico;

            _listaHorarios = new ObservableCollection<Horario>();
            HorariosDataGrid.IsReadOnly = true;
            HorariosDataGrid.SelectionMode = DataGridSelectionMode.Single;
            HorariosDataGrid.CanUserReorderColumns = false;
            HorariosDataGrid.CanUserResizeRows = false;
            HorariosDataGrid.ItemsSource = _listaHorarios;

            var tipoUsuario = Sesión.GetTipoUsuario();

            if (tipoUsuario == TipoUsuario.Coordinador)
            {
                var coordinador = (Coordinador) Sesión.GetUsuarioActual();
                var programaEducativo = coordinador.ProgramaEducativo;
                ProgramaEducativoComboBox.SelectedIndex = (int) programaEducativo;
                ProgramaEducativoComboBox.IsEnabled = false;
            }
        }

        /// <summary>
        ///     Constructor de RegistroTécnicoAcadémico para modo de edición.
        /// </summary>
        /// <param name="idTécnicoAcadémico">Id del Técnico Académico a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroTécnicoAcadémico(int idTécnicoAcadémico) : this()
        {
            RegistrarButton.IsEnabled = false;

            RegistrarButton.IsEnabled = false;
            ModoEditarRegistro = true;
            TítuloError = strings.ErrorAlEditarTécnicoAcadémico;
            TítuloÉxito = strings.ÉxitoAlEditarTécnicoAcadémico;

            TítuloLabel.Content = strings.EditandoUnTécnicoAcadémico;
            RegistrarButton.Content = strings.Guardar;

            idElemento = idTécnicoAcadémico;
        }

        protected sealed override async void MostrarInformaciónElemento(int idTécnicoAcadémico)
        {
            TécnicoAcadémico técnicoAcadémico;
            try
            {
                técnicoAcadémico = await Task.Run(() => DataAccessObject.GetTécnicoAcadémicoPorId(idTécnicoAcadémico));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarTécnicoAcadémico,
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = técnicoAcadémico;

            UsuarioTextBox.Text = técnicoAcadémico.Usuario.NombreUsuario;
            UsuarioTextBox.IsEnabled = false;
            NúmeroPersonalTextBox.IsEnabled = false;
            NombreTextBox.Text = técnicoAcadémico.Nombre;
            ApellidoPaternoTextBox.Text = técnicoAcadémico.ApellidoPaterno;
            ApellidoMaternoTextBox.Text = técnicoAcadémico.ApellidoMaterno;
            if (técnicoAcadémico.Género != null)
            {
                GéneroComboBox.SelectedIndex = (int) técnicoAcadémico.Género;
            }

            ProgramaEducativoComboBox.SelectedIndex = (int) técnicoAcadémico.ProgramaEducativo;
            EmailTextBox.Text = técnicoAcadémico.Email;
            NúmeroPersonalTextBox.Text = técnicoAcadémico.NúmeroPersonal;
            _listaHorarios = new ObservableCollection<Horario>(técnicoAcadémico.Horarios);
            HorariosDataGrid.ItemsSource = _listaHorarios;

            HabilitarBotonesHorario();

            RegistrarButton.IsEnabled = true;
        }

        protected override void Registrar()
        {
            var usuarioText = UsuarioTextBox.Text;
            var contraseñaText = ContraseñaPasswordBox.Password;
            var repetirContraseñaText = RepetirContraseñaPasswordBox.Password;
            const TipoUsuario tipoUsuario = TipoUsuario.Coordinador;

            var nombreText = NombreTextBox.Text;
            var apellidoPaternoText = ApellidoPaternoTextBox.Text;
            var apellidoMaternoText = ApellidoMaternoTextBox.Text;
            var géneroIndex = GéneroComboBox.SelectedIndex;
            var emailText = EmailTextBox.Text;

            var programaEducativoIndex = ProgramaEducativoComboBox.SelectedIndex;
            var númeroPersonalText = NúmeroPersonalTextBox.Text;

            Género género;
            ProgramaEducativo programaEducativo;

            try
            {
                género = GetEnum<Género>(géneroIndex, strings.ErrorSeleccionaUnGénero);
                programaEducativo =
                    GetEnum<ProgramaEducativo>(programaEducativoIndex, strings.ErrorSeleccionaUnProgramaEducativo);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            var errorValidaciónUsuario = ValidadorCampos.ValidarCamposUsuario(ModoEditarRegistro, usuarioText,
                contraseñaText, repetirContraseñaText);

            if (!string.IsNullOrEmpty(errorValidaciónUsuario))
            {
                MessageBox.Show(errorValidaciónUsuario, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                ContraseñaPasswordBox.Clear();
                RepetirContraseñaPasswordBox.Clear();
                return;
            }

            TécnicoAcadémico técnicoAcadémico;
            if (ModoEditarRegistro)
            {
                técnicoAcadémico = Elemento;
            }
            else
            {
                técnicoAcadémico = new TécnicoAcadémico
                {
                    Estatus = Estatus.Activo,
                    Usuario = new Usuario()
                };
            }

            técnicoAcadémico.Nombre = nombreText;
            técnicoAcadémico.ApellidoPaterno = apellidoPaternoText;
            técnicoAcadémico.ApellidoMaterno = apellidoMaternoText;
            técnicoAcadémico.Género = género;
            técnicoAcadémico.Email = emailText;

            técnicoAcadémico.NúmeroPersonal = númeroPersonalText;
            técnicoAcadémico.ProgramaEducativo = programaEducativo;

            técnicoAcadémico.Usuario.NombreUsuario = usuarioText;
            técnicoAcadémico.Usuario.Contraseña = contraseñaText;
            técnicoAcadémico.Usuario.TipoUsuario = tipoUsuario;

            var horarios = new List<Horario>();
            foreach (Horario horario in HorariosDataGrid.Items)
            {
                horarios.Add(horario);
            }

            técnicoAcadémico.Horarios = horarios;

            RealizarRegistro(técnicoAcadémico);
        }

        protected override int AgregarElemento(TécnicoAcadémico técnicoAcadémico)
        {
            return DataAccessObject.AgregarTécnicoAcadémico(técnicoAcadémico);
        }

        protected override int EditarElemento(TécnicoAcadémico técnicoAcadémico)
        {
            DataAccessObject.EditarTécnicoAcadémico(técnicoAcadémico);
            return técnicoAcadémico.IdTécnicoAcadémico;
        }

        private void OnHorariosDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HabilitarBotonesHorario();
        }

        private void OnHorariosDataGridRowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnEditarHorarioButtonClick(sender, e);
        }

        private void OnAgregarHorarioButtonClick(object sender, RoutedEventArgs e)
        {
            MostrarVentanaRegistroHorario();
        }

        private void OnEditarHorarioButtonClick(object sender, RoutedEventArgs e)
        {
            if (HorariosDataGrid.SelectedItem is Horario horario)
            {
                MostrarVentanaRegistroHorario(horario);
            }
        }

        private void OnEliminarHorarioButtonClick(object sender, RoutedEventArgs e)
        {
            if (HorariosDataGrid.SelectedItem is Horario horario)
            {
                if (horario.IdHorario > 0)
                {
                    MessageBox.Show(strings.ErrorHorarioYaRegistradoEnSistema, TítuloError, MessageBoxButton.OK,
                        MessageBoxImage.Warning);
                }
                else
                {
                    _listaHorarios.Remove(horario);
                }
            }
        }

        private void MostrarVentanaRegistroHorario(Horario horario = null)
        {
            DeshabilitarBotonesHorario();
            if (horario == null)
            {
                _registroHorario = new RegistroHorario();
                _registroHorario.OnNuevoHorarioRegistrado += OnNuevoHorarioRegistrado;
            }
            else
            {
                _registroHorario = new RegistroHorario(horario);
                _registroHorario.OnHorarioEditado += OnHorarioEditado;
            }

            _registroHorario.Closed += OnRegistroHorarioClosed;
            _registroHorario.Show();
        }

        private void OnRegistroHorarioClosed(object sender, EventArgs e)
        {
            _registroHorario = null;
            HabilitarBotonesHorario();
        }

        private void HabilitarBotonesHorario()
        {
            HorariosDataGrid.IsEnabled = true;
            AgregarHorarioButton.IsEnabled = true;
            if (HorariosDataGrid.SelectedItem is Horario horario)
            {
                EditarHorarioButton.IsEnabled = true;
                EliminarHorarioButton.IsEnabled = horario.IdHorario <= 0;
            }
            else
            {
                EditarHorarioButton.IsEnabled = false;
                EliminarHorarioButton.IsEnabled = false;
            }
        }

        private void DeshabilitarBotonesHorario()
        {
            HorariosDataGrid.IsEnabled = false;
            AgregarHorarioButton.IsEnabled = false;
            EditarHorarioButton.IsEnabled = false;
            EliminarHorarioButton.IsEnabled = false;
        }

        private void OnNuevoHorarioRegistrado(object sender, Horario horario)
        {
            _listaHorarios.Add(horario);
        }

        private void OnHorarioEditado(object sender, Horario horario)
        {
            HorariosDataGrid.SelectedItem = horario;
            HorariosDataGrid.Items.Refresh();
        }

        private void OnUnload(object sender, RoutedEventArgs e)
        {
            if (_registroHorario != null)
            {
                _registroHorario.Close();
                _registroHorario = null;
            }
        }
    }
}