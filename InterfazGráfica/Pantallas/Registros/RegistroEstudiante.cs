﻿using System;
using System.Threading.Tasks;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistrarEstudiante.xaml
    /// </summary>
    public partial class RegistroEstudiante
    {
        public RegistroEstudiante()
        {
            InitializeComponent();
            TítuloError = strings.ErrorAlRegistrarEstudiante;
            TítuloÉxito = strings.ÉxitoAlRegistrarEstudiante;
            MensajeErrorEditar = strings.ErrorEnEditarEstudiante;
            MensajeErrorRegistrar = strings.ErrorEnRegistrarEstudiante;
            MensajeÉxitoEditar = strings.MensajeÉxitoEditarEstudiante;
            MensajeÉxitoRegistrar = strings.MensajeÉxitoRegistrarEstudiante;
        }

        /// <summary>
        ///     Constructor de RegistroEstudiante para modo de edición.
        /// </summary>
        /// <param name="idEstudiante">Id del Estudiante a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        public RegistroEstudiante(int idEstudiante) : this()
        {
            RegistrarButton.IsEnabled = false;

            ModoEditarRegistro = true;
            TítuloError = strings.ErrorAlEditarEstudiante;
            TítuloÉxito = strings.ÉxitoAlEditarEstudiante;

            RegistrarButton.Content = strings.Guardar;
            TítuloLabel.Content = strings.EditandoUnDirector;

            idElemento = idEstudiante;
        }

        protected sealed override async void MostrarInformaciónElemento(int idEstudiante)
        {
            Estudiante estudiante;
            try
            {
                estudiante = await Task.Run(() => DataAccessObject.GetEstudiantePorId(idEstudiante));
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlEditarEstudiante, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                AdministradorPáginas.Regresar();
                return;
            }

            Elemento = estudiante;

            MatrículaTextBox.Text = estudiante.Usuario.NombreUsuario;
            MatrículaTextBox.IsEnabled = false;

            NombreTextBox.Text = estudiante.Nombre;
            ApellidoPaternoTextBox.Text = estudiante.ApellidoPaterno;
            ApellidoMaternoTextBox.Text = estudiante.ApellidoMaterno;
            if (estudiante.Género != null)
            {
                GéneroComboBox.SelectedIndex = (int) estudiante.Género;
            }

            EmailTextBox.Text = estudiante.Email;

            if (estudiante.ProgramaEducativo != null)
            {
                ProgramaEducativoComboBox.SelectedIndex = (int) estudiante.ProgramaEducativo;
            }

            if (estudiante.Sección != null)
            {
                SecciónComboBox.SelectedIndex = (int) estudiante.Sección;
            }

            if (estudiante.Bloque != null)
            {
                BloqueComboBox.SelectedIndex = (int) estudiante.Bloque;
            }

            PeriodoEscolarTextBox.Text = estudiante.PeriodoEscolar;

            RegistrarButton.IsEnabled = true;
        }

        protected override void Registrar()
        {
            var usuarioText = MatrículaTextBox.Text.ToLower();
            var contraseñaText = ContraseñaPasswordBox.Password;
            var repetirContraseñaText = RepetirContraseñaPasswordBox.Password;
            const TipoUsuario tipoUsuario = TipoUsuario.Estudiante;

            var géneroIndex = GéneroComboBox.SelectedIndex;
            var programaEducativoIndex = ProgramaEducativoComboBox.SelectedIndex;


            var nombreText = NombreTextBox.Text;
            var apellidoPaternoText = ApellidoPaternoTextBox.Text;
            var apellidoMaternoText = ApellidoMaternoTextBox.Text;
            var emailText = EmailTextBox.Text;

            var matriculaText = usuarioText;
            var bloqueIndex = BloqueComboBox.SelectedIndex;
            var periodoEscolar = PeriodoEscolarTextBox.Text;
            var seccionIndex = SecciónComboBox.SelectedIndex;

            Género género;
            ProgramaEducativo programaEducativo;
            Sección sección;
            Bloque bloque;

            try
            {
                género = GetEnum<Género>(géneroIndex, strings.ErrorSeleccionaUnGénero);
                programaEducativo =
                    GetEnum<ProgramaEducativo>(programaEducativoIndex, strings.ErrorSeleccionaUnProgramaEducativo);
                bloque = GetEnum<Bloque>(bloqueIndex, "Debes seleccionar un bloque.");
                sección = GetEnum<Sección>(seccionIndex, strings.ErrorSelecciónaUnaSección);
            }
            catch (Exception)
            {
                return;
            }

            var errorValidaciónUsuario = ValidadorCampos.ValidarCamposUsuario(ModoEditarRegistro, usuarioText,
                contraseñaText, repetirContraseñaText);

            if (!string.IsNullOrEmpty(errorValidaciónUsuario))
            {
                MessageBox.Show(errorValidaciónUsuario, TítuloError, MessageBoxButton.OK, MessageBoxImage.Warning);
                ContraseñaPasswordBox.Clear();
                RepetirContraseñaPasswordBox.Clear();
                return;
            }

            Estudiante estudiante;
            if (ModoEditarRegistro)
            {
                estudiante = Elemento;
            }
            else
            {
                estudiante = new Estudiante
                {
                    //EstatusEstudiante = EstatusEstudiante.Espera,
                    Usuario = new Usuario()
                };
            }

            estudiante.Matrícula = matriculaText;
            estudiante.Nombre = nombreText;
            estudiante.ApellidoPaterno = apellidoPaternoText;
            estudiante.ApellidoMaterno = apellidoMaternoText;
            estudiante.Género = género;
            estudiante.Email = emailText;

            estudiante.ProgramaEducativo = programaEducativo;
            estudiante.Bloque = bloque;
            estudiante.Sección = sección;
            estudiante.PeriodoEscolar = periodoEscolar;

            estudiante.Usuario.NombreUsuario = usuarioText;
            estudiante.Usuario.Contraseña = contraseñaText;
            estudiante.Usuario.TipoUsuario = tipoUsuario;

            RealizarRegistro(estudiante);
        }

        protected override int AgregarElemento(Estudiante estudiante)
        {
            return DataAccessObject.AgregarEstudiante(estudiante);
        }

        protected override int EditarElemento(Estudiante estudiante)
        {
            DataAccessObject.AgregarEstudiante(estudiante);
            return estudiante.IdEstudiante;
        }
    }
}