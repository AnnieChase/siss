﻿using System;
using System.Windows;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Registros
{
    /// <summary>
    ///     Interaction logic for RegistroProyecto.xaml
    /// </summary>
    public partial class RegistroProyecto
    {
        private readonly bool _modoEditarRegistro;
        private readonly Proyecto _proyecto;

        public RegistroProyecto()
        {
            InitializeComponent();
        }

        public RegistroProyecto(int idProyecto)
        {
            _modoEditarRegistro = true;
            RegistrarProyectoButton.Content = "Editar Proyecto";

            var proyectoDao = new ProyectoDAO();
            var proyecto = proyectoDao.GetByIdProyecto(idProyecto);
            _proyecto = proyecto;

            NombreProyectoTextBox.Text = proyecto.NombreProyecto;
            EstudiantesSolicitadosTextBox.Text = Convert.ToString(proyecto.EstudiantesSolicitados);
            DescripcionTextBox.Text = proyecto.DescripciónGeneral;
            ObjetivoGeneralProyectoTextBox.Text = proyecto.ObjetivosGenerales;
            ObjetivoInmediatoProyectoTextBox.Text = proyecto.ObjetivosInmediatos;
            MetodologiaProyectoTextBox.Text = proyecto.Metodología;
            ObjetivoMediatoProyectoTextBox.Text = proyecto.ObjetivosMediatos;
            RecursosHumanosEconomicosMaterialesProyectoTextBox.Text = proyecto.Recursos;
            ResponsabilidadesProyectoTextBox.Text = proyecto.Responsabilidades;
            DuracionSemanasTextBox.Text = proyecto.Duración;
            ActividadesFuncionesProyectoTextBox.Text = proyecto.ActividadesFunciones;
        }

        private void OnSalirButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Regresar();
        }

        private void RegistrarProyectoButton_Click(object sender, RoutedEventArgs e)
        {
            var nombreProyectoText = NombreProyectoTextBox.Text;
            var estudiantesSolicitdadesText = EstudiantesSolicitadosTextBox.Text;
            var descripcionText = DescripcionTextBox.Text;
            var objetivoGeneralText = ObjetivoGeneralProyectoTextBox.Text;
            var obejtivoInMediatoText = ObjetivoInmediatoProyectoTextBox.Text;
            var metodologiaText = MetodologiaProyectoTextBox.Text;
            var objeitivoMediatoText = ObjetivoMediatoProyectoTextBox.Text;
            var recursosHumanosText = RecursosHumanosEconomicosMaterialesProyectoTextBox.Text;
            var actividadesFuncionesText = ActividadesFuncionesProyectoTextBox.Text;
            var responsabilidadesText = ResponsabilidadesProyectoTextBox.Text;
            var duracionText = DuracionSemanasTextBox.Text;

            Proyecto proyecto;
            if (_modoEditarRegistro)
            {
                proyecto = _proyecto;
            }
            else
            {
                proyecto = new Proyecto
                {
                    Estatus = Estatus.Activo
                };
            }

            proyecto.NombreProyecto = nombreProyectoText;
            proyecto.EstudiantesSolicitados = Convert.ToInt32(estudiantesSolicitdadesText);
            proyecto.DescripciónGeneral = descripcionText;
            proyecto.ObjetivosGenerales = objetivoGeneralText;
            proyecto.ObjetivosInmediatos = obejtivoInMediatoText;
            proyecto.Metodología = metodologiaText;
            proyecto.ObjetivosMediatos = objeitivoMediatoText;
            proyecto.Recursos = recursosHumanosText;
            proyecto.Responsabilidades = responsabilidadesText;
            proyecto.Duración = duracionText;
            proyecto.ActividadesFunciones = actividadesFuncionesText;

            var proyectoDao = new ProyectoDAO();

            int idProyecto;
            try
            {
                if (_modoEditarRegistro)
                {
                    proyectoDao.EditarProyecto(proyecto);
                    idProyecto = proyecto.IdProyecto;
                }
                else
                {
                    idProyecto = proyectoDao.AgregarProyecto(proyecto);
                }
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al registrar Proyecto", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            if (idProyecto > 0)
            {
                if (_modoEditarRegistro)
                {
                    MessageBox.Show("El Proyecto se editó de manera exitosa.",
                        "Éxito al editar Proyecto", MessageBoxButton.OK);
                }
                else
                {
                    MessageBox.Show("El Proyecto se registró de manera exitosa.",
                        "Éxito al registrar Proyecto", MessageBoxButton.OK);
                }

                AdministradorPáginas.Regresar();
            }
            else
            {
                MessageBox.Show("Hubo un error al registrar el Proyecto, Inténtelo de nuevo.",
                    "Error al registrar proyecto", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}