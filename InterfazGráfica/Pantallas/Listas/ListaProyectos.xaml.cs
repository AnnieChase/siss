﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;


namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para ListaProyectos.xaml
    /// </summary>
    public partial class ListaProyectos
    {
        public ListaProyectos()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid();
        }

        protected override List<Proyecto> GetListaElementos()
        {
            return DataAccessObject.GetProyectos();
        }

        protected override void ElementoSeleccionado(Proyecto proyecto = default)
        {
            if (proyecto != null)
            {
                EditarButton.IsEnabled = true;
                if (proyecto.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                    DeshabilitarButton.IsEnabled = true;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                    DeshabilitarButton.IsEnabled = false;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
                DeshabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_PROYECTO);
        }

        protected override void EditarElemento(Proyecto proyecto)
        {
            var idProyecto = proyecto.IdProyecto;
            var registroProyecto = new RegistroProyecto(idProyecto);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_PROYECTO, registroProyecto);
        }

        protected override void HabilitarElemento(Proyecto proyecto)
        {
            var idProyecto = proyecto.IdProyecto;

            proyecto.Estatus = Estatus.Activo;
            try
            {
                DataAccessObject.CambiarEstatus(proyecto.IdProyecto, Estatus.Activo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al habilitar proyecto", MessageBoxButton.OK);
                proyecto.Estatus = Estatus.Inactivo;
            }

        }

        protected override void DeshabilitarElemento(Proyecto proyecto)
        {
            proyecto.Estatus = Estatus.Inactivo;
            try
            {
                DataAccessObject.CambiarEstatus(proyecto.IdProyecto, Estatus.Inactivo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al deshabilitar proyecto", MessageBoxButton.OK);
                proyecto.Estatus = Estatus.Activo;
            }
        }

    }
}

    


