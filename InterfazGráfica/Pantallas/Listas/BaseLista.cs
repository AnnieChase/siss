using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Página base para todas las pantallas de Lista
    /// </summary>
    /// <typeparam name="TDAO">DAO para el TElement.</typeparam>
    /// <typeparam name="TElement">Tipo de elemento de la Lista.</typeparam>
    public abstract class BaseLista<TDAO, TElement> : Page where TDAO : new()
    {
        /// <summary>
        ///     TDAO para TElement.
        /// </summary>
        internal readonly TDAO DataAccessObject;

        /// <summary>
        ///     Lista principal.
        /// </summary>
        private DataGrid _dataGrid;

        /// <summary>
        ///     Lista de vista de collección para el filtrado del DataGrid.
        /// </summary>
        private ListCollectionView _listCollectionView;

        /// <summary>
        ///     Lista de TElements del DataGrid
        /// </summary>
        internal ObservableCollection<TElement> ListaElementos;

        /// <summary>
        ///     Constructor de BaseLista. Crea un nuevo TDAO para TElement.
        /// </summary>
        internal BaseLista()
        {
            DataAccessObject = new TDAO();
        }

        /// <summary>
        ///     Devuelve una lista con todos los TElement.
        /// </summary>
        /// <returns></returns>
        protected abstract List<TElement> GetListaElementos();

        /// <summary>
        ///     Evento de selección de TElement en el DataGrid
        /// </summary>
        /// <param name="elemento"></param>
        protected abstract void ElementoSeleccionado(TElement elemento = default);

        /// <summary>
        ///     Muestra la pantalla de registro de TElement.
        /// </summary>
        protected abstract void AgregarElemento();

        /// <summary>
        ///     Muestra la pantalla de edición de registro del TElement.
        /// </summary>
        /// <param name="elemento">TElement a editar</param>
        protected abstract void EditarElemento(TElement elemento);

        /// <summary>
        ///     Habilita el TElement en la base de datos.
        /// </summary>
        /// <param name="elemento">TElement a habilitar</param>
        protected abstract void HabilitarElemento(TElement elemento);

        /// <summary>
        ///     Deshabilita el TElement en la base de datos.
        /// </summary>
        /// <param name="elemento">TElement a deshabilitar</param>
        protected abstract void DeshabilitarElemento(TElement elemento);

        /// <summary>
        ///     Envuelve en una Task el método abstracto GetListaElementos.
        /// </summary>
        /// <returns>Un Task con el método GetListaElementos</returns>
        private Task<List<TElement>> GetListaElementosAsync()
        {
            return Task.Run(() => GetListaElementos());
        }

        /// <summary>
        ///     Actualiza visualmente los elementos del DataGrid.
        /// </summary>
        internal void RefrescarDataGrid()
        {
            _dataGrid.SelectedItem = null;
            _dataGrid.Items.Refresh();
            ActualizarEstadoBotones();
        }

        /// <summary>
        ///     Actualiza el estado de los botones, dependiend del elemento seleccionado.
        /// </summary>
        private void ActualizarEstadoBotones()
        {
            if (_dataGrid.SelectedItem is TElement elemento)
            {
                ElementoSeleccionado(elemento);
            }
            else
            {
                ElementoSeleccionado();
            }
        }

        /// <summary>
        ///     Configura apropiadamente el DataGrid recibido.
        /// </summary>
        /// <param name="dataGrid">DataGrid de la pantalla Lista</param>
        internal void SetDataGrid(DataGrid dataGrid)
        {
            _dataGrid = dataGrid;
            _dataGrid.IsReadOnly = true;
            _dataGrid.SelectionMode = DataGridSelectionMode.Single;
            _dataGrid.CanUserReorderColumns = false;
            _dataGrid.CanUserResizeRows = false;
        }

        /// <summary>
        ///     Actualiza la lista de elementos a mostrar en el DataGrid, de manera asíncrona.
        /// </summary>
        internal async void ActualizarDataGrid(Action callback = null)
        {
            ListaElementos = new ObservableCollection<TElement>(await GetListaElementosAsync());
            _listCollectionView = new ListCollectionView(ListaElementos);
            _dataGrid.ItemsSource = _listCollectionView;
            if (callback != null)
            {
                callback();
            }

            RefrescarDataGrid();
        }

        /// <summary>
        ///     Filtra la lista de elementos bajo un predicado.
        /// </summary>
        /// <param name="predicate">Predicado para el filtrado de elementos.</param>
        internal void FiltrarLista(Predicate<object> predicate = null)
        {
            _listCollectionView.Filter = predicate;
        }

        /// <summary>
        ///     Manejador para el evento de selección de elemento en el DataGrid. Actualiza el estado de los botones.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal void OnDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            ActualizarEstadoBotones();
        }

        /// <summary>
        ///     Manejador para el evento de doble click en el DataGrid. Lanza el evento Editar.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal void OnDataGridRowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnEditarButtonClick(sender, e);
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón Agregar. Muestra la pantalla de registro de TElement.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal void OnAgregarButtonClick(object sender, RoutedEventArgs e)
        {
            AgregarElemento();
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón Editar. Muestra la pantalla de edición de registro para el
        ///     TElement seleccionado.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal void OnEditarButtonClick(object sender, RoutedEventArgs e)
        {
            if (_dataGrid.SelectedItem is TElement elemento)
            {
                EditarElemento(elemento);
            }
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón Habilitar. Habilita el TElement seleccionado.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal async void OnHabilitarButtonClick(object sender, RoutedEventArgs e)
        {
            if (!(_dataGrid.SelectedItem is TElement elemento))
            {
                return;
            }

            await Task.Run(() => HabilitarElemento(elemento));
            RefrescarDataGrid();
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón Deshabilitar. Deshabilita el TElement seleccionado.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal async void OnDeshabilitarButtonClick(object sender, RoutedEventArgs e)
        {
            if (!(_dataGrid.SelectedItem is TElement elemento))
            {
                return;
            }

            await Task.Run(() => DeshabilitarElemento(elemento));
            RefrescarDataGrid();
        }

        /// <summary>
        ///     Manejador para el evento de click en el botón regresar. Regresa a la pantalla anterior.
        /// </summary>
        /// <param name="sender">Elemento que lanza el evento</param>
        /// <param name="e">El evento</param>
        internal void OnRegresarButtonClick(object sender, RoutedEventArgs e)
        {
            AdministradorPáginas.Regresar();
        }
    }
}