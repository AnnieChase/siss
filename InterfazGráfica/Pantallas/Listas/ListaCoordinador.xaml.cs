﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para la pantalla ListaCoordinador
    /// </summary>
    public partial class ListaCoordinador
    {
        public ListaCoordinador()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid();
        }

        protected override List<Coordinador> GetListaElementos()
        {
            return DataAccessObject.GetCoordinadores();
        }

        protected override void ElementoSeleccionado(Coordinador coordinador = default)
        {
            if (coordinador != null)
            {
                EditarButton.IsEnabled = true;
                if (coordinador.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                    DeshabilitarButton.IsEnabled = true;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                    DeshabilitarButton.IsEnabled = false;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
                DeshabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_COORDINADOR);
        }

        protected override void EditarElemento(Coordinador coordinador)
        {
            var idCoordinador = coordinador.IdCoordinador;
            var registroCoordinador = new RegistroCoordinador(idCoordinador);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_COORDINADOR, registroCoordinador);
        }

        protected override void HabilitarElemento(Coordinador coordinador)
        {
            var idCoordinador = coordinador.IdCoordinador;
            var programaEducativo = coordinador.ProgramaEducativo;

            var hayUnCoordinadorActivoEnProgramaEducativo =
                DataAccessObject.HayUnCoordinadorActivoEn(programaEducativo, idCoordinador);
            if (hayUnCoordinadorActivoEnProgramaEducativo)
            {
                var opción = MessageBox.Show(
                    strings.AdvertenciaDosCoordinadoresEnProgramaEducativo.Replace("{0}",
                        programaEducativo.ToString()),
                    strings.ConfirmaciónDeRegistro, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (opción == MessageBoxResult.No)
                {
                    return;
                }
            }

            coordinador.Estatus = Estatus.Activo;

            try
            {
                DataAccessObject.CambiarEstatus(coordinador, Estatus.Activo);
                DesactivarTodosLosCoordinadoresExcepto(coordinador);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlHabilitarCoordinador,
                    MessageBoxButton.OK);
                coordinador.Estatus = Estatus.Inactivo;
            }
        }

        protected override void DeshabilitarElemento(Coordinador coordinador)
        {
            coordinador.Estatus = Estatus.Inactivo;
            try
            {
                DataAccessObject.CambiarEstatus(coordinador, Estatus.Inactivo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlDeshabilitarCoordinador,
                    MessageBoxButton.OK);
                coordinador.Estatus = Estatus.Activo;
            }
        }

        /// <summary>
        ///     Deshabilita todos los coordinadores en el DataGrid del mismo programa educativo del coordinador que se
        ///     proporciona, excepto éste mismo.
        /// </summary>
        /// <param name="coordinadorExento">Coordinador exento</param>
        private void DesactivarTodosLosCoordinadoresExcepto(Coordinador coordinadorExento)
        {
            foreach (var coordinador in ListaElementos)
            {
                if (coordinador.ProgramaEducativo == coordinadorExento.ProgramaEducativo &&
                    coordinador.IdCoordinador != coordinadorExento.IdCoordinador)
                {
                    coordinador.Estatus = Estatus.Inactivo;
                }
            }
        }
    }
}