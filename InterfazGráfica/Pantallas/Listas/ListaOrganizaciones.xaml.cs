﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para ListaOrganizaciones.xaml
    /// </summary>
    public partial class ListaOrganizaciones
    {
        public ListaOrganizaciones()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid();
        }

        protected override List<Organización> GetListaElementos()
        {
            return DataAccessObject.GetOrganizaciones();
        }

        protected override void ElementoSeleccionado(Organización organización = default)
        {
            if (organización != null)
            {
                EditarButton.IsEnabled = true;
                if (organización.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                    DeshabilitarButton.IsEnabled = true;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                    DeshabilitarButton.IsEnabled = false;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
                DeshabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ORGANIZACIÓN);
        }

        protected override void EditarElemento(Organización organización)
        {
            var idOrganización = organización.IdOrganización;
            var registroOrganización = new RegistroOrganización(idOrganización);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ORGANIZACIÓN, registroOrganización);
        }

        protected override void HabilitarElemento(Organización organización)
        {
            organización.Estatus = Estatus.Activo;
            try
            {
                DataAccessObject.CambiarEstatus(organización.IdOrganización, Estatus.Activo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al habilitar organización", MessageBoxButton.OK);
                organización.Estatus = Estatus.Inactivo;
            }
        }

        protected override void DeshabilitarElemento(Organización organización)
        {
            organización.Estatus = Estatus.Inactivo;
            try
            {
                DataAccessObject.CambiarEstatus(organización.IdOrganización, Estatus.Inactivo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al deshabilitar organización",
                    MessageBoxButton.OK);
                organización.Estatus = Estatus.Activo;
            }
        }
    }
}