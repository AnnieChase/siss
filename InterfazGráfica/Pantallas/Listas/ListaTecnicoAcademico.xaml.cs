﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para la pantalla ListaTécnicoAcadémico
    /// </summary>
    public partial class ListaTécnicoAcadémico
    {
        public ListaTécnicoAcadémico()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid(() =>
            {
                var tipoUsuario = Sesión.GetTipoUsuario();

                if (tipoUsuario == TipoUsuario.Coordinador)
                {
                    var coordinador = (Coordinador) Sesión.GetUsuarioActual();
                    var programaEducativo = coordinador.ProgramaEducativo;
                    FiltrarLista(e =>
                        e is TécnicoAcadémico técnicoAcadémico &&
                        técnicoAcadémico.ProgramaEducativo == programaEducativo);
                }
            });
        }

        protected override List<TécnicoAcadémico> GetListaElementos()
        {
            return DataAccessObject.GetTécnicosAcadémicos();
        }

        protected override void ElementoSeleccionado(TécnicoAcadémico técnicoAcadémico = default)
        {
            if (técnicoAcadémico != null)
            {
                EditarButton.IsEnabled = true;
                if (técnicoAcadémico.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                    DeshabilitarButton.IsEnabled = true;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                    DeshabilitarButton.IsEnabled = false;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
                DeshabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_TÉCNICO_ACADÉMICO);
        }

        protected override void EditarElemento(TécnicoAcadémico técnicoAcadémico)
        {
            var idTecnicoAcademico = técnicoAcadémico.IdTécnicoAcadémico;
            var registroTecnicoAcademico = new RegistroTécnicoAcadémico(idTecnicoAcademico);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_TÉCNICO_ACADÉMICO, registroTecnicoAcademico);
        }

        protected override void HabilitarElemento(TécnicoAcadémico técnicoAcadémico)
        {
            técnicoAcadémico.Estatus = Estatus.Activo;
            try
            {
                DataAccessObject.CambiarEstatus(técnicoAcadémico.IdTécnicoAcadémico, Estatus.Activo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al habilitar tecnico academico",
                    MessageBoxButton.OK);
                técnicoAcadémico.Estatus = Estatus.Inactivo;
            }
        }

        protected override void DeshabilitarElemento(TécnicoAcadémico técnicoAcadémico)
        {
            técnicoAcadémico.Estatus = Estatus.Inactivo;
            try
            {
                DataAccessObject.CambiarEstatus(técnicoAcadémico.IdTécnicoAcadémico, Estatus.Inactivo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al deshabilitar el Encargado",
                    MessageBoxButton.OK);
                técnicoAcadémico.Estatus = Estatus.Activo;
            }
        }
    }
}