﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para ListaEncargados.xaml
    /// </summary>
    public partial class ListaEncargados
    {
        public ListaEncargados()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid();
        }

        protected override List<Encargado> GetListaElementos()
        {
            return DataAccessObject.GetEncargados();
        }

        protected override void ElementoSeleccionado(Encargado encargado = default)
        {
            if (encargado != null)
            {
                EditarButton.IsEnabled = true;
                if (encargado.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                    DeshabilitarButton.IsEnabled = true;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                    DeshabilitarButton.IsEnabled = false;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
                DeshabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ENCARGADO);
        }

        protected override void EditarElemento(Encargado encargado)
        {
            var idEncargado = encargado.IdEncargado;
            var registroEncargado = new RegistroEncargado(idEncargado);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_ENCARGADO, registroEncargado);
        }

        protected override void HabilitarElemento(Encargado encargado)
        {
            encargado.Estatus = Estatus.Activo;
            try
            {
                DataAccessObject.CambiarEstatus(encargado.IdEncargado, Estatus.Activo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al habilitar encargado", MessageBoxButton.OK);
                encargado.Estatus = Estatus.Inactivo;
            }
        }

        protected override void DeshabilitarElemento(Encargado encargado)
        {
            encargado.Estatus = Estatus.Inactivo;
            try
            {
                DataAccessObject.CambiarEstatus(encargado.IdEncargado, Estatus.Inactivo);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, "Error al deshabilitar encargado", MessageBoxButton.OK);
                encargado.Estatus = Estatus.Activo;
            }
        }
    }
}