﻿using System.Collections.Generic;
using System.Windows;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Pantallas.Registros;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para ListaDirectores.xaml
    /// </summary>
    public partial class ListaDirectores
    {
        public ListaDirectores()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid();
        }

        protected override List<Director> GetListaElementos()
        {
            return DataAccessObject.GetDirectores();
        }

        protected override void ElementoSeleccionado(Director director = default)
        {
            if (director != null)
            {
                EditarButton.IsEnabled = true;
                if (director.Estatus == Estatus.Activo)
                {
                    HabilitarButton.IsEnabled = false;
                }
                else
                {
                    HabilitarButton.IsEnabled = true;
                }
            }
            else
            {
                EditarButton.IsEnabled = false;
                HabilitarButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
            AdministradorPáginas.Mostrar(Página.REGISTRAR_DIRECTOR);
        }

        protected override void EditarElemento(Director director)
        {
            var idDirector = director.IdDirector;
            var registroDirector = new RegistroDirector(idDirector);
            AdministradorPáginas.Mostrar(Página.REGISTRAR_DIRECTOR, registroDirector);
        }

        protected override void HabilitarElemento(Director director)
        {
            var idDirector = director.IdDirector;

            var hayAlMenosUnDirectorActivo = DataAccessObject.HayAlMenosUnDirectorActivo();
            if (hayAlMenosUnDirectorActivo)
            {
                var opción = MessageBox.Show(
                    strings.AdvertenciaNuevoDirector, strings.ConfirmaciónDeRegistro, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (opción == MessageBoxResult.No)
                {
                    return;
                }
            }

            director.Estatus = Estatus.Activo;

            try
            {
                DataAccessObject.HabilitarDirector(idDirector);
            }
            catch (BusinessLogicException businessLogicException)
            {
                MessageBox.Show(businessLogicException.Message, strings.ErrorAlHabilitarDirector,
                    MessageBoxButton.OK);
                director.Estatus = Estatus.Inactivo;
                director.Estatus = Estatus.Inactivo;
                RefrescarDataGrid();
            }

            Sesión.CerrarSesión();
            AdministradorPáginas.Mostrar(Página.INICIO_DE_SESIÓN);
        }

        protected override void DeshabilitarElemento(Director elemento)
        {
        }
    }
}