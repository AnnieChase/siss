﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using InterfazGráfica.Recursos.Localización;
using InterfazGráfica.Utilidad;

namespace InterfazGráfica.Pantallas.Listas
{
    /// <summary>
    ///     Lógica de interacción para ListaEstudiantes.xaml
    /// </summary>
    public partial class ListaEstudiantes
    {
        public ListaEstudiantes()
        {
            InitializeComponent();
            SetDataGrid(ListaElementosDataGrid);
            ActualizarDataGrid(() =>
            {
                var tipoUsuario = Sesión.GetTipoUsuario();
                ProgramaEducativo programaEducativo;
                switch (tipoUsuario)
                {
                    case TipoUsuario.Coordinador:
                        var coordinador = (Coordinador) Sesión.GetUsuarioActual();
                        programaEducativo = coordinador.ProgramaEducativo;
                        FiltrarLista(e =>
                            e is Estudiante estudiante &&
                            estudiante.ProgramaEducativo == programaEducativo);
                        break;
                    case TipoUsuario.TécnicoAcadémico:
                        var técnicoAcadémico = (TécnicoAcadémico) Sesión.GetUsuarioActual();
                        programaEducativo = técnicoAcadémico.ProgramaEducativo;
                        FiltrarLista(e =>
                            e is Estudiante estudiante &&
                            estudiante.ProgramaEducativo == programaEducativo);
                        break;
                }
            });
        }

        protected override List<Estudiante> GetListaElementos()
        {
            return DataAccessObject.GetEstudiantes();
        }

        protected override void ElementoSeleccionado(Estudiante estudiante = default)
        {
            if (estudiante != null)
            {
                switch (estudiante.EstatusEstudiante)
                {
                    case EstatusEstudiante.Espera:
                        ValidarButton.IsEnabled = true;
                        RechazarButton.IsEnabled = true;
                        BajaButton.IsEnabled = false;
                        break;
                    case EstatusEstudiante.Aceptado:
                        BajaButton.IsEnabled = true;
                        ValidarButton.IsEnabled = false;
                        RechazarButton.IsEnabled = false;
                        break;
                    default:
                        ValidarButton.IsEnabled = false;
                        RechazarButton.IsEnabled = false;
                        BajaButton.IsEnabled = false;
                        break;
                }
            }
            else
            {
                ValidarButton.IsEnabled = false;
                RechazarButton.IsEnabled = false;
                BajaButton.IsEnabled = false;
            }
        }

        protected override void AgregarElemento()
        {
        }

        protected override void EditarElemento(Estudiante elemento)
        {
        }

        protected override void HabilitarElemento(Estudiante elemento)
        {
        }

        protected override void DeshabilitarElemento(Estudiante elemento)
        {
        }

        private async void OnCambiarEstatusButtonClick(object sender, RoutedEventArgs e)
        {
            if (ListaElementosDataGrid.SelectedItem is Estudiante estudiante)
            {
                var button = (Button) sender;
                EstatusEstudiante nuevoEstatusEstudiante;
                var anteriorEstatusEstudiante = estudiante.EstatusEstudiante;
                string mensajeAdvertencia;
                string títuloError;

                if (button == ValidarButton)
                {
                    nuevoEstatusEstudiante = EstatusEstudiante.Aceptado;
                    mensajeAdvertencia = strings.AdvertenciaAceptarEstudiante;
                    títuloError = strings.ErrorAlAceptarEstudiante;
                }
                else if (button == RechazarButton)
                {
                    nuevoEstatusEstudiante = EstatusEstudiante.Rechazado;
                    mensajeAdvertencia = strings.AdvertenciaRechazarEstudiante;
                    títuloError = strings.ErrorAlRechazarEstudiante;
                }
                else if (button == BajaButton)
                {
                    nuevoEstatusEstudiante = EstatusEstudiante.Baja;
                    mensajeAdvertencia = strings.AdvertenciaBajaEstudiante;
                    títuloError = strings.ErrorAlDarBajaEstudiante;
                }
                else
                {
                    return;
                }

                var opción = MessageBox.Show(mensajeAdvertencia, strings.ConfirmaciónCambioEstatusEstudiante,
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (opción == MessageBoxResult.No)
                {
                    return;
                }

                estudiante.EstatusEstudiante = nuevoEstatusEstudiante;

                try
                {
                    await Task.Run(() =>
                        DataAccessObject.CambiarEstatus(estudiante.IdEstudiante, nuevoEstatusEstudiante));
                }
                catch (BusinessLogicException businessLogicException)
                {
                    MessageBox.Show(businessLogicException.Message, títuloError,
                        MessageBoxButton.OK);
                    estudiante.EstatusEstudiante = anteriorEstatusEstudiante;
                }

                RefrescarDataGrid();
            }
        }
    }
}