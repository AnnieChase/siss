﻿using BusinessLogic.Util;
using Xunit;

namespace UnitTesting.BusinessLogic.Util
{
    public class ValidatorTest
    {
        [Fact]
        public void EsContraseniaInvalida()
        {
            const string contrasenia = "DVkerEf7806miPBW72QTpig4SidwyF";
            var resultado = Validator.EsContraseniaValida(contrasenia);

            Assert.False(resultado);
        }


        [Fact]
        public void EsContraseniaValida()
        {
            const string contrasenia = "@hNbh5MAizqkd9iCB91g6ajuG5a^uh";
            var resultado = Validator.EsContraseniaValida(contrasenia);

            Assert.True(resultado);
        }

        [Fact]
        public void EsEmailInvalidoTest()
        {
            const string email = "kuparin!#$%&/e@verizon.net";
            var resultado = Validator.EsEmailValido(email);

            Assert.False(resultado);
        }

        [Fact]
        public void EsEmailValidoTest()
        {
            const string email = "kuparine@verizon.net";
            var resultado = Validator.EsEmailValido(email);

            Assert.True(resultado);
        }

        [Fact]
        public void EsMatriculaInvalidaTest()
        {
            const string matricula = "zs17012959a";
            var resultado = Validator.EsMatriculaValida(matricula);

            Assert.False(resultado);
        }

        [Fact]
        public void EsMatriculaValidaTest()
        {
            const string matricula = "S17012959";
            var resultado = Validator.EsMatriculaValida(matricula);

            Assert.True(resultado);
        }

        [Fact]
        public void EsNombreInvalidoTest()
        {
            const string nombre = "Samuel Gaona(5)";
            var resultado = Validator.EsNombreValido(nombre);

            Assert.False(resultado);
        }

        [Fact]
        public void EsNombreValidoTest()
        {
            const string nombre = "Samuel Gaona";
            var resultado = Validator.EsNombreValido(nombre);

            Assert.True(resultado);
        }

        [Fact]
        public void EsNumeroInvalidoTest()
        {
            const string numero = "(+55)228 62 27(3)";
            var resultado = Validator.EsNumeroValido(numero);

            Assert.False(resultado);
        }

        [Fact]
        public void EsNumeroValidoTest()
        {
            const string numero = "2288406227";
            var resultado = Validator.EsNumeroValido(numero);

            Assert.True(resultado);
        }
    }
}