﻿using System;
using Database;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public abstract class DAOBaseTest : IDisposable
    {
        internal readonly TransacciónSQL TransacciónSql;

        protected DAOBaseTest()
        {
            TransacciónSql = new TransacciónSQL();
        }

        public void Dispose()
        {
            TransacciónSql.Revertir();
        }

        internal string GetStringLargo(int tamaño)
        {
            return new string('A', tamaño);
        }

        internal string GetStringConCaracteresEspeciales()
        {
            return "Nombre completo ?";
        }
    }
}