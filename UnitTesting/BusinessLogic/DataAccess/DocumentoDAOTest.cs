﻿using System;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class DocumentoDAOTest : DAOBaseTest
    {
        [Fact]
        public void AgregarDocumento()
        {
            var expediente = new Expediente
            {
                FechaCreaciónExpediente = new DateTime(2019, 05, 05)
            };
            var expedienteDAO = new ExpedienteDAO(TransacciónSql);
            var idExpediente = expedienteDAO.AgregarExpediente(expediente);

            var documento = new Documento
            {
                FechaEntrega = new DateTime(2019, 05, 05),
                TipoDocumento = TipoDocumento.Memoria
            };
            var documentoDAO = new DocumentoDAO(TransacciónSql);
            var idDocumento = documentoDAO.AgregarDocumento(documento, idExpediente);
            Assert.True(idDocumento > 0);
        }

        [Fact]
        public void GetAtLeastOneDocumentoTest()
        {
            var expediente = new Expediente
            {
                FechaCreaciónExpediente = new DateTime(2019, 05, 05)
            };
            var expedienteDAO = new ExpedienteDAO(TransacciónSql);
            var idExpediente = expedienteDAO.AgregarExpediente(expediente);

            var documento = new Documento
            {
                FechaEntrega = new DateTime(2019, 05, 05),
                TipoDocumento = TipoDocumento.Memoria
            };
            var documentoDAO = new DocumentoDAO(TransacciónSql);
            documentoDAO.AgregarDocumento(documento, idExpediente);

            var documentos = documentoDAO.GetDocumentos();
            Assert.True(documentos.Count > 0);
        }
    }
}