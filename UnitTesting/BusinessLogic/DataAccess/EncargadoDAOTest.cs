﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class EncargadoDAOTest : DAOBaseTest
    {
        private Encargado GetEncargadoVálidoBase()
        {
            var organizacion = new Organización
            {
                NombreOrganización = "Treviño",
                Ciudad = "Xalapa",
                DirecciónOrganización = "AV. Murillo Vidal",
                Email = "TrevisñoCompu@gmail.com",
                EntidadFederativa = EntidadFederativa.Veracruz,
                Sector = "Computacional",
                Teléfono = "2282365986",
                Estatus = Estatus.Activo
            };

            var organizacionDAO = new OrganizaciónDAO(TransacciónSql);
            var idOrganizacion = organizacionDAO.AgregarOrganización(organizacion);

            return new Encargado
            {
                Nombre = "Cecilia",
                ApellidoPaterno = "Lopez",
                ApellidoMaterno = "Morales",
                Email = "CeciliaLopezM@koppodev.com",
                Género = Género.Indefinido,
                Cargo = "Project Manager",
                ExtensiónTelefónica = "057",
                Estatus = Estatus.Activo,
                IdOrganización = idOrganizacion
            };
        }

        private Encargado GetEncargadoVálidoDesdeBaseDeDatos()
        {
            var encargado = GetEncargadoVálidoBase();
            var encargadoDAO = new EncargadoDAO(TransacciónSql);
            var idEncargado = encargadoDAO.AgregarEncargado(encargado);
            return encargadoDAO.GetEncargadosPorId(idEncargado);
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoConApellidoMaternoConCaracteresEspeciales()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoConApellidoMaternoLargo()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoMaterno = GetStringLargo(60);

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoConApellidoMaternoVacio()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoMaterno = default;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoConNombreConCaracteresEspeciales()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.Nombre = GetStringConCaracteresEspeciales();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoConNombreLargo()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.Nombre = GetStringLargo(60);

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoSinApellidoMaterno()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoMaterno = null;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoSinApellidoPaterno()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoPaterno = null;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoSinGenero()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.Género = null;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoSinNombre()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.Nombre = null;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEncargadoVacío()
        {
            var encargado = new Encargado();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEstudianteConApellidoPaternoConCaracteresEspeciales()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEstudianteConApellidoPaternoLargo()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoPaterno = GetStringLargo(60);

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_NoSePuedeAgregarEstudianteConApellidoPaternoVacío()
        {
            var encargado = GetEncargadoVálidoBase();
            encargado.ApellidoPaterno = default;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.AgregarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEncargado_SePuedeAgregarEncargadoCorrectamente()
        {
            var encargado = GetEncargadoVálidoBase();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);
            var idEncargado = encargadoDAO.AgregarEncargado(encargado);
            Assert.True(idEncargado > 0);
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeCambiarEstatusDeEncargadoInexistente()
        {
            var idEncargado = 0;
            var encargadoDAO = new EncargadoDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                encargadoDAO.CambiarEstatus(idEncargado, Estatus.Inactivo));
            Assert.Equal(BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_ENCARGADO,
                exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoMaternoConCaracteresEspeciales()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoMaternoLargo()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoMaterno = GetStringLargo(60);

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoMaternoVacío()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoMaterno = default;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoPaternoConCaracteresEspeciales()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoPaternoLargo()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoPaterno = GetStringLargo(60);

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEncargado_NoSePuedeEditarEncargadoConApellidoPaternoVacío()
        {
            var encargado = GetEncargadoVálidoDesdeBaseDeDatos();
            encargado.ApellidoPaterno = default;

            var encargadoDAO = new EncargadoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => encargadoDAO.EditarEncargado(encargado));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO, exception.GetCode());
        }
    }
}