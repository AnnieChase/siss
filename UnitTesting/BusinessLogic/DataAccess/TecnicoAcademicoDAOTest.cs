﻿using System;
using System.Collections.Generic;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class TecnicoAcademicoDAOTest : DAOBaseTest
    {
        [Fact]
        public void AñadirTecnicoAcademicoConHorarioTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "dianaantonio",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            var tecnicoAcademico = new TécnicoAcadémico
            {
                Nombre = "Diana",
                ApellidoPaterno = "Antonio",
                ApellidoMaterno = "Gómez",
                Email = "dianita@icedcofee.com",
                NúmeroPersonal = new Random().Next().ToString(),
                Género = Género.Femenino,
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var horario = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "1 pm",
                HoraFinal = "4 pm",
                Ubicación = " Cubiculo 15"
            };

            tecnicoAcademico.Horarios = new List<Horario> {horario};

            var tecnicoAcademicoDao = new TécnicoAcadémicoDAO(TransacciónSql);
            var idTecnicoAcademico = tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico);
            Assert.True(idTecnicoAcademico > 0);
        }

        [Fact]
        public void AñadirTecnicoAcademicoTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "dianaantonio",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            var tecnicoAcademico = new TécnicoAcadémico
            {
                Nombre = "Diana",
                ApellidoPaterno = "Antonio",
                ApellidoMaterno = "Gómez",
                Email = "dianita@icedcofee.com",
                NúmeroPersonal = new Random().Next().ToString(),
                Género = Género.Femenino,
                Estatus = Estatus.Activo,
                Usuario = usuario
            };
            var tecnicoAcademicoDao = new TécnicoAcadémicoDAO(TransacciónSql);
            var idTecnicoAcademico = tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico);

            Assert.True(idTecnicoAcademico > 0);
        }

        [Fact]
        public void GetAtLeastOneTecnicoAcademicoTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "dianaantonio",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            var tecnicoAcademico = new TécnicoAcadémico
            {
                Nombre = "Diana",
                ApellidoPaterno = "Antonio",
                ApellidoMaterno = "Gómez",
                Email = "dianita@icedcofee.com",
                NúmeroPersonal = new Random().Next().ToString(),
                Género = Género.Femenino,
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var horario = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "1 pm",
                HoraFinal = "4 pm",
                Ubicación = " Cubiculo 15"
            };

            tecnicoAcademico.Horarios = new List<Horario> {horario};

            var tecnicoAcademicoDao = new TécnicoAcadémicoDAO(TransacciónSql);

            tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico);
            var tecnicosAcademicos = tecnicoAcademicoDao.GetTécnicosAcadémicos();
            Assert.True(tecnicosAcademicos.Count > 0);
        }

        [Fact]
        public void NoSePuedeCrear2TecnicosAcademicosConElMismoNumeroPersonalTest()
        {
            var tecnicoAcademicoDao = new TécnicoAcadémicoDAO(TransacciónSql);

            var usuario1 = new Usuario
            {
                NombreUsuario = "eduardojosue2111",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            var tecnicoAcademico1 = new TécnicoAcadémico
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                Género = Género.Masculino,
                NúmeroPersonal = "1234",
                Estatus = Estatus.Activo,
                Usuario = usuario1
            };

            var usuario2 = new Usuario
            {
                NombreUsuario = "dianaantonio",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico1);

            var tecnicoAcademico2 = new TécnicoAcadémico
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                Género = Género.Masculino,
                NúmeroPersonal = "1234",
                Estatus = Estatus.Activo,
                Usuario = usuario2
            };

            var exception =
                Assert.Throws<BusinessLogicException>(() =>
                    tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico2));
            Assert.Equal(BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_TECNICOS_ACADEMICOS_CON_MISMO_NUMERO_PERSONAL,
                exception.GetCode());
        }
    }
}