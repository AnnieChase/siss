﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class EstudianteDAOTest : DAOBaseTest
    {
        private Estudiante GetEstudianteVálidoBase()
        {
            return new Estudiante
            {
                Nombre = "Prueba",
                ApellidoPaterno = "Agregar",
                ApellidoMaterno = "Estudiante",
                Género = Género.Femenino,
                Email = "pruebaestudiante@uv.mx",
                Matrícula = "S00000000",
                ProgramaEducativo = ProgramaEducativo.IngenieriaDeSoftware,
                Bloque = Bloque.Diez,
                Sección = Sección.Dos,
                PeriodoEscolar = "Agosto - diciembre",
                Usuario = new Usuario
                {
                    NombreUsuario = "S00000000",
                    Contraseña = "pulposmágicos"
                }
            };
        }

        private Estudiante GetEstudianteVálidoDesdeBaseDeDatos()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDAO = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDAO.AgregarEstudiante(estudiante);
            return estudianteDAO.GetEstudiantePorId(idEstudiante);
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoMaternoConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoMaternoLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoMaterno = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoMaternoVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoMaterno = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoPaternoConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoPaternoLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoPaterno = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConApellidoPaternoVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoPaterno = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConBloqueCero()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Bloque = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        /*[Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConBloqueMayorQueTres()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Bloque = 4;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }*/

        /*[Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConBloqueMenorQueUno()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Bloque = 0;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }*/

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConContraseñaLarga()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.Contraseña = GetStringLargo(210);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConContraseñaVacía()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.Contraseña = string.Empty;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConEmailInválido()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Email = "??@213213.123";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConEmailLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Email = GetStringLargo(110);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConEmailVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Email = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConMatrículaDuplicada()
        {
            var estudiante1 = GetEstudianteVálidoBase();
            var estudiante2 = GetEstudianteVálidoBase();

            var estudianteDao = new EstudianteDAO(TransacciónSql);
            estudianteDao.AgregarEstudiante(estudiante1);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante2));
            Assert.Equal(BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_USUARIOS_CON_MISMO_NOMBRE_DE_USUARIO,
                exception.GetCode());
            // Sale primero la excepción de UsuarioDAO.
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConMatrículaInválida()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Matrícula = "S000000000";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConMatrículaLarga()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Matrícula = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConMatrículaVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Matrícula = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Nombre = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Nombre = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreUsuarioLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.NombreUsuario = GetStringLargo(110);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreUsuarioVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.NombreUsuario = string.Empty;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreUsuarioYMatrículaDiferente()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Matrícula = "S00000000";
            estudiante.Usuario.NombreUsuario = "S00000001";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE,
                exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConNombreVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Nombre = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConPeriodoEscolarLargo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.PeriodoEscolar = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConPeriodoEscolarVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.PeriodoEscolar = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConUsuarioDuplicado()
        {
            var estudiante1 = GetEstudianteVálidoBase();
            var estudiante2 = GetEstudianteVálidoBase();

            var estudianteDao = new EstudianteDAO(TransacciónSql);
            estudianteDao.AgregarEstudiante(estudiante1);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante2));
            Assert.Equal(BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_USUARIOS_CON_MISMO_NOMBRE_DE_USUARIO,
                exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteConUsuarioVacío()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario = new Usuario();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinApellidoMaterno()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoMaterno = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinApellidoPaterno()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ApellidoPaterno = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinBloque()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Bloque = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinContraseña()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.Contraseña = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinEmail()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Email = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinGénero()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Género = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinMatrícula()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Matrícula = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinNombre()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Nombre = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinNombreDeUsuario()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario.NombreUsuario = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinPeriodoEscolar()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.PeriodoEscolar = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinProgramaEducativo()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.ProgramaEducativo = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinSección()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Sección = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteSinUsuario()
        {
            var estudiante = GetEstudianteVálidoBase();
            estudiante.Usuario = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_NoSePuedeAgregarEstudianteVacío()
        {
            var estudiante = new Estudiante();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.AgregarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarEstudiante_SePuedeAgregarEstudianteCorrectamente()
        {
            var estudiante = GetEstudianteVálidoBase();

            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);
            Assert.True(idEstudiante > 0);
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeAceptarUnEstudianteDadoDeBaja()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeAceptarUnEstudianteRechazado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeCambiarEstatusDeEstudianteInexistente()
        {
            var idEstudiante = 0;
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado));
            Assert.Equal(BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_ESTUDIANTE,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeDarDeBajaUnEstudianteEnEspera()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_DAR_DE_BAJA_A_UN_ESTUDIANTE_EN_ESPERA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeDarDeBajaUnEstudianteRechazado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedePonerEnEsperaUnEstudianteAceptado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Espera));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_A_ESPERA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedePonerEnEsperaUnEstudianteDadoDeBaja()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Espera));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedePonerEnEsperaUnEstudianteRechazado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Espera));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeRechazarUnEstudianteAceptado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado));
            Assert.Equal(BusinessLogicExceptionCode.SOLO_PUEDES_DAR_DE_BAJA_A_UN_ESTUDIANTE_ACEPTADO,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_NoSePuedeRechazarUnEstudianteDadoDeBaja()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado));
            Assert.Equal(BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA,
                exception.GetCode());
        }

        [Fact]
        public void Test_CambiarEstatus_SePuedeAceptarUnEstudianteEnEspera()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
        }

        [Fact]
        public void Test_CambiarEstatus_SePuedeDarDeBajaUnEstudianteAceptado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
        }

        [Fact]
        public void Test_CambiarEstatus_SePuedeRechazarUnEstudianteEnEspera()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
        }

        [Fact]
        public void Test_CambiarEstatus_VolverAPonerElMismoEstatusNoHaceNada_Aceptado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
        }

        [Fact]
        public void Test_CambiarEstatus_VolverAPonerElMismoEstatusNoHaceNada_Baja()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);
        }

        [Fact]
        public void Test_CambiarEstatus_VolverAPonerElMismoEstatusNoHaceNada_Espera()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Espera);
        }

        [Fact]
        public void Test_CambiarEstatus_VolverAPonerElMismoEstatusNoHaceNada_Rechazado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoMaternoConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoMaternoLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoMaterno = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoMaternoVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoMaterno = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoPaternoConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoPaternoLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoPaterno = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConApellidoPaternoVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoPaterno = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConBloqueCero()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Bloque = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        /*[Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConBloqueMayorQueTres()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Bloque = 4;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }*/

        /*[Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConBloqueMenorQueUno()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Bloque = 0;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }*/

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConContraseñaLarga()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringLargo(210);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConEmailInválido()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Email = "??@213213.123";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConEmailLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringLargo(110);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConEmailVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Email = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConMatrículaInválida()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Matrícula = "S000000000";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConMatrículaLarga()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Matrícula = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConMatrículaVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Matrícula = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConNombreConCaracteresEspeciales()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringConCaracteresEspeciales();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConNombreLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConNombreUsuarioLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringLargo(110);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConNombreVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConPeriodoEscolarLargo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = GetStringLargo(60);

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteConPeriodoEscolarVacío()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.PeriodoEscolar = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinApellidoMaterno()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoMaterno = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinApellidoPaterno()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ApellidoPaterno = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinBloque()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Bloque = default;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinEmail()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Email = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinGénero()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Género = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinMatrícula()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Matrícula = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinNombre()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Nombre = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinPeriodoEscolar()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.PeriodoEscolar = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinProgramaEducativo()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.ProgramaEducativo = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinSección()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Sección = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteSinUsuario()
        {
            var estudiante = GetEstudianteVálidoDesdeBaseDeDatos();
            estudiante.Usuario = null;

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_NoSePuedeEditarEstudianteVacío()
        {
            var estudiante = new Estudiante();

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.EditarEstudiante(estudiante));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_EditarEstudiante_SePuedeEditarEstudianteConContraseñaVacía()
        {
            var estudiante1 = GetEstudianteVálidoDesdeBaseDeDatos();

            estudiante1.Nombre = "Nuevo";
            estudiante1.ApellidoPaterno = "Nombre";
            estudiante1.ApellidoMaterno = "Estudiante";
            estudiante1.Género = Género.Indefinido;
            estudiante1.Email = "nuevocorreo@uv.mx";
            estudiante1.Bloque = Bloque.Diez;
            estudiante1.Sección = Sección.Tres;
            estudiante1.PeriodoEscolar = "FEBRERO - JUNIO 2020";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            estudianteDao.EditarEstudiante(estudiante1);

            var estudiante2 = estudianteDao.GetEstudiantePorId(estudiante1.IdEstudiante);

            Assert.True(estudiante2.Nombre == "Nuevo");
        }

        [Fact]
        public void Test_EditarEstudiante_SePuedeEditarEstudianteCorrectamente()
        {
            var estudiante1 = GetEstudianteVálidoDesdeBaseDeDatos();

            estudiante1.Nombre = "Nuevo";
            estudiante1.ApellidoPaterno = "Nombre";
            estudiante1.ApellidoMaterno = "Estudiante";
            estudiante1.Género = Género.Indefinido;
            estudiante1.Email = "nuevocorreo@uv.mx";
            estudiante1.Bloque = Bloque.Doce;
            estudiante1.Sección = Sección.Tres;
            estudiante1.PeriodoEscolar = "FEBRERO - JUNIO 2020";
            estudiante1.Usuario.Contraseña = "nuevacontraseñajajaja";

            var estudianteDao = new EstudianteDAO(TransacciónSql);

            estudianteDao.EditarEstudiante(estudiante1);

            var estudiante2 = estudianteDao.GetEstudiantePorId(estudiante1.IdEstudiante);

            Assert.True(estudiante2.Nombre == "Nuevo");
        }

        [Fact]
        public void Test_GetEstatusActualEstudiante_NoSePuedeConseguirEstatusActualDeEstudianteInexistente()
        {
            var idEstudiante = 0;
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() =>
                estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado));
            Assert.Equal(BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_ESTUDIANTE,
                exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantePorId_NoSePuedeConseguirEstudianteConIdCero()
        {
            var estudianteDao = new EstudianteDAO();
            var exception = Assert.Throws<BusinessLogicException>(() => estudianteDao.GetEstudiantePorId(0));
            Assert.Equal(BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_ESTUDIANTE, exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantePorId_SePuedeConseguirEstudianteConIdVálido()
        {
            var estudiante1 = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante1);

            var estudiante2 = estudianteDao.GetEstudiantePorId(idEstudiante);

            Assert.True(estudiante1.Nombre == estudiante2.Nombre);
        }

        [Fact]
        public void Test_GetEstudiantePorIdUsuario_NoSePuedeConseguirEstudianteConIdUsuarioCero()
        {
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var exception =
                Assert.Throws<BusinessLogicException>(() => estudianteDao.GetEstudiantePorIdUsuario(0));
            Assert.Equal(BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantePorIdUsuario_SePuedeConseguirEstudianteConIdUsuarioVálido_Aceptado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var usuario =
                usuarioDao.GetUsuarioConCredenciales(estudiante.Usuario.NombreUsuario, estudiante.Usuario.Contraseña);

            estudianteDao.GetEstudiantePorIdUsuario(usuario.IdUsuario);
        }

        [Fact]
        public void Test_GetEstudiantePorIdUsuario_SePuedeConseguirEstudianteConIdUsuarioVálido_Baja()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Aceptado);
            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Baja);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var usuario =
                usuarioDao.GetUsuarioConCredenciales(estudiante.Usuario.NombreUsuario, estudiante.Usuario.Contraseña);

            var exception =
                Assert.Throws<BusinessLogicException>(() => estudianteDao.GetEstudiantePorIdUsuario(usuario.IdUsuario));
            Assert.Equal(BusinessLogicExceptionCode.ESTUDIANTE_BAJA, exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantePorIdUsuario_SePuedeConseguirEstudianteConIdUsuarioVálido_Espera()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var usuario =
                usuarioDao.GetUsuarioConCredenciales(estudiante.Usuario.NombreUsuario, estudiante.Usuario.Contraseña);

            var exception =
                Assert.Throws<BusinessLogicException>(() => estudianteDao.GetEstudiantePorIdUsuario(usuario.IdUsuario));
            Assert.Equal(BusinessLogicExceptionCode.ESTUDIANTE_EN_ESPERA, exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantePorIdUsuario_SePuedeConseguirEstudianteConIdUsuarioVálido_Rechazado()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDao.AgregarEstudiante(estudiante);

            estudianteDao.CambiarEstatus(idEstudiante, EstatusEstudiante.Rechazado);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var usuario =
                usuarioDao.GetUsuarioConCredenciales(estudiante.Usuario.NombreUsuario, estudiante.Usuario.Contraseña);

            var exception =
                Assert.Throws<BusinessLogicException>(() => estudianteDao.GetEstudiantePorIdUsuario(usuario.IdUsuario));
            Assert.Equal(BusinessLogicExceptionCode.ESTUDIANTE_RECHAZADO, exception.GetCode());
        }

        [Fact]
        public void Test_GetEstudiantes_SePuedeConseguirUnaListaDeEstudiantes()
        {
            var estudiante = GetEstudianteVálidoBase();
            var estudianteDao = new EstudianteDAO(TransacciónSql);
            estudianteDao.AgregarEstudiante(estudiante);

            var estudiantes = estudianteDao.GetEstudiantes();
            Assert.True(estudiantes.Count > 0);
        }
    }
}