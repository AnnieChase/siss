﻿using System.Collections.Generic;
using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class ProyectoDAOTest : DAOBaseTest
    {
        public void PuedeCrearProyectoTest()
        {
            var horarios = new List<Horario>();
            var horarioLunes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioLunes);

            var horarioMartes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMartes);

            var horarioMiercoles = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMiercoles);

            var horarioJueves = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioJueves);

            var proyecto = new Proyecto
            {
                DescripciónGeneral =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                    "Euismod lacinia at quis risus sed vulputate. Scelerisque fermentum dui faucibus in ornare quam viverra orci.",
                Duración = "24 semanas",
                NombreProyecto = "Enseñanza de programación para niños",
                EstudiantesSolicitados = 6,
                ObjetivosGenerales = "orem ipsum dolor sit amet," +
                                     "consectetur adipiscing elit, " +
                                     "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                ObjetivosInmediatos = "Sit amet purus gravida quis blandit turpis cursus. ",
                ObjetivosMediatos = "Sit amet purus gravida quis blandit turpis cursus.",
                Metodología = "Scrum",
                Recursos =
                    "orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                ActividadesFunciones = "Lectus arcu bibendum at varius vel pharetra vel.",
                Estatus = Estatus.Activo
            };

            var proyectoDAO = new ProyectoDAO(TransacciónSql);
            var idProyecto = proyectoDAO.AgregarProyecto(proyecto);
            Assert.True(idProyecto > 0);
        }

        public void GetProyectosTest()
        {
            var horarios = new List<Horario>();
            var horarioLunes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioLunes);

            var horarioMartes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMartes);

            var horarioMiercoles = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMiercoles);

            var horarioJueves = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioJueves);
            var proyecto = new Proyecto
            {
                DescripciónGeneral =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                    "Euismod lacinia at quis risus sed vulputate. Scelerisque fermentum dui faucibus in ornare quam viverra orci.",
                Duración = "24 semanas",
                NombreProyecto = "Enseñanza de programación para niños",
                EstudiantesSolicitados = 6,
                ObjetivosGenerales = "orem ipsum dolor sit amet," +
                                     "consectetur adipiscing elit, " +
                                     "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                ObjetivosInmediatos = "Sit amet purus gravida quis blandit turpis cursus. ",
                ObjetivosMediatos = "Sit amet purus gravida quis blandit turpis cursus.",
                Metodología = "Scrum",
                Recursos =
                    "orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                ActividadesFunciones = "Lectus arcu bibendum at varius vel pharetra vel.",
                Estatus = Estatus.Activo
            };


            var proyectoDAO = new ProyectoDAO(TransacciónSql);
            proyectoDAO.AgregarProyecto(proyecto);
        }

        public void NoPuedeCrearProyectoValoresFaltantesTest()
        {
            var horarios = new List<Horario>();
            var horarioLunes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioLunes);

            var horarioMartes = new Horario
            {
                Día = DíaSemana.Martes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMartes);

            var horarioMiercoles = new Horario
            {
                Día = DíaSemana.Miércoles,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMiercoles);

            var horarioJueves = new Horario
            {
                Día = DíaSemana.Jueves,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioJueves);

            var proyecto = new Proyecto
            {
                DescripciónGeneral =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                    "Euismod lacinia at quis risus sed vulputate. Scelerisque fermentum dui faucibus in ornare quam viverra orci." +
                    " Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci. In ornare quam viverra orci sagittis eu volutpat odio facilisis. " +
                    "Tempor nec feugiat nisl pretium fusce. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Pharetra vel turpis nunc eget. " +
                    "Sem viverra aliquet eget sit amet. Sed risus ultricies tristique nulla aliquet enim. Integer enim neque volutpat ac.",
                Duración = "24 semanas",
                NombreProyecto = "Enseñanza de programación para niños",
                EstudiantesSolicitados = 6,
                ObjetivosGenerales = "Risus ultricies tristique nulla aliquet enim. Euismod quis viverra nibh cras.",
                ObjetivosInmediatos =
                    "Sit amet purus gravida quis blandit turpis cursus. Lectus arcu bibendum at varius vel pharetra vel.",
                ObjetivosMediatos =
                    "Sit amet purus gravida quis blandit turpis cursus. Lectus arcu bibendum at varius vel pharetra vel.",
                Estatus = Estatus.Activo
            };

            var proyectoDAO = new ProyectoDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => proyectoDAO.AgregarProyecto(proyecto));
            Assert.Equal(BusinessLogicExceptionCode.VALORES_FALTANTES, exception.GetCode());
        }

        public void NoPuedeCrearProyectoValoresMuyGrandesTest()
        {
            var horarios = new List<Horario>();
            var horarioLunes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioLunes);

            var horarioMartes = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "9:00",
                HoraFinal = "13:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMartes);

            var horarioMiercoles = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioMiercoles);

            var horarioJueves = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "16:00",
                HoraFinal = "20:00",
                Ubicación = "Edificio A de rectoría,Operatividad e Impacto TI"
            };
            horarios.Add(horarioJueves);

            var proyecto = new Proyecto
            {
                DescripciónGeneral =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                    "Euismod lacinia at quis risus sed vulputate. Scelerisque fermentum dui faucibus in ornare quam viverra orci." +
                    " Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci. In ornare quam viverra orci sagittis eu volutpat odio facilisis. " +
                    "Tempor nec feugiat nisl pretium fusce. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Pharetra vel turpis nunc eget. " +
                    "Sem viverra aliquet eget sit amet. Sed risus ultricies tristique nulla aliquet enim. Integer enim neque volutpat ac.",
                Duración = "24 semanas",
                EstudiantesSolicitados = 6,
                ObjetivosGenerales = "orem ipsum dolor sit amet," +
                                     "consectetur adipiscing elit, " +
                                     "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                                     "Euismod lacinia at quis risus sed vulputate. Scelerisque fermentum dui faucibus in ornare quam viverra orci." +
                                     " Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci. In ornare quam viverra orci sagittis eu volutpat odio facilisis. " +
                                     "Tempor nec feugiat nisl pretium fusce. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Pharetra vel turpis nunc eget. " +
                                     "Sem viverra aliquet eget sit amet. Sed risus ultricies tristique nulla aliquet enim. Integer enim neque volutpat ac.",
                ObjetivosInmediatos =
                    "Sit amet purus gravida quis blandit turpis cursus. Lectus arcu bibendum at varius vel pharetra vel.",
                ObjetivosMediatos =
                    "Sit amet purus gravida quis blandit turpis cursus. Lectus arcu bibendum at varius vel pharetra vel.",
                Metodología = "Scrum",
                Recursos =
                    "orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
                ActividadesFunciones = "Lectus arcu bibendum at varius vel pharetra vel.",
                Estatus = Estatus.Activo
            };

            var proyectoDAO = new ProyectoDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => proyectoDAO.AgregarProyecto(proyecto));
            Assert.Equal(BusinessLogicExceptionCode.VALORES_DEMASIADO_LARGOS, exception.GetCode());
        }
    }
}