﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class CoordinadorDAOTest : DAOBaseTest
    {
        [Fact]
        public void GetAtLeastOneCoordinadorTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "alfredosanchez",
                Contraseña = "123413113",
                TipoUsuario = TipoUsuario.Coordinador
            };

            var coordinador = new Coordinador
            {
                Nombre = "Alfredo",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Montes",
                Email = "alfredo123@panoramicas.com",
                NúmeroPersonal = "1020123123231",
                Género = Género.Masculino,
                ProgramaEducativo = ProgramaEducativo.RedesyServiciosDeComputo,
                Estatus = Estatus.Activo,
                Usuario = usuario
            };
            var coordinadorDao = new CoordinadorDAO(TransacciónSql);
            coordinadorDao.AgregarCoordinador(coordinador);

            var coordinadores = coordinadorDao.GetCoordinadores();
            Assert.True(coordinadores.Count > 0);
        }

        [Fact]
        public void NoSePuedeCrear2CoordinadoresConElMismoNumeroPersonalTest()
        {
            var coordinadorDAO = new CoordinadorDAO(TransacciónSql);

            var usuario1 = new Usuario
            {
                NombreUsuario = "alfredosanchez",
                Contraseña = "123413113",
                TipoUsuario = TipoUsuario.Coordinador
            };

            var coordinador1 = new Coordinador
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                IdCoordinador = 1234,
                Género = Género.Masculino,
                ProgramaEducativo = ProgramaEducativo.IngenieriaDeSoftware,
                NúmeroPersonal = "1asdasd",
                Estatus = Estatus.Activo,
                Usuario = usuario1
            };

            coordinadorDAO.AgregarCoordinador(coordinador1);

            var usuario2 = new Usuario
            {
                NombreUsuario = "eduardojosue",
                Contraseña = "123413113",
                TipoUsuario = TipoUsuario.Coordinador
            };

            var coordinador2 = new Coordinador
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                IdCoordinador = 1234,
                Género = Género.Masculino,
                ProgramaEducativo = ProgramaEducativo.IngenieriaDeSoftware,
                NúmeroPersonal = "1asdasd",
                Estatus = Estatus.Activo,
                Usuario = usuario2
            };

            var exception =
                Assert.Throws<BusinessLogicException>(() => coordinadorDAO.AgregarCoordinador(coordinador2));
            Assert.Equal(BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_COORDINADORES_CON_MISMO_NUMERO_PERSONAL,
                exception.GetCode());
        }

        [Fact]
        public void TestAgregarCoordinador()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "alfredosanchez",
                Contraseña = "123413113",
                TipoUsuario = TipoUsuario.Coordinador
            };

            var coordinador = new Coordinador
            {
                Nombre = "Alfredo",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Montes",
                Email = "alfredo123@panoramicas.com",
                NúmeroPersonal = "1020",
                Género = Género.Masculino,
                ProgramaEducativo = ProgramaEducativo.RedesyServiciosDeComputo,
                Estatus = Estatus.Activo,
                Usuario = usuario
            };
            var coordinadorDao = new CoordinadorDAO(TransacciónSql);
            var idCoordinador = coordinadorDao.AgregarCoordinador(coordinador);
            Assert.True(idCoordinador > 0);
        }
    }
}