﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class UsuarioDAOTest : DAOBaseTest
    {
        private Usuario GetUsuarioVálido()
        {
            return new Usuario
            {
                NombreUsuario = "123123123",
                Contraseña = "pikapikachu",
                TipoUsuario = TipoUsuario.Director
            };
        }

        private Usuario GetUsuarioVálidoDesdeBaseDeDatos()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var idUsuario = usuarioDao.AgregarUsuario(usuario);
            usuario.IdUsuario = idUsuario;
            return usuario;
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioConContraseñaLargo()
        {
            var usuario = GetUsuarioVálido();
            usuario.Contraseña = GetStringLargo(210);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioConContraseñaVacía()
        {
            var usuario = GetUsuarioVálido();
            usuario.Contraseña = default;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioConNombreUsuarioLargo()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = GetStringLargo(110);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioConNombreUsuarioVacío()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = default;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioQueNoSeaEstudianteConNombreUsuarioSiendoMatrícula()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = "S12300123";
            usuario.TipoUsuario = TipoUsuario.Director;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioSinContraseña()
        {
            var usuario = GetUsuarioVálido();
            usuario.Contraseña = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioSinNombreUsuario()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioSinTipoUsuario()
        {
            var usuario = GetUsuarioVálido();
            usuario.TipoUsuario = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_NoSePuedeAgregarUsuarioVacío()
        {
            var usuario = new Usuario();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_SePuedeAgregarUsuarioCorrectamente()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var idUsuario = usuarioDao.AgregarUsuario(usuario);
            Assert.True(idUsuario > 0);
        }

        [Fact]
        public void Test_AgregarUsuario_SePuedeAgregarUsuarioQueNoSeaEstudianteConNombreUsuarioQueNoSeaMatrícula()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = "holiatodos";
            usuario.TipoUsuario = TipoUsuario.Director;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var idUsuario = usuarioDao.AgregarUsuario(usuario);
            Assert.True(idUsuario > 0);
        }

        [Fact]
        public void Test_AgregarUsuario_SePuedeAgregarUsuarioQueSeaEstudianteConNombreUsuarioQueNoSeaMatrícula()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = "holiatodos";
            usuario.TipoUsuario = TipoUsuario.Estudiante;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.AgregarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarUsuario_SePuedeAgregarUsuarioQueSeaEstudianteConNombreUsuarioSiendoMatrícula()
        {
            var usuario = GetUsuarioVálido();
            usuario.NombreUsuario = "S12300123";
            usuario.TipoUsuario = TipoUsuario.Estudiante;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var idUsuario = usuarioDao.AgregarUsuario(usuario);
            Assert.True(idUsuario > 0);
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioConContraseñaLargo()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.Contraseña = GetStringLargo(210);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioConContraseñaVacía()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.Contraseña = default;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioConNombreUsuarioLargo()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.NombreUsuario = GetStringLargo(110);

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioConNombreUsuarioVacío()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.NombreUsuario = default;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioSinContraseña()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.Contraseña = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioSinNombreUsuario()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.NombreUsuario = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioSinTipoUsuario()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.TipoUsuario = null;

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_NoSePuedeEditarUsuarioVacío()
        {
            var usuario = new Usuario();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var exception = Assert.Throws<BusinessLogicException>(() => usuarioDao.EditarUsuario(usuario));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_EditarUsuario_SePuedeEditarUsuarioCorrectamente()
        {
            var usuario = GetUsuarioVálidoDesdeBaseDeDatos();
            usuario.Contraseña = "Nueva contraseña";

            var usuarioDao = new UsuarioDAO(TransacciónSql);
            usuarioDao.EditarUsuario(usuario);
        }

        [Fact]
        public void Test_GetUsuarioConCredenciales_NoSePuedeConseguirUsuarioConContraseñaVacía()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            usuarioDao.AgregarUsuario(usuario);

            var exception = Assert.Throws<BusinessLogicException>(() =>
                usuarioDao.GetUsuarioConCredenciales(usuario.NombreUsuario, default));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_GetUsuarioConCredenciales_NoSePuedeConseguirUsuarioConCredencialesInválidas()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            usuarioDao.AgregarUsuario(usuario);

            var exception =
                Assert.Throws<BusinessLogicException>(
                    () => usuarioDao.GetUsuarioConCredenciales("invalido", "invalido"));
            Assert.Equal(BusinessLogicExceptionCode.CREDENCIALES_INVÁLIDAS, exception.GetCode());
        }

        [Fact]
        public void Test_GetUsuarioConCredenciales_NoSePuedeConseguirUsuarioConUsuarioVálidoYContraseñaInválida()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            usuarioDao.AgregarUsuario(usuario);

            var exception = Assert.Throws<BusinessLogicException>(() =>
                usuarioDao.GetUsuarioConCredenciales(usuario.NombreUsuario, "invalido"));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_INVÁLIDA, exception.GetCode());
        }

        [Fact]
        public void Test_GetUsuarioConCredenciales_NoSePuedeConseguirUsuarioSinContraseña()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            usuarioDao.AgregarUsuario(usuario);

            var exception = Assert.Throws<BusinessLogicException>(() =>
                usuarioDao.GetUsuarioConCredenciales(usuario.NombreUsuario, null));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_GetUsuarioConCredenciales_SePuedeConseguirUsuarioConCredencialesVálidas()
        {
            var usuario = GetUsuarioVálido();
            var usuarioDao = new UsuarioDAO(TransacciónSql);
            var idUsuario = usuarioDao.AgregarUsuario(usuario);

            var usuario2 = usuarioDao.GetUsuarioConCredenciales(usuario.NombreUsuario, usuario.Contraseña);
            Assert.True(usuario.NombreUsuario == usuario2.NombreUsuario);
        }
    }
}