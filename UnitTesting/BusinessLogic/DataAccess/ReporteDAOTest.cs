﻿using System;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class ReporteDAOTest : DAOBaseTest
    {
        [Fact]
        public void AgregarReporte()
        {
            var reporte = new Reporte
            {
                FechaEntrega = new DateTime(2019, 05, 05),
                HorasServicioSocial = 5,
                NumeroReporte = 2
            };
            var reporteDAO = new ReporteDAO(TransacciónSql);
            var idReporte = reporteDAO.AgregarReporte(reporte);
            Assert.True(idReporte > 0);
        }

        [Fact]
        public void GetAtLeastOneReporteTest()
        {
            var reporte = new Reporte
            {
                FechaEntrega = new DateTime(2019, 05, 05),
                HorasServicioSocial = 5,
                NumeroReporte = 2
            };
            var reporteDAO = new ReporteDAO(TransacciónSql);
            reporteDAO.AgregarReporte(reporte);

            var reportes = reporteDAO.GetReportes();
            Assert.True(reportes.Count > 0);
        }
    }
}