﻿using System;
using System.Collections.Generic;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class HorarioDAOTest : DAOBaseTest
    {
        [Fact]
        public void PuedeAgregarHorarioTecnicoAcademicoTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "dianaantonio",
                Contraseña = "anniechase",
                TipoUsuario = TipoUsuario.TécnicoAcadémico
            };

            var tecnicoAcademico = new TécnicoAcadémico
            {
                Nombre = "Diana",
                ApellidoPaterno = "Antonio",
                ApellidoMaterno = "Gómez",
                Email = "dianita@icedcofee.com",
                NúmeroPersonal = new Random().Next().ToString(),
                Género = Género.Femenino,
                Estatus = Estatus.Activo,
                Usuario = usuario,
                Horarios = new List<Horario>()
            };

            var horario = new Horario
            {
                Día = DíaSemana.Lunes,
                HoraInicio = "1 pm",
                HoraFinal = "4 pm",
                Ubicación = " Cubiculo 15"
            };

            tecnicoAcademico.Horarios.Add(horario);

            var horarioDAO = new HorarioDAO(TransacciónSql);
            var tecnicoAcademicoDao = new TécnicoAcadémicoDAO(TransacciónSql);
            var idTecnicoAcademico = tecnicoAcademicoDao.AgregarTécnicoAcadémico(tecnicoAcademico);
            var idHorarioTecnicoAcademico = horarioDAO.AgregarHorarioTécnicoAcadémico(horario, idTecnicoAcademico);

            Assert.True(idHorarioTecnicoAcademico > 0);
        }
    }
}