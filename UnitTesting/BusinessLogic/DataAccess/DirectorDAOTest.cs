﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class DirectorDAOTest : DAOBaseTest
    {
        private Director GetDirectorVálidoBase()
        {
            return new Director
            {
                Nombre = "Prueba",
                ApellidoPaterno = "Agregar",
                ApellidoMaterno = "Estudiante",
                Género = Género.Femenino,
                Email = "pruebaestudiante@uv.mx",
                NúmeroPersonal = "739182465",
                Usuario = new Usuario
                {
                    NombreUsuario = "directorwow",
                    Contraseña = "pulposmágicos"
                }
            };
        }

        private Director GetDirectorVálidoDesdeBaseDeDatos()
        {
            var director = GetDirectorVálidoBase();
            var directorDAO = new DirectorDAO(TransacciónSql);
            var idDirector = directorDAO.AgregarDirector(director);
            return directorDAO.GetDirectorPorId(idDirector);
        }

        [Fact]
        public void AgregarDirectorTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "albertosanchez",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "213",
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            var idDirector = directorDao.AgregarDirector(director);
            Assert.True(idDirector > 0);
        }

        [Fact]
        public void EditarDirectorTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "pepitouwu",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "123",
                Estatus = Estatus.Inactivo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            var idDirector = directorDao.AgregarDirector(director);
            var director2 = directorDao.GetDirectorPorId(idDirector);
            directorDao.EditarDirector(director2);
            var director3 = directorDao.GetDirectorPorId(idDirector);

            Assert.True(director3.IdDirector == idDirector);
        }

        [Fact]
        public void GetDirectoresTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "pepitouwu",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "vd12v1d221",
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            directorDao.AgregarDirector(director);

            var directores = directorDao.GetDirectores();

            Assert.True(directores.Count > 0);
        }

        [Fact]
        public void GetDirectorPorIdDirectorTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "pepitouwu",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "asdv",
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            var idDirector = directorDao.AgregarDirector(director);

            director = directorDao.GetDirectorPorId(idDirector);

            Assert.True(director.IdDirector == idDirector);
        }

        [Fact]
        public void GetDirectorPorIdUsuarioTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "pepitouwu",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "vdav1v",
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            var idDirector = directorDao.AgregarDirector(director);
            var director1 = directorDao.GetDirectorPorId(idDirector);
            var director2 = directorDao.GetDirectorPorIdUsuario(director1.Usuario.IdUsuario);

            var resultado = director1.IdDirector == director2.IdDirector;

            Assert.True(resultado);
        }

        [Fact]
        public void HayAlMenosUnDirectorTest()
        {
            var usuario = new Usuario
            {
                NombreUsuario = "pepitouwu",
                Contraseña = "asdasd",
                TipoUsuario = TipoUsuario.Director
            };

            var director = new Director
            {
                Nombre = "Alberto",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Ruiz",
                Género = Género.Masculino,
                Email = "alberto.rodrigues@gmail.com",
                NúmeroPersonal = "v1212v12v",
                Estatus = Estatus.Activo,
                Usuario = usuario
            };

            var directorDao = new DirectorDAO(TransacciónSql);
            directorDao.AgregarDirector(director);

            var resultado = directorDao.HayAlMenosUnDirectorActivo();

            Assert.True(resultado);
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoMaternoConCaracteresEspeciales()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoMaternoLargo()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoMaterno = GetStringLargo(60);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoMaternoVacío()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoMaterno = default;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoPaternoConCaracteresEspeciales()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoPaternoLargo()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoPaterno = GetStringLargo(60);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConApellidoPaternoVacío()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoPaterno = default;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConContraseñaLarga()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario.Contraseña = GetStringLargo(210);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConContraseñaVacía()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario.Contraseña = string.Empty;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConEmailInválido()
        {
            var director = GetDirectorVálidoBase();
            director.Email = "??@213213.123";

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConEmailLargo()
        {
            var director = GetDirectorVálidoBase();
            director.Email = GetStringLargo(110);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConEmailVacío()
        {
            var director = GetDirectorVálidoBase();
            director.Email = default;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConNombreConCaracteresEspeciales()
        {
            var director = GetDirectorVálidoBase();
            director.Nombre = GetStringConCaracteresEspeciales();

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConNombreLargo()
        {
            var director = GetDirectorVálidoBase();
            director.Nombre = GetStringLargo(60);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConNombreVacío()
        {
            var director = GetDirectorVálidoBase();
            director.Nombre = default;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConNombreUsuarioLargo()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario.NombreUsuario = GetStringLargo(110);

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConNombreUsuarioVacío()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario.NombreUsuario = string.Empty;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConUsuarioDuplicado()
        {
            var director1 = GetDirectorVálidoBase();
            var director2 = GetDirectorVálidoBase();

            var directorDao = new DirectorDAO(TransacciónSql);
            directorDao.AgregarDirector(director1);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director2));
            Assert.Equal(BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_USUARIOS_CON_MISMO_NOMBRE_DE_USUARIO,
                exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorConUsuarioVacío()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario = new Usuario();

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinApellidoMaterno()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoMaterno = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinApellidoPaterno()
        {
            var director = GetDirectorVálidoBase();
            director.ApellidoPaterno = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinContraseña()
        {
            var director = GetDirectorVálidoBase();
            director.Usuario.Contraseña = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.CONTRASEÑA_VACÍA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinEmail()
        {
            var director = GetDirectorVálidoBase();
            director.Email = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinGénero()
        {
            var director = GetDirectorVálidoBase();
            director.Género = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }


        [Fact]
        public void Test_AgregarDirector_NoSePuedeAgregarDirectorSinNombre()
        {
            var director = GetDirectorVálidoBase();
            director.Nombre = null;

            var directorDao = new DirectorDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => directorDao.AgregarDirector(director));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR, exception.GetCode());
        }
    }

}