﻿using System;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class ExpedienteDAOTest : DAOBaseTest
    {
        [Fact]
        public void AgregarExpediente()
        {
            var expediente = new Expediente
            {
                FechaCreaciónExpediente = new DateTime(2019, 05, 05)
            };
            var expedienteDAO = new ExpedienteDAO(TransacciónSql);
            var idExpediente = expedienteDAO.AgregarExpediente(expediente);
            Assert.True(idExpediente > 0);
        }

        [Fact]
        public void GetAtLeastOneExpedienteTest()
        {
            var expediente = new Expediente
            {
                FechaCreaciónExpediente = new DateTime(2019, 05, 05)
            };
            var expedienteDAO = new ExpedienteDAO(TransacciónSql);
            expedienteDAO.AgregarExpediente(expediente);

            var expedientes = expedienteDAO.GetExpedientes();
            Assert.True(expedientes.Count > 0);
        }
    }
}