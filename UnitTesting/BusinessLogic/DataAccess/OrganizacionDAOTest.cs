﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class OrganizacionDAOTest : DAOBaseTest
    {
        [Fact]
        public void AgregarOrganizacionTest()
        {
            var organizacion = new Organización
            {
                NombreOrganización = "Treviño",
                Ciudad = "Xalapa",
                DirecciónOrganización = "AV. Murillo Vidal",
                Email = "TrevisñoCompu@gmail.com",
                EntidadFederativa = EntidadFederativa.Veracruz,
                Sector = "Computacional",
                Teléfono = "2148185436",
                Estatus = Estatus.Activo
            };

            var organizacionDAO = new OrganizaciónDAO(TransacciónSql);
            var idOrganizacion = organizacionDAO.AgregarOrganización(organizacion);
            Assert.True(idOrganizacion > 0);
        }

        [Fact]
        public void ExisteAlMenosUnaOrganizaciónTest()
        {
            var organizacionDAO = new OrganizaciónDAO(TransacciónSql);

            var organizacion = new Organización
            {
                NombreOrganización = "Treviño Tecnicos en Servicio de Administración y Mantenimiento de Codigo",
                Ciudad = "Xalapa",
                DirecciónOrganización = "AV. Murillo Vidal",
                Email = "TrevisñoCompu@gmail.com",
                EntidadFederativa = EntidadFederativa.Veracruz,
                Sector = "Computacional",
                Teléfono = "228216565",
                Estatus = Estatus.Activo
            };

            organizacionDAO.AgregarOrganización(organizacion);
            var resultado = organizacionDAO.HayAlMenosUnaOrganización();

            Assert.True(resultado);
        }

        [Fact]
        public void GetOrganizacionesTest()
        {
            var organizacion = new Organización
            {
                NombreOrganización = "Treviño",
                Ciudad = "Xalapa",
                DirecciónOrganización = "AV. Murillo Vidal",
                Email = "TrevisñoCompu@gmail.com",
                EntidadFederativa = EntidadFederativa.Veracruz,
                Sector = "Computacional",
                Teléfono = "2148185436",
                Estatus = Estatus.Activo
            };

            var organizacionDAO = new OrganizaciónDAO(TransacciónSql);
            organizacionDAO.AgregarOrganización(organizacion);
            var organizaciones = organizacionDAO.GetOrganizaciones();
            Assert.True(organizaciones.Count > 0);
        }

        [Fact]
        public void NoSeAgregaOrganizacionConValoresLargos()
        {
            var organizacionDAO = new OrganizaciónDAO(TransacciónSql);

            var organizacion = new Organización
            {
                NombreOrganización =
                    "Treviño Tecnicos en Servicio de Administración y Mantenimiento de CodigoAÚNLEFALTAMÁSLETRASPARATIRARLAEXCEPCIÓNASDASDASDADSASDASDASDHJASDKJLASDKLJADSJKLASD",
                Ciudad = "Xalapa",
                DirecciónOrganización =
                    GetStringLargo(300),
                Email = "TrevisñoCompu@gmail.com",
                EntidadFederativa = EntidadFederativa.Veracruz,
                Sector = "Computacional",
                Teléfono = "228216565",
                Estatus = Estatus.Activo
            };

            var exception =
                Assert.Throws<BusinessLogicException>(() => organizacionDAO.AgregarOrganización(organizacion));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ORGANIZACION, exception.GetCode());
        }
    }
}