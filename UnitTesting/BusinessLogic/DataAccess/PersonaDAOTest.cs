﻿using BusinessLogic.DataAccess;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTesting.BusinessLogic.DataAccess
{
    public class PersonaDAOTest : DAOBaseTest
    {
        private Persona GetPersonaVálidoBase()
        {
            return new Estudiante
            {
                Nombre = "Prueba",
                ApellidoPaterno = "Agregar",
                ApellidoMaterno = "Persona",
                Género = Género.Femenino,
                Email = "pruebaestudiante@uv.mx",
                Matrícula = "S00000000",
                ProgramaEducativo = ProgramaEducativo.IngenieriaDeSoftware,
                Bloque = Bloque.Seis,
                Sección = Sección.Dos,
                PeriodoEscolar = "Agosto - diciembre",
                Usuario = new Usuario
                {
                    NombreUsuario = "S00000000",
                    Contraseña = "pulposmágicos"
                }
            };
        }

        private Persona GetPersonaVálidoDesdeBaseDeDatos()
        {
            var estudiante = GetPersonaVálidoBase() as Estudiante;
            var estudianteDAO = new EstudianteDAO(TransacciónSql);
            var idEstudiante = estudianteDAO.AgregarEstudiante(estudiante);
            return estudianteDAO.GetEstudiantePorId(idEstudiante);
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoMaternoConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoMaternoLargo()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoMaterno = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoMaternoVacío()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoMaterno = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoPaternoConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoPaternoLargo()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoPaterno = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConApellidoPaternoVacío()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoPaterno = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConEmailInválido()
        {
            var persona = GetPersonaVálidoBase();
            persona.Email = "??@213213.123";

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConEmailLargo()
        {
            var persona = GetPersonaVálidoBase();
            persona.Email = GetStringLargo(110);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConEmailVacío()
        {
            var persona = GetPersonaVálidoBase();
            persona.Email = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConNombreConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoBase();
            persona.Nombre = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConNombreLargo()
        {
            var persona = GetPersonaVálidoBase();
            persona.Nombre = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaConNombreVacío()
        {
            var persona = GetPersonaVálidoBase();
            persona.Nombre = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaSinApellidoMaterno()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoMaterno = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaSinApellidoPaterno()
        {
            var persona = GetPersonaVálidoBase();
            persona.ApellidoPaterno = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaSinEmail()
        {
            var persona = GetPersonaVálidoBase();
            persona.Email = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaSinGénero()
        {
            var persona = GetPersonaVálidoBase();
            persona.Género = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaSinNombre()
        {
            var persona = GetPersonaVálidoBase();
            persona.Nombre = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_NoSePuedeAgregarPersonaVacío()
        {
            var persona = new Estudiante();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.AgregarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_AgregarPersona_SePuedeAgregarPersonaCorrectamente()
        {
            var persona = GetPersonaVálidoBase();

            var personaDao = new PersonaDAO(TransacciónSql);
            var idPersona = personaDao.AgregarPersona(persona);
            Assert.True(idPersona > 0);
        }

        [Fact]
        public void Test_EditarEstudiante_SePuedeEditarEstudianteCorrectamente()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();

            persona.Nombre = "Nuevo";
            persona.ApellidoPaterno = "Nombre";
            persona.ApellidoMaterno = "Persona";
            persona.Género = Género.Indefinido;
            persona.Email = "nuevocorreo@uv.mx";

            var personaDao = new PersonaDAO(TransacciónSql);

            personaDao.EditarPersona(persona);
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoMaternoConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoMaterno = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoMaternoLargo()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoMaterno = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoMaternoVacío()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoMaterno = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoPaternoConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoPaterno = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoPaternoLargo()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoPaterno = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConApellidoPaternoVacío()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoPaterno = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConEmailInválido()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Email = "??@213213.123";

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConEmailLargo()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Email = GetStringLargo(110);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConEmailVacío()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Email = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConNombreConCaracteresEspeciales()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Nombre = GetStringConCaracteresEspeciales();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConNombreLargo()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Nombre = GetStringLargo(60);

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaConNombreVacío()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Nombre = default;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaSinApellidoMaterno()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoMaterno = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaSinApellidoPaterno()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.ApellidoPaterno = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaSinEmail()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Email = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaSinGénero()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Género = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaSinNombre()
        {
            var persona = GetPersonaVálidoDesdeBaseDeDatos();
            persona.Nombre = null;

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }

        [Fact]
        public void Test_EditarPersona_NoSePuedeEditarPersonaVacío()
        {
            var persona = new Estudiante();

            var personaDao = new PersonaDAO(TransacciónSql);

            var exception = Assert.Throws<BusinessLogicException>(() => personaDao.EditarPersona(persona));
            Assert.Equal(BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA, exception.GetCode());
        }
    }
}