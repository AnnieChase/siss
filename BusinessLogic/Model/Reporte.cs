﻿using System;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Reporte representa un reporte mensual que es agregado por un técnico académico al expediente de un alumno
    /// </summary>
    public class Reporte
    {
        /// <summary>
        ///     Fecha límite de entrega de un reporte
        /// </summary>
        public DateTime FechaEntrega { get; set; }

        /// <summary>
        ///     Horas de servicio social que se reportan
        /// </summary>
        public int HorasServicioSocial { get; set; }

        /// <summary>
        ///     Número de reporte entregado
        /// </summary>
        public int NumeroReporte { get; set; }

        /// <summary>
        ///     Id del Reporte
        /// </summary>
        public int IdReporte { get; set; }
    }
}