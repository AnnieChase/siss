﻿using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Coordinador representa el coordinador de servicio social de un programa educativo
    /// </summary>
    public class Coordinador : Persona
    {
        /// <summary>
        ///     Usuario del coordinador
        /// </summary>
        [Required(ErrorMessage = "Los datos del usuario son requeridos.")]
        public Usuario Usuario { get; set; }

        /// <summary>
        ///     Número de personal del coordinador
        /// </summary>
        [Required(ErrorMessage = "El numero de personal es necesario")]
        [StringLength(50, ErrorMessage = "El numero de personal no puede ser mayor a 50 caracteres")]
        public string NúmeroPersonal { get; set; }

        /// <summary>
        ///     Programa educativo del coordinador
        /// </summary>
        [Required(ErrorMessage = "El Programa Educativo es necesario")]
        public ProgramaEducativo ProgramaEducativo { get; set; }

        /// <summary>
        ///     Estatus del coordinador
        /// </summary>
        [Required(ErrorMessage = "El estatus del Coordinador es necesario")]
        public Estatus? Estatus { get; set; } = Enum.Estatus.Activo;

        /// <summary>
        ///     Id del coordinador
        /// </summary>
        public int IdCoordinador { get; set; }
    }
}