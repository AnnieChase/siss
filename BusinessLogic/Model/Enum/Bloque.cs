﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador del bloque
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum Bloque
    {
        /// <summary>
        ///     Bloque numero 6
        /// </summary>
        [Description("Bloque 6")] Seis,

        /// <summary>
        ///     Bloque numero 7
        /// </summary>
        [Description("Bloque 7")] Siete,

        /// <summary>
        ///     Bloque numero 8
        /// </summary>
        [Description("Bloque 8")] Ocho,

        /// <summary>
        ///     Bloque numero 9
        /// </summary>
        [Description("Bloque 9")] Nueve,

        /// <summary>
        ///     Bloque numero 10
        /// </summary>
        [Description("Bloque 10")] Diez,

        /// <summary>
        ///     Bloque numero 11
        /// </summary>
        [Description("Bloque 11")] Once,

        /// <summary>
        ///     Bloque numero 12
        /// </summary>
        [Description("Bloque 12")] Doce
    }
}