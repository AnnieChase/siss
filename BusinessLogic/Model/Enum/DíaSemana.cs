﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de dias de la Ssemana
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum DíaSemana
    {
        /// <summary>
        ///     Dia de la semana lunes
        /// </summary>
        [Description("Lunes")] Lunes,

        /// <summary>
        ///     Dia de la semana martes
        /// </summary>
        [Description("Martes")] Martes,

        /// <summary>
        ///     Dia de la semana miercoles
        /// </summary>
        [Description("Miércoles")] Miércoles,

        /// <summary>
        ///     Dia de la semana jueves
        /// </summary>
        [Description("Jueves")] Jueves,

        /// <summary>
        ///     Dia de la semana viernes
        /// </summary>
        [Description("Viernes")] Viernes,

        /// <summary>
        ///     Dia de la semana sabado
        /// </summary>
        [Description("Sábado")] Sábado,

        /// <summary>
        ///     Dia de la semana domingo
        /// </summary>
        [Description("Domingo")] Domingo
    }
}