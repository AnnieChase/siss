﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de Seccion
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum Sección
    {
        /// <summary>
        ///     Seccion 1
        /// </summary>
        [Description("Sección 1")] Uno,

        /// <summary>
        ///     Seccion 2
        /// </summary>
        [Description("Sección 2")] Dos,

        /// <summary>
        ///     Seccion 3
        /// </summary>
        [Description("Sección 3")] Tres
    }
}