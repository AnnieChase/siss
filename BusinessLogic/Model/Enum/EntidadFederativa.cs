﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de Entidades Federativas
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum EntidadFederativa
    {
        /// <summary>
        ///     Entidad Federativa Aguascalientes
        /// </summary>
        [Description("Aguascalientes")] Aguascalientes,

        /// <summary>
        ///     Entidad Federativa BajaCalifornia
        /// </summary>
        [Description("Baja California")] BajaCalifornia,

        /// <summary>
        ///     Entidad Federativa BajaCaliforniaSur
        /// </summary>
        [Description("Baja California Sur")] BajaCaliforniaSur,

        /// <summary>
        ///     Entidad Federativa Campeche
        /// </summary>
        [Description("Campeche")] Campeche,

        /// <summary>
        ///     Entidad Federativa Chihuahua
        /// </summary>
        [Description("Chihuahua")] Chihuahua,

        /// <summary>
        ///     Entidad Federativa Ciudad de Mexico
        /// </summary>
        [Description("Ciudad de México")] CiudadDeMexico,

        /// <summary>
        ///     Entidad Federativa Coahuila
        /// </summary>
        [Description("Coahuila")] Coahuila,

        /// <summary>
        ///     Entidad Federativa Colima
        /// </summary>
        [Description("Colima")] Colima,

        /// <summary>
        ///     Entidad Federativa Durango
        /// </summary>
        [Description("Durango")] Durango,

        /// <summary>
        ///     Entidad Federativa Guanajuato
        /// </summary>
        [Description("Guanajuato")] Guanajuato,

        /// <summary>
        ///     Entidad Federativa Guerrero
        /// </summary>
        [Description("Guerrero")] Guerrero,

        /// <summary>
        ///     Entidad Federativa Hidalgo
        /// </summary>
        [Description("Hidalgo")] Hidalgo,

        /// <summary>
        ///     Entidad Federativa Jalisco
        /// </summary>
        [Description("Jalisco")] Jalisco,

        /// <summary>
        ///     Entidad Federativa Estado de Mexico
        /// </summary>
        [Description("Estado de México")] EstadoDeMexico,

        /// <summary>
        ///     Entidad Federativa Michoacan
        /// </summary>
        [Description("Michoacan")] Michoacan,

        /// <summary>
        ///     Entidad Federativa Morelos
        /// </summary>
        [Description("Morelos")] Morelos,

        /// <summary>
        ///     Entidad Federativa Nayarit
        /// </summary>
        [Description("Nayarit")] Nayarit,

        /// <summary>
        ///     Entidad Federativa NuevoLeon
        /// </summary>
        [Description("Nuevo León")] NuevoLeon,

        /// <summary>
        ///     Entidad Federativa Oaxaca
        /// </summary>
        [Description("Oaxaca")] Oaxaca,

        /// <summary>
        ///     Entidad Federativa Puebla
        /// </summary>
        [Description("Puebla")] Puebla,

        /// <summary>
        ///     Entidad Federativa Queretaro
        /// </summary>
        [Description("Querétaro")] Queretaro,

        /// <summary>
        ///     Entidad Federativa Quintana Roo
        /// </summary>
        [Description("Quintana Roo")] QuintanaRoo,

        /// <summary>
        ///     Entidad Federativa San Luis Potosi
        /// </summary>
        [Description("San Luis Potosí")] SanLuisPotosi,

        /// <summary>
        ///     Entidad Federativa Sinaloa
        /// </summary>
        [Description("Sinaloa")] Sinaloa,

        /// <summary>
        ///     Entidad Federativa Tabasco
        /// </summary>
        [Description("Tabasco")] Tabasco,

        /// <summary>
        ///     Entidad Federativa Tamaulipas
        /// </summary>
        [Description("Tamaulipas")] Tamaulipas,

        /// <summary>
        ///     Entidad Federativa Tlaxcala
        /// </summary>
        [Description("Tlaxcala")] Tlaxcala,

        /// <summary>
        ///     Entidad Federativa Veracruz
        /// </summary>
        [Description("Veracruz")] Veracruz,

        /// <summary>
        ///     Entidad Federativa Yucatan
        /// </summary>
        [Description("Yucatán")] Yucatan,

        /// <summary>
        ///     Entidad Federativa Zacatecas
        /// </summary>
        [Description("Zacatecas")] Zacatecas
    }
}