﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de géneros de Persona
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum Género
    {
        /// <summary>
        ///     Sexo masculino
        /// </summary>
        [Description("Masculino")] Masculino,

        /// <summary>
        ///     Sexo femenino
        /// </summary>
        [Description("Femenino")] Femenino,

        /// <summary>
        ///     Sexo indefinido
        /// </summary>
        [Description("Indefinido")] Indefinido
    }
}