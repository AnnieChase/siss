﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de tipo de Usuario
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum TipoUsuario
    {
        /// <summary>
        ///     Tipo de Usuario Coordinador
        /// </summary>
        [Description("Coordinador")] Coordinador,

        /// <summary>
        ///     Tipo de Usuario Director
        /// </summary>
        [Description("Director")] Director,

        /// <summary>
        ///     Tipo de Usuario Estudiante
        /// </summary>
        [Description("Estudiante")] Estudiante,

        /// <summary>
        ///     Tipo de Usuario Técnico Académico
        /// </summary>
        [Description("Técnico académico")] TécnicoAcadémico
    }
}