﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerado de meses del año
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum Mes
    {
        /// <summary>
        ///     Mes del año Enero
        /// </summary>
        [Description("Enero")] Enero,

        /// <summary>
        ///     Mes del año Febrero
        /// </summary>
        [Description("Febrero")] Febrero,

        /// <summary>
        ///     Mes del año Marzo
        /// </summary>
        [Description("Marzo")] Marzo,

        /// <summary>
        ///     Mes del año Abril
        /// </summary>
        [Description("Abril")] Abril,

        /// <summary>
        ///     Mes del año Mayo
        /// </summary>
        [Description("Mayo")] Mayo,

        /// <summary>
        ///     Mes del año Junio
        /// </summary>
        [Description("Junio")] Junio,

        /// <summary>
        ///     Mes del año Julio
        /// </summary>
        [Description("Julio")] Julio,

        /// <summary>
        ///     Mes del año Agosto
        /// </summary>
        [Description("Agosto")] Agosto,

        /// <summary>
        ///     Mes del año Septiembre
        /// </summary>
        [Description("Septiembre")] Septiembre,

        /// <summary>
        ///     Mes del año Octubre
        /// </summary>
        [Description("Octubre")] Octubre,

        /// <summary>
        ///     Mes del año Noviembre
        /// </summary>
        [Description("Noviembre")] Noviembre,

        /// <summary>
        ///     Mes del año Diciembre
        /// </summary>
        [Description("Diciembre")] Diciembre
    }
}