﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de estatus de activacion
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum Estatus
    {
        /// <summary>
        ///     Estatus activo
        /// </summary>
        [Description("Activo")] Activo,

        /// <summary>
        ///     Estatus inactivo
        /// </summary>
        [Description("Inactivo")] Inactivo
    }
}