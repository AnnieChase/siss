﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de estatus del alumno
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum EstatusEstudiante
    {
        /// <summary>
        ///     Estatus espera
        /// </summary>
        [Description("En espera")] Espera,

        /// <summary>
        ///     Estatus aceptado
        /// </summary>
        [Description("Aceptado")] Aceptado,

        /// <summary>
        ///     Estatus rechazado
        /// </summary>
        [Description("Rechazado")] Rechazado,

        /// <summary>
        ///     Estatus baja
        /// </summary>
        [Description("Baja")] Baja
    }
}