﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de tipo de documento
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum TipoDocumento
    {
        /// <summary>
        ///     Tipo de documento Carta de aceptacion
        /// </summary>
        [Description("Carta de aceptación")] CartaAceptacion,

        /// <summary>
        ///     Tipo de documento Carta de liberacion
        /// </summary>
        [Description("Carta de liberación")] CartaLiberacion,

        /// <summary>
        ///     Tipo de documento Registro y Plan de Actividades
        /// </summary>
        [Description("Registro y Plan de Actividades")]
        RegistroYPlanDeActividades,

        /// <summary>
        ///     Tipo de documento Memoria
        /// </summary>
        [Description("Memoria")] Memoria,

        /// <summary>
        ///     Tipo de documento Carta de autorizacion
        /// </summary>
        [Description("Carta de autorización")] CartaDeAutorizacion,

        /// <summary>
        ///     Tipo de documento Solicitud de servicio social
        /// </summary>
        [Description("Solicitud de Servicio Social")]
        SolicitudServicioSocial
    }
}