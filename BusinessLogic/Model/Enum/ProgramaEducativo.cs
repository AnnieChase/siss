﻿using System.ComponentModel;
using BusinessLogic.Util;

namespace BusinessLogic.Model.Enum
{
    /// <summary>
    ///     Enumerador de programa educativo
    /// </summary>
    [TypeConverter(typeof(EnumTypeConverter))]
    public enum ProgramaEducativo
    {
        /// <summary>
        ///     Programa Educativo Ingenieria de Software
        /// </summary>
        [Description("Ingeniería de Software")]
        IngenieriaDeSoftware,

        /// <summary>
        ///     Programa Educativo Redes y Servicios de Computo
        /// </summary>
        [Description("Redes y Servicios de Cómputo")]
        RedesyServiciosDeComputo,

        /// <summary>
        ///     Programa Educativo Estadistica
        /// </summary>
        [Description("Estadística")] Estadistica,

        /// <summary>
        ///     Programa Educativo Tecnologias Computacionales
        /// </summary>
        [Description("Tecnologías Computacionales")]
        TecnologiasComputacionales
    }
}