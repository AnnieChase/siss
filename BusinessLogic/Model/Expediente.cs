﻿using System;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Expediente representa el expediente de un alumno que realiza su servicio social
    /// </summary>
    public class Expediente
    {
        /// <summary>
        ///     Fecha en la que se crea el expediente
        /// </summary>
        public DateTime FechaCreaciónExpediente { get; set; }

        /// <summary>
        ///     Id del expediente
        /// </summary>
        public int IdExpediente { get; set; }
    }
}