﻿using System.Collections.Generic;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Solicitud representa un solicitud de servicio social hecha por el estudiante para participar en uno de
    ///     tres proyectos
    /// </summary>
    public class Solicitud
    {
        /// <summary>
        ///     Id de la Solicitud
        /// </summary>
        public int IdSolicitud { get; set; }

        /// <summary>
        ///     Lista que contiene los proyectos en los que el estudiante desea participar
        /// </summary>
        public List<Proyecto> Proyectos { get; set; }
    }
}