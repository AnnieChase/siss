﻿using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Director representa el director de facultad en el SISS
    /// </summary>
    public class Director : Persona
    {
        /// <summary>
        ///     Usuario del director
        /// </summary>
        [Required(ErrorMessage = "Los datos del usuario son requeridos.")]
        public Usuario Usuario { get; set; }

        /// <summary>
        ///     Id del directos
        /// </summary>
        public int IdDirector { get; set; }

        /// <summary>
        ///     Número de personal del director
        /// </summary>
        [Required(ErrorMessage = "El número de personal del director es requerido.")]
        [StringLength(50, ErrorMessage = "La número de personal del director no puede ser mayor a 50 caracteres.")]
        public string NúmeroPersonal { get; set; }

        /// <summary>
        ///     Estatus del director
        /// </summary>
        [Required(ErrorMessage = "El estatus del director es requerido.")]
        public Estatus? Estatus { get; set; } = Enum.Estatus.Activo;
    }
}