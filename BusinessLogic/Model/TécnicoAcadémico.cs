﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Ténico Académico representa un usuario técnico académico que utilizará el SISS
    /// </summary>
    public class TécnicoAcadémico : Persona
    {
        /// <summary>
        ///     Usuario del técnico académico
        /// </summary>
        [Required(ErrorMessage = "Los datos del usuario son requeridos.")]
        public Usuario Usuario { get; set; }

        /// <summary>
        ///     Id del técnico académico
        /// </summary>
        public int IdTécnicoAcadémico { get; set; }

        /// <summary>
        ///     Número de personal del técnico académico
        /// </summary>
        [Required(ErrorMessage = "El numero de personal es necesario")]
        [StringLength(50, ErrorMessage = "El numero de personal no puede ser mayor a 50 caracteres")]
        public string NúmeroPersonal { get; set; }

        /// <summary>
        ///     Programa educativo del técnico académico
        /// </summary>
        [Required(ErrorMessage = "El programa educativo de técnico académico es requerido.")]
        public ProgramaEducativo ProgramaEducativo { get; set; }

        /// <summary>
        ///     Estatus del técnico académico
        /// </summary>
        [Required(ErrorMessage = "El estatus de técnico académico es requerido.")]
        public Estatus? Estatus { get; set; }

        /// <summary>
        ///     Horario de atención del técnico académico
        /// </summary>
        public List<Horario> Horarios { get; set; }
    }
}