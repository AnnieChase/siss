﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Usuario representa un usuario dentro del SISS que es utilizado por los modelos; director, coordinador,
    ///     técnico académico y estudiante
    /// </summary>
    public class Usuario : IValidatableObject
    {
        /// <summary>
        ///     Id del Usuario
        /// </summary>
        public int IdUsuario { get; set; }

        /// <summary>
        ///     Nombre del usuario
        /// </summary>
        [Required(ErrorMessage = "El nombre de usuario es requerido.")]
        [StringLength(100, ErrorMessage = "El nombre de usuario no puede ser mayor a 100 caracteres.")]
        public string NombreUsuario { get; set; }

        /// <summary>
        ///     Contraseña del usuario
        /// </summary>
        [StringLength(200, ErrorMessage = "La contraseña no puede ser mayor a 200 caracteres.")]
        public string Contraseña { get; set; }

        /// <summary>
        ///     Tipo de usuario
        /// </summary>
        [Required(ErrorMessage = "El tipo de usuario es requerido.")]
        public TipoUsuario? TipoUsuario { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var matrícula = new Matrícula();
            if (TipoUsuario != Enum.TipoUsuario.Estudiante)
            {
                if (matrícula.IsValid(NombreUsuario))
                {
                    yield return new ValidationResult(
                        "El nombre de usuario no puede tener el formato de una matrícula.");
                }
            }
            else
            {
                if (!matrícula.IsValid(NombreUsuario))
                {
                    yield return new ValidationResult("El nombre de usuario debe tener el formato de una matrícula.");
                }
            }
        }
    }
}