﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Asinación representa la asignación de un alumno a un proyecto de servicio social
    /// </summary>
    public class Asignación
    {
        /// <summary>
        ///     Id de la asignación
        /// </summary>
        public int IdAsignación { get; set; }

        /// <summary>
        ///     Fecha en la que se realiza la asignación
        /// </summary>
        [Required(ErrorMessage = "La fecha de asignación es requerida")]
        public DateTime FechaAsignación { get; set; }
    }
}