using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Encargado representa al encargado de una organizaci�n en el SISS
    /// </summary>
    public class Encargado : Persona
    {
        /// <summary>
        ///     Cargo o posici�n que tiene el encargado dentro de la organizaci�n
        /// </summary>
        [Required(ErrorMessage = "El cargo de la persona es requerido")]
        [StringLength(60, ErrorMessage = "El cargo de la persona no puede ser mayor a 60 caracteres")]
        [SoloLetras(ErrorMessage = "Sólo se permiten espacios y letras en el cargo de la persona")]
        public string Cargo { get; set; }

        /// <summary>
        ///     Extensi�n telef�nica para contactar al encargado
        /// </summary>
        [Required(ErrorMessage =
            "La extensión telefónica para el contacto al encargado de la organización es requerido")]
        [StringLength(10, ErrorMessage = "La extensión telefónica no debe ser mayor a 10 caracteres")]
        public string ExtensiónTelefónica { get; set; }

        /// <summary>
        ///     Id del encargado
        /// </summary>
        public int IdEncargado { get; set; }

        /// <summary>
        ///     Tipo de Estatus qeu tiene el Encargdo en el SISS
        /// </summary>
        [Required(ErrorMessage = "El estatus del encargado es requerido")]
        public Estatus? Estatus { get; set; }

        /// <summary>
        ///     Id de la Organizacion que pertenece el Encargado
        /// </summary>
        [Required(ErrorMessage = "El Id de la organización es requerido")]
        public int IdOrganización { get; set; }


        /// <summary>
        ///     Nombre de la organización a la que pertenece el encargado
        /// </summary>
        public string NombreOrganización { get; set; }
    }
}