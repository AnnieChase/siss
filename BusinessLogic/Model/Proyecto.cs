using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Proyecto representa un proyecto de serivcio social
    /// </summary>
    public class Proyecto
    {
        /// <summary>
        ///     Id del Proyecto
        /// </summary>
        public int IdProyecto { get; set; }

        /// <summary>
        ///     Id del Encargado
        /// </summary>
        public int IdEncargado { get; set; }

        /// <summary>
        ///     Id de la Organización
        /// </summary>
        public int IdOrganización { get; set; }

        /// <summary>
        ///     Id del Coordinador
        /// </summary>
        public int IdCoordinador { get; set; }

        /// <summary>
        ///     Nombre del Proyecto
        /// </summary>
        [Required(ErrorMessage = "El nombre del proyecto es requerido")]
        [StringLength(200, ErrorMessage = "El nombre del proyecto no debe ser mayor a 200 caracteres")]
        public string NombreProyecto { get; set; }

        /// <summary>
        ///     Actividades y funciones dentro del Proyecto
        /// </summary>
        [Required(ErrorMessage = "Las actividades y funciones del proyecto son requeridos")]
        [StringLength(8000, ErrorMessage =
            "Las actividades y funciones del proyecto no deben ser mayor a 8000 caracteres")]
        public string ActividadesFunciones { get; set; }

        /// <summary>
        ///     Cantidad de estudiantes que se solicitan para el Proyecto
        /// </summary>
        [Required(ErrorMessage = "Los estudiantes solicitados del proyecto son requerido")]
        public int EstudiantesSolicitados { get; set; }

        /// <summary>
        ///     Descripción general del Proyecto
        /// </summary>
        [Required(ErrorMessage = "La descripción del proyecto es requerido")]
        [StringLength(8000, ErrorMessage = "La descripción del proyecto no debe ser mayor a 8000 caracteres")]
        public string DescripciónGeneral { get; set; }

        /// <summary>
        ///     Objetivos generales del Proyecto
        /// </summary>
        [Required(ErrorMessage = "Los Objetivos Generales del proyecto son requerido")]
        [StringLength(8000, ErrorMessage = "Los Objetivos del proyecto no debe ser mayor a 8000 caracteres")]
        public string ObjetivosGenerales { get; set; }

        /// <summary>
        ///     Objetivos inmediatos del Proyecto
        /// </summary>
        [Required(ErrorMessage = "Los Objetivos Inmediatos del proyecto son requerido")]
        [StringLength(8000, ErrorMessage = "Los Objetivos Inmediatos del proyecto no debe ser mayor a 8000 caracteres")]
        public string ObjetivosInmediatos { get; set; }

        /// <summary>
        ///     Metodología a utilizar en el Proyecto
        /// </summary>
        [Required(ErrorMessage = "La metodologia del proyecto es requerido")]
        [StringLength(100, ErrorMessage = "La metodologia del proyecto no debe ser mayor a 100 caracteres")]
        public string Metodología { get; set; }

        /// <summary>
        ///     Objetivos mediatos del Proyecto
        /// </summary>
        [Required(ErrorMessage = "Los Objetivos Mediatos del proyecto son requerido")]
        [StringLength(8000, ErrorMessage = "Los Objetivos Mediatos del proyecto no debe ser mayor a 8000 caracteres")]
        public string ObjetivosMediatos { get; set; }

        /// <summary>
        ///     Recursos humanos, económicos y materiales del Proyecto
        /// </summary>
        [Required(ErrorMessage = "Los recursos del proyecto son requerido")]
        [StringLength(8000, ErrorMessage = "Los recursos del proyecto no deben ser mayor a 8000 caracteres")]
        public string Recursos { get; set; }

        /// <summary>
        ///     Resposabilidades del Estudiante en el Proyecto
        /// </summary>
        [Required(ErrorMessage = "Las responsabilidades del proyecto son requerido")]
        [StringLength(8000, ErrorMessage = "Las responsabilidades del proyecto no debe ser mayor a 8000 caracteres")]
        public string Responsabilidades { get; set; }


        /// <summary>
        ///     Duración aproximado del Proyecto
        /// </summary>
        [Required(ErrorMessage = "La duración del proyecto es requerido")]
        [StringLength(100, ErrorMessage = "La duración del proyecto no debe ser mayor a 100 caracteres")]
        public string Duración { get; set; }

        /// <summary>
        ///     Horario probable del Proyecto
        /// </summary>
        [Required(ErrorMessage = "El Horario probable del proyecto es requerido")]
        [StringLength(500, ErrorMessage = "El Horario probable del proyecto no debe ser mayor a 500 caracteres")]
        public string HorarioProbable { get; set; }

        /// <summary>
        ///     Estatus del Proyecto
        /// </summary>
        [Required(ErrorMessage = "El estatus del proyecto es requerido")]
        public Estatus Estatus { get; set; }
    }
}