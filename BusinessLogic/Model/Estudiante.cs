﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Estudiante representa un estudiante que utiliza el SISS
    /// </summary>
    public class Estudiante : Persona, IValidatableObject
    {
        /// <summary>
        ///     Usuario del estudiante
        /// </summary>
        [Required(ErrorMessage = "Los datos del usuario son requeridos.")]
        public Usuario Usuario { get; set; }

        /// <summary>
        ///     Id del Estudiante
        /// </summary>
        public int IdEstudiante { get; set; }

        /// <summary>
        ///     Matrícula del estudiante
        /// </summary>
        [Required(ErrorMessage = "La matrícula del estudiante es requerida.")]
        [StringLength(50, ErrorMessage = "La matrícula del estudiante no puede ser mayor a 50 caracteres.")]
        [Matrícula(ErrorMessage = "La matrícula tiene un formato incorrecto.")]
        public string Matrícula { get; set; }

        /// <summary>
        ///     Programa educativo del estudiante
        /// </summary>
        [Required(ErrorMessage = "El programa educativo del estudiante es requerido.")]
        public ProgramaEducativo? ProgramaEducativo { get; set; }

        /// <summary>
        ///     Bloque del estudiante
        /// </summary>
        [Required(ErrorMessage = "El bloque del estudiante es requerido.")]
        public Bloque? Bloque { get; set; }

        /// <summary>
        ///     Sección o semestre en el que se encuentra cursando el estudiante
        /// </summary>
        [Required(ErrorMessage = "La sección del estudiante es requerida.")]
        public Sección? Sección { get; set; }

        /// <summary>
        ///     Periodo escolar que se encuentra cursando el estudiante
        /// </summary>
        [Required(ErrorMessage = "El periodo escolar del estudiante es requerido.")]
        [StringLength(50, ErrorMessage = "El periodo escolar del estudiante no puede ser mayor a 50 caracteres.")]
        public string PeriodoEscolar { get; set; }

        /// <summary>
        ///     Estatus del estudiante
        /// </summary>
        [Required(ErrorMessage = "El estatus del estudiante es requerido.")]
        public EstatusEstudiante? EstatusEstudiante { get; set; } = Enum.EstatusEstudiante.Espera;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Matrícula != Usuario.NombreUsuario)
            {
                yield return new ValidationResult("La matrícula y el nombre de usuario deben ser iguales.");
            }

            /*if (!Validator.EsMatriculaValida(Matrícula))
            {
                yield return new ValidationResult("Matrícula inválida");
            }*/
        }
    }
}