﻿using System;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Documento representa un document o formato
    /// </summary>
    public class Documento
    {
        /// <summary>
        ///     Tipo de documento
        /// </summary>
        public TipoDocumento TipoDocumento { get; set; }


        /// <summary>
        ///     Fecha límite de entrega del  documento
        /// </summary>
        public DateTime FechaEntrega { get; set; }
    }
}