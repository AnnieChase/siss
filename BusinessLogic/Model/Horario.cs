﻿using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El model Horario representa un horario de atención o asistencia que será utilizado por los modelos técnico
    ///     académico y proyecto
    /// </summary>
    public class Horario
    {
        /// <summary>
        ///     Día de la semana
        /// </summary>
        [Required(ErrorMessage = "El día de la semana es necesario")]
        public DíaSemana Día { get; set; }

        /// <summary>
        ///     Hora de finalización
        /// </summary>
        [Required(ErrorMessage = "El Horario Final es necesario")]
        [StringLength(50, ErrorMessage = "El horario de inicio ser mayor a 50 caracteres")]
        public string HoraFinal { get; set; }

        /// <summary>
        ///     Hora de inicio
        /// </summary>
        [Required(ErrorMessage = "El Horario de Inicio es necesario")]
        [StringLength(50, ErrorMessage = "El horario de incio no puede ser mayor a 50 caracteres")]
        public string HoraInicio { get; set; }

        /// <summary>
        ///     Ubicación o lugar
        /// </summary>
        [Required(ErrorMessage = "La ubicación es necesario")]
        [StringLength(150, ErrorMessage = "La Ubicación no puede ser mayor a 150 caracteres")]
        public string Ubicación { get; set; }

        /// <summary>
        ///     Id del horario
        /// </summary>
        public int IdHorario { get; set; }
    }
}