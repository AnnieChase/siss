﻿using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Persona representa a una persona que es usuario directo o indirecto de SISS
    /// </summary>
    public abstract class Persona
    {
        /// <summary>
        ///     Id de la persona
        /// </summary>
        public int IdPersona { get; set; }

        /// <summary>
        ///     Nombre de la persona
        /// </summary>
        [Required(ErrorMessage = "El nombre de la persona es requerido.")]
        [StringLength(50, ErrorMessage = "El nombre de la persona no puede ser mayor a 50 caracteres.")]
        [SoloLetras(ErrorMessage = "Solo se permiten letras y espacios en el nombre de la persona.")]
        public string Nombre { get; set; }

        /// <summary>
        ///     Apellido paterno de la persona
        /// </summary>
        [Required(ErrorMessage = "El apellido paterno de la persona es requerido.")]
        [StringLength(50, ErrorMessage = "El apellido paterno de la persona no puede ser mayor a 50 caracteres.")]
        [SoloLetras(ErrorMessage = "Solo se permiten letras y espacios en el apellido paterno de la persona.")]
        public string ApellidoPaterno { get; set; }

        /// <summary>
        ///     Apellido materno de la persona
        /// </summary>
        [Required(ErrorMessage = "El apellido materno de la persona es requerido.")]
        [StringLength(50, ErrorMessage = "El apellido materno de la persona no puede ser mayor a 50 caracteres.")]
        [SoloLetras(ErrorMessage = "Solo se permiten letras y espacios en el apellido materno de la persona.")]
        public string ApellidoMaterno { get; set; }

        /// <summary>
        ///     Email o correo electrónico de la persona
        /// </summary>
        [Required(ErrorMessage = "El correo electrónico de la persona es requerido.")]
        [StringLength(100, ErrorMessage = "El correo electrónico de la persona no puede ser mayor a 100 caracteres.")]
        [EmailAddress(ErrorMessage = "El correo electrónico de la persona tiene un formato incorrecto.")]
        public string Email { get; set; }

        /// <summary>
        ///     Género de la persona
        /// </summary>
        [Required(ErrorMessage = "El género de la persona es requerido.")]
        public Género? Género { get; set; }

        /// <summary>
        ///     Recupera el nombre completo de una persona
        /// </summary>
        /// <returns>El nombre completo de una persona</returns>
        public string GetNombreCompleto()
        {
            return Nombre + " " + ApellidoPaterno + " " + ApellidoMaterno;
        }
    }
}