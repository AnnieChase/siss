﻿using System.ComponentModel.DataAnnotations;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;

namespace BusinessLogic.Model
{
    /// <summary>
    ///     El modelo Organización representa una organización dentro del SISS
    /// </summary>
    public class Organización
    {
        /// <summary>
        ///     Ciudad en la que se encuentra localizada la organización
        /// </summary>
        [Required(ErrorMessage = "La ciudad en la que se encuentra ubicada la organización es requerida")]
        [StringLength(50, ErrorMessage = "El nombre de la ciudad no debe ser mayor a 50 caracteres")]
        [SoloLetras(ErrorMessage = "Sólo se permiten letras y espacios en el nombre de la ciudad")]
        public string Ciudad { get; set; }

        /// <summary>
        ///     Dirección de la organización
        /// </summary>
        [Required(ErrorMessage = "La dirección de la organización es requerida")]
        [StringLength(250, ErrorMessage = "La direccipon de la organización no debe ser mayor a 250 caracteres")]
        public string DirecciónOrganización { get; set; }

        /// <summary>
        ///     Email o correo electrónico de contacto de la organización
        /// </summary>
        [Required(ErrorMessage = "El correo electrónico de la organización es requerido")]
        [StringLength(100, ErrorMessage =
            "El correo electrónico de la organización no debe ser mayor a 100 caracteres")]
        [EmailAddress(ErrorMessage = "El correo electrónico de la organización tiene un formato incorrecto")]
        public string Email { get; set; }

        /// <summary>
        ///     Nombre de la organización
        /// </summary>
        [Required(ErrorMessage = "El nombre de la organización es requerido")]
        [StringLength(150, ErrorMessage = "El nombre de la organización no debe ser mayor a 150 caracteres")]
        [SoloLetras(ErrorMessage = "Sólo se permiten letras y espacios en el nombre de la organización")]
        public string NombreOrganización { get; set; }

        /// <summary>
        ///     Sector de la organización
        /// </summary>
        [Required(ErrorMessage = "El sector a que pertenece la organización es requerido")]
        [StringLength(50, ErrorMessage =
            "El nombre del sector al que pertenece la organización no debe ser mayor a 50 caracteres")]
        [SoloLetras(ErrorMessage = "Sólo se permiten letras y espacios en el sector de la organización")]
        public string Sector { get; set; }

        /// <summary>
        ///     Entidad federativa o estado en el que se encuentra localizada la organización
        /// </summary>
        [Required(ErrorMessage = "La entidad federativa en la que se encuentra la organización es requerida")]
        public EntidadFederativa EntidadFederativa { get; set; }

        /// <summary>
        ///     Teléfono de contacto de la organización
        /// </summary>
        [Required(ErrorMessage = "El número de teléfono de contacto de la organización es requerido")]
        [StringLength(50, ErrorMessage = "El número de teléfono de la organización no debe ser mayor a 50 caracteres")]
        [Phone(ErrorMessage = "El número de teléfono de la organización tiene un formato incorrecto")]
        public string Teléfono { get; set; }

        /// <summary>
        ///     Id de la organización
        /// </summary>
        public int IdOrganización { get; set; }

        /// <summary>
        ///     Estatus de la organización
        /// </summary>
        [Required(ErrorMessage = "El estatus de la organización es requerido")]
        public Estatus? Estatus { get; set; }

        public override string ToString()
        {
            return NombreOrganización;
        }
    }
}