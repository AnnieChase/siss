using System;
using System.Data.SqlClient;
using System.Diagnostics;
using BusinessLogic.Util;

namespace BusinessLogic.Excepción
{
    public sealed class BusinessLogicException : Exception
    {
        private readonly BusinessLogicExceptionCode _businessLogicExceptionCode;

        internal BusinessLogicException()
        {
        }

        internal BusinessLogicException(string message) : base(message)
        {
        }

        internal BusinessLogicException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BusinessLogicException(string message, BusinessLogicExceptionCode businessLogicExceptionCode) :
            base(message)
        {
            _businessLogicExceptionCode = businessLogicExceptionCode;
            RegistrarExcepciónEnArchivo(message, new StackTrace(1).ToString());
        }

        public BusinessLogicException(SqlException sqlException) : base(sqlException.Message, sqlException)
        {
            var number = 0;
            if (sqlException.Errors.Count > 0)
            {
                var error = sqlException.Errors[0];
                number = error.Number;
                switch (number)
                {
                    // http://www.sql-server-helper.com/error-messages/msg-1-500.aspx
                    case 53:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.NO_SE_PUDO_CONECTAR_AL_SERVIDOR_SQL;
                        break;
                    case 102:
                        _businessLogicExceptionCode =
                            BusinessLogicExceptionCode.ERROR_DE_SINTAXIS;
                        break;
                    case 109:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.INSERT_MÁS_COLUMNAS_QUE_VALORES;
                        break;
                    case 110:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.INSERT_MENOS_COLUMNAS_QUE_VALORES;
                        break;
                    case 137:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.PARÁMETRO_ESCALAR_SIN_DECLARAR;
                        break;
                    case 156:
                        _businessLogicExceptionCode =
                            BusinessLogicExceptionCode.ERROR_DE_SINTAXIS_CERCA_DE_PALABRA_CLAVE;
                        break;
                    case 207:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.NOMBRE_COLUMNA_INVÁLIDA;
                        break;
                    case 208:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.NOMBRE_OBJETO_INVÁLIDO;
                        break;
                    case 245:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.PARÁMETRO_DIFERENTE_TIPO_DE_DATO;
                        break;
                    case 2601:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO;
                        break;
                    case 4145:
                        _businessLogicExceptionCode =
                            BusinessLogicExceptionCode.CONDICIÓN_NO_BOOLEANA;
                        break;
                    case 8114:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.ERROR_CONVERSIÓN_TIPOS_DE_DATOS;
                        break;
                    case 8152:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.VALORES_DEMASIADO_LARGOS;
                        break;
                    case 8178:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.VALORES_FALTANTES;
                        break;
                    case 18456:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.SQL_LOGIN_FAIL;
                        break;
                    default:
                        _businessLogicExceptionCode = BusinessLogicExceptionCode.ERROR_GENÉRICO_DE_BASE_DE_DATOS;
                        break;
                }
            }
            else
            {
                _businessLogicExceptionCode = BusinessLogicExceptionCode.ERROR_GENÉRICO_DE_BASE_DE_DATOS;
            }

            RegistrarExcepciónEnArchivo(sqlException.Message + " [" + number + "]", sqlException.StackTrace);
        }

        public BusinessLogicException(string message, BusinessLogicExceptionCode businessLogicExceptionCode,
            Exception innerException) : base(message, innerException)
        {
            _businessLogicExceptionCode = businessLogicExceptionCode;
            RegistrarExcepciónEnArchivo(message, new StackTrace(1).ToString());
        }

        public BusinessLogicExceptionCode GetCode()
        {
            return _businessLogicExceptionCode;
        }

        private void RegistrarExcepciónEnArchivo(string message, string stacktrace)
        {
            var logger = new Logger();
            var code = GetCode();
            logger.AgregarExcepciónARegistro(message, code, stacktrace);
        }
    }
}