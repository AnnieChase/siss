using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace BusinessLogic.Validación
{
    public class SoloLetras : ValidationAttribute
    {
        public override bool IsValid(object cadena)
        {
            return Regex.IsMatch((string) cadena, @"^[a-zñA-ZÑáéíóúÁÉÍÓÚ ]*$");
        }
    }
}