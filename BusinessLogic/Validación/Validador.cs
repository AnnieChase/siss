using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BusinessLogic.Excepción;

namespace BusinessLogic.Validación
{
    public static class Validador
    {
        public static void Validar(this object source, string encabezadoError,
            BusinessLogicExceptionCode businessLogicExceptionCode)
        {
            var valContext = new ValidationContext(source, null, null);
            var result = new List<ValidationResult>();
            Validator.TryValidateObject(source, valContext, result, true);
            if (result.Any())
            {
                var mensajeError = encabezadoError + "\n\n";
                foreach (var error in result)
                {
                    mensajeError += "\t- " + error.ErrorMessage + "\n";
                }

                throw new BusinessLogicException(mensajeError, businessLogicExceptionCode);
            }
        }
    }
}