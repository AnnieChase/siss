using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace BusinessLogic.Validación
{
    public class Matrícula : ValidationAttribute
    {
        public override bool IsValid(object matrícula)
        {
            return Regex.IsMatch((string) matrícula, @"^([Ss]\d{8})$");
        }
    }
}