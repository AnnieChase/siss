﻿using System.Text.RegularExpressions;

namespace BusinessLogic.Util
{
    public static class Validator
    {
        /// <summary>
        ///     Realiza la validación de un email
        /// </summary>
        /// <param name="email">Email a verificar</param>
        /// <returns>Regresa verdadero si es un email válido</returns>
        public static bool EsEmailValido(string email)
        {
            return Regex.IsMatch(email,
                @"^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$");
        }

        /// <summary>
        ///     Realiza la validación de un nombre
        /// </summary>
        /// <param name="nombre">Nombre a verificar</param>
        /// <returns>Regresa verdadero si es un nombre válido</returns>
        public static bool EsNombreValido(string nombre)
        {
            return Regex.IsMatch(nombre, @"^[A-Za-z]*\s()[A-Za-z]*$");
        }

        /// <summary>
        ///     Realiza la validación de una matrícula
        /// </summary>
        /// <param name="matrícula">Matrícula a verificar</param>
        /// <returns>Regresa verdadero si es una matrícula válida</returns>
        public static bool EsMatriculaValida(string matrícula)
        {
            return Regex.IsMatch(matrícula, @"^([Ss]\d{8})$");
        }

        /// <summary>
        ///     Realiza la validación de un número telefónico
        /// </summary>
        /// <param name="número">Número telefónico a verificar</param>
        /// <returns>Regresa verdadero si es un número telefónico válido</returns>
        public static bool EsNumeroValido(string número)
        {
            return Regex.IsMatch(número, @"^\d{10}$");
        }

        /// <summary>
        ///     Realiza la validación de una contraseña
        /// </summary>
        /// <param name="contraseña">Contraseña a verificar</param>
        /// <returns>Regresa verdadero si es una contraseña válida</returns>
        public static bool EsContraseniaValida(string contraseña)
        {
            return Regex.IsMatch(contraseña, @"((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,})");
        }
    }
}