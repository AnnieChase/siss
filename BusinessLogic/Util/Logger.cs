using System;
using System.IO;
using BusinessLogic.Excepción;

namespace BusinessLogic.Util
{
    public class Logger
    {
        private const string LogFilename = "SISS.log";

        public void AgregarExcepciónARegistro(string message, BusinessLogicExceptionCode code, string stacktrace)
        {
            using (var streamWriter = new StreamWriter(LogFilename, true))
            {
                streamWriter.Write(DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss") + " " + message + " (" + code +
                                   ")");
                streamWriter.WriteLine();
                streamWriter.Write(stacktrace);
                streamWriter.WriteLine();
                streamWriter.Close();
            }
        }
    }
}