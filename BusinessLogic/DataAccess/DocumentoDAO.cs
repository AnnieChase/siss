﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Database;

namespace BusinessLogic.DataAccess
{
    public class DocumentoDAO : BaseDAO, IDocumentoDAO
    {
        public DocumentoDAO()
        {
        }

        public DocumentoDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarDocumento(Documento documento, int idExpediente)
        {
            var comandoSQL = CrearComandoSQL("INSERT INTO Documento(FechaEntrega, TipoDocumento, IdExpediente)" +
                                             "VALUES(@FechaEntrega, @TipoDocumento, @IdExpediente);");
            comandoSQL.AgregarParámetro("@FechaEntrega", documento.FechaEntrega.ToString("MM/dd/yyyy"));
            comandoSQL.AgregarParámetro("@TipoDocumento", documento.TipoDocumento.ToString());
            comandoSQL.AgregarParámetro("@IdExpediente", idExpediente);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el documento.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_DOCUMENTO);
            }

            var idDocumento = comandoSQL.GetÚltimoIdInsertado();

            return idDocumento;
        }

        public void EditarDocumento(Documento documento)
        {
            var comandoSQL = CrearComandoSQL("UPDATE Documento" +
                                             "SET FechaEntrega = @FechaEntrega, TipoDocumento = @TipoDocumento" +
                                             "WHERE IdTecnicoAcademico = @IdTecnicoAcademico;");

            comandoSQL.AgregarParámetro("@FechaEntrega", documento.FechaEntrega.ToString("MM/dd/yyyy"));
            comandoSQL.AgregarParámetro("@TipoDocumento", documento.TipoDocumento.ToString());

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el documento.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_DOCUMENTO);
            }
        }

        public List<Documento> GetDocumentos()
        {
            var documentos = new List<Documento>();

            var comandoSQL = CrearComandoSQL(
                "SELECT Doc.FechaEntrega, " +
                "Doc.TipoDocumento " +
                "FROM Documento AS Doc; "
            );

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var documento = new Documento
                {
                    FechaEntrega = row.Field<DateTime>("FechaEntrega"),
                    TipoDocumento =
                        (TipoDocumento) Enum.Parse(typeof(TipoDocumento), row.Field<string>("TipoDocumento"))
                };
                documentos.Add(documento);
            }

            return documentos;
        }
    }
}