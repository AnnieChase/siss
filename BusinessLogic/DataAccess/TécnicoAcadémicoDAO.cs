﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class TécnicoAcadémicoDAO : BaseDAO, ITécnicoAcadémicoDAO
    {
        public TécnicoAcadémicoDAO()
        {
        }

        public TécnicoAcadémicoDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico)
        {
            if (técnicoAcadémico.Usuario != null)
            {
                técnicoAcadémico.Usuario.TipoUsuario = TipoUsuario.TécnicoAcadémico;
            }

            técnicoAcadémico.Validar("Error(es) al validar el técnico académico:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_TÉCNICO_ACADÉMICO);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            var usuarioDao = new UsuarioDAO(transacciónSql);
            var horarioDao = new HorarioDAO(transacciónSql);

            var idPersona = personaDao.AgregarPersona(técnicoAcadémico);
            var idUsuario = usuarioDao.AgregarUsuario(técnicoAcadémico.Usuario);

            var comandoSQL =
                CrearComandoSQL(
                    "INSERT INTO TecnicoAcademico (IdPersona, IdUsuario, NumeroPersonal, ProgramaEducativo, Estatus) VALUES (@IdPersona, @IdUsuario, @NumeroPersonal, @ProgramaEducativo, @Estatus);");

            comandoSQL.AgregarParámetro("@IdPersona", idPersona);
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);
            comandoSQL.AgregarParámetro("@NumeroPersonal", técnicoAcadémico.NúmeroPersonal);
            comandoSQL.AgregarParámetro("@ProgramaEducativo", técnicoAcadémico.ProgramaEducativo.ToString());
            comandoSQL.AgregarParámetro("@Estatus", técnicoAcadémico.Estatus.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO:
                        throw new BusinessLogicException(
                            "No puede haber dos técnicos académicos con el mismo número personal.",
                            BusinessLogicExceptionCode
                                .NO_PUEDEN_EXISTIR_DOS_TECNICOS_ACADEMICOS_CON_MISMO_NUMERO_PERSONAL);
                    default:
                        throw new BusinessLogicException(
                            "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                            BusinessLogicExceptionCode.ERROR_GENÉRICO_DE_BASE_DE_DATOS);
                }
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el técnico académico.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_TECNICO_ACADEMICO);
            }

            var idTecnicoAcademico = comandoSQL.GetÚltimoIdInsertado();

            if (técnicoAcadémico.Horarios != null)
            {
                foreach (var horario in técnicoAcadémico.Horarios)
                {
                    var idHorario = horarioDao.AgregarHorarioTécnicoAcadémico(horario, idTecnicoAcademico);
                    horario.IdHorario = idHorario;
                }
            }

            RealizarCommit();

            return idTecnicoAcademico;
        }

        public void EditarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico)
        {
            técnicoAcadémico.Validar("Error(es) al validar el técnico académico:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_TÉCNICO_ACADÉMICO);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            personaDao.EditarPersona(técnicoAcadémico);

            if (!string.IsNullOrEmpty(técnicoAcadémico.Usuario.Contraseña))
            {
                var usuarioDao = new UsuarioDAO(transacciónSql);
                usuarioDao.EditarUsuario(técnicoAcadémico.Usuario);
            }

            var horarioDAO = new HorarioDAO(transacciónSql);

            var comandoSQL = CrearComandoSQL("UPDATE TecnicoAcademico " +
                                             "SET NumeroPersonal = @NumeroPersonal, ProgramaEducativo = @ProgramaEducativo, Estatus = @Estatus " +
                                             "WHERE idTecnicoAcademico = @idTecnicoAcademico;");
            comandoSQL.AgregarParámetro("@NumeroPersonal", técnicoAcadémico.NúmeroPersonal);
            comandoSQL.AgregarParámetro("@ProgramaEducativo", técnicoAcadémico.ProgramaEducativo.ToString());
            comandoSQL.AgregarParámetro("@Estatus", técnicoAcadémico.Estatus.ToString());
            comandoSQL.AgregarParámetro("@idTecnicoAcademico", técnicoAcadémico.IdTécnicoAcadémico);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el técnico académico.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_TECNICO_ACADEMICO);
            }

            if (técnicoAcadémico.Horarios != null)
            {
                foreach (var horario in técnicoAcadémico.Horarios)
                {
                    if (horario.IdHorario > 0)
                    {
                        horarioDAO.EditarHorario(horario);
                    }
                    else
                    {
                        var idHorario =
                            horarioDAO.AgregarHorarioTécnicoAcadémico(horario, técnicoAcadémico.IdTécnicoAcadémico);
                        horario.IdHorario = idHorario;
                    }
                }
            }

            RealizarCommit();
        }

        public void CambiarEstatus(int idTécnicoAcadémico, Estatus estatus)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE TecnicoAcademico SET Estatus = @Estatus WHERE IdTecnicoAcademico = @IdTecnicoAcademico");
            comandoSQL.AgregarParámetro("@Estatus", estatus.ToString());
            comandoSQL.AgregarParámetro("@IdTecnicoAcademico", idTécnicoAcadémico);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo cambiar el estatus del técnico académico.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_TÉCNICO_ACADÉMICO);
            }
        }


        public List<TécnicoAcadémico> GetTécnicosAcadémicos()
        {
            var tecnicosAcademicos = new List<TécnicoAcadémico>();

            var comandoSQL = CrearComandoSQL(
                "SELECT P.IdPersona, " +
                "U.IdUsuario, " +
                "U.Usuario, " +
                "TA.IdTecnicoAcademico, " +
                "P.Nombre, " +
                "P.ApellidoPaterno, " +
                "P.ApellidoMaterno, " +
                "P.Email, " +
                "P.Genero, " +
                "TA.NumeroPersonal, TA.Estatus, ProgramaEducativo " +
                "FROM TecnicoAcademico AS TA " +
                "LEFT JOIN Persona AS P " +
                "ON TA.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U " +
                "ON TA.IdUsuario = U.IdUsuario;");

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var tecnicoAcademico = new TécnicoAcadémico
                {
                    IdPersona = row.Field<int>("IdPersona"),
                    Nombre = row.Field<string>("Nombre"),
                    ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                    ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                    Email = row.Field<string>("Email"),
                    Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                    ProgramaEducativo = (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo),
                        row.Field<string>("ProgramaEducativo")),
                    IdTécnicoAcadémico = row.Field<int>("IdTecnicoAcademico"),
                    NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                    Usuario = new Usuario
                    {
                        IdUsuario = row.Field<int>("IdUsuario"),
                        NombreUsuario = row.Field<string>("Usuario")
                    }
                };
                tecnicosAcademicos.Add(tecnicoAcademico);
            }

            return tecnicosAcademicos;
        }

        public TécnicoAcadémico GetTécnicoAcadémicoPorIdUsuario(int idUsuario)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, TA.IdTecnicoAcademico, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, TA.NumeroPersonal, TA.Estatus, ProgramaEducativo FROM TecnicoAcademico AS TA LEFT JOIN Persona AS P ON TA.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON TA.IdUsuario = U.IdUsuario WHERE U.IdUsuario = @IdUsuario;");
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un técnico académico, con el ID de Usuario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_TECNICO_ACADEMICO_CON_ID_USUARIO);
            }

            var row = dataTable.Rows[0];

            var técnicoAcadémico = new TécnicoAcadémico
            {
                IdTécnicoAcadémico = row.Field<int>("IdTecnicoAcademico"),
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                ProgramaEducativo =
                    (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo), row.Field<string>("ProgramaEducativo")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            if (técnicoAcadémico.Estatus == Estatus.Inactivo)
            {
                throw new BusinessLogicException(
                    "El técnico académico " + técnicoAcadémico.GetNombreCompleto() + " está inactivo en el sistema.",
                    BusinessLogicExceptionCode.TÉCNICO_ACADÉMICO_INACTIVO);
            }

            return técnicoAcadémico;
        }

        public TécnicoAcadémico GetTécnicoAcadémicoPorId(int idTécnicoAcadémico)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, TA.IdTecnicoAcademico, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, TA.NumeroPersonal, TA.Estatus, ProgramaEducativo FROM TecnicoAcademico AS TA LEFT JOIN Persona AS P ON TA.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON TA.IdUsuario = U.IdUsuario WHERE IdTecnicoAcademico = @IdTecnicoAcademico;");
            comandoSQL.AgregarParámetro("@IdTecnicoAcademico", idTécnicoAcadémico);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException(
                    "No existe un Técnico Acedémico con el ID de Técnico Académico proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_TECNICO_ACADEMICO_CON_ID_TENICO_ACADEMICO);
            }

            var row = dataTable.Rows[0];

            var tecnicoAcademico = new TécnicoAcadémico
            {
                IdTécnicoAcadémico = row.Field<int>("IdTecnicoAcademico"),
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                ProgramaEducativo =
                    (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo), row.Field<string>("ProgramaEducativo")),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            var horarioDao = new HorarioDAO();
            var horarios = horarioDao.GetHorariosTécnicoAcadémico(idTécnicoAcadémico);
            tecnicoAcademico.Horarios = horarios;

            return tecnicoAcademico;
        }
    }
}