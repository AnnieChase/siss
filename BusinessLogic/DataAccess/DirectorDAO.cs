﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class DirectorDAO : BaseDAO, IDirectorDAO
    {
        public DirectorDAO()
        {
        }

        public DirectorDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarDirector(Director director)
        {
            if (director.Usuario != null)
            {
                director.Usuario.TipoUsuario = TipoUsuario.Director;
            }

            director.Validar("Error(es) al validar el director:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            var usuarioDao = new UsuarioDAO(transacciónSql);

            var idPersona = personaDao.AgregarPersona(director);
            var idUsuario = usuarioDao.AgregarUsuario(director.Usuario);

            var comandoSQL =
                CrearComandoSQL(
                    "INSERT INTO Director (IdPersona, IdUsuario, NumeroPersonal, Estatus) VALUES (@IdPersona, @IdUsuario, @NumeroPersonal, @Estatus);");

            comandoSQL.AgregarParámetro("@IdPersona", idPersona);
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);
            comandoSQL.AgregarParámetro("@NumeroPersonal", director.NúmeroPersonal);
            comandoSQL.AgregarParámetro("@Estatus", director.Estatus.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO:
                        throw new BusinessLogicException(
                            "No puede haber dos directores con el mismo número personal.",
                            BusinessLogicExceptionCode
                                .NO_PUEDEN_EXISTIR_DOS_COORDINADORES_CON_MISMO_NUMERO_PERSONAL);
                    default:
                        throw new BusinessLogicException(
                            "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                            BusinessLogicExceptionCode.ERROR_GENÉRICO_DE_BASE_DE_DATOS);
                }
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el director.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_DIRECTOR);
            }

            var idDirector = comandoSQL.GetÚltimoIdInsertado();

            DeshabilitarTodosLosDirectoresExcepto(idDirector); // TODO Should catch

            RealizarCommit();

            return idDirector;
        }

        public void EditarDirector(Director director)
        {
            director.Validar("Error(es) al validar el director:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_DIRECTOR);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            personaDao.EditarPersona(director);

            if (!string.IsNullOrEmpty(director.Usuario.Contraseña))
            {
                var usuarioDAO = new UsuarioDAO(transacciónSql);
                usuarioDAO.EditarUsuario(director.Usuario);
            }

            var comandoSQL = CrearComandoSQL("UPDATE Director SET Estatus = @Estatus WHERE IdDirector = @IdDirector;");
            comandoSQL.AgregarParámetro("@Estatus", director.Estatus.ToString());
            comandoSQL.AgregarParámetro("@IdDirector", director.IdDirector);

            var resultado = EjecutarComando(comandoSQL);

            if (resultado == 0)
            {
                throw new BusinessLogicException("No se pudo editar el director",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_DIRECTOR);
            }

            if (director.Estatus == Estatus.Activo)
            {
                DeshabilitarTodosLosDirectoresExcepto(director.IdDirector);
            }

            RealizarCommit();
        }

        public void HabilitarDirector(int idDirector)
        {
            var transacciónSql = GetTransacción();

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Director SET Estatus = @Estatus WHERE IdDirector = @IdDirector");
            comandoSQL.AgregarParámetro("@Estatus", Estatus.Activo.ToString());
            comandoSQL.AgregarParámetro("@IdDirector", idDirector);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo habilitar el director.",
                    BusinessLogicExceptionCode.FALLO_AL_HABILITAR_DIRECTOR);
            }

            DeshabilitarTodosLosDirectoresExcepto(idDirector);

            RealizarCommit();
        }

        public List<Director> GetDirectores()
        {
            var directores = new List<Director>();

            var comandoSQL = CrearComandoSQL(
                "SELECT IdDirector, P.IdPersona, U.IdUsuario, Usuario, Nombre, ApellidoPaterno, ApellidoMaterno, Email, Genero, Estatus, NumeroPersonal " +
                "FROM Director AS D " +
                "LEFT JOIN Persona AS P " +
                "ON D.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U " +
                "ON D.IdUsuario = U.IdUsuario;"
            );
            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var director = new Director
                {
                    IdDirector = row.Field<int>("IdDirector"),
                    IdPersona = row.Field<int>("IdPersona"),
                    Nombre = row.Field<string>("Nombre"),
                    ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                    ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                    NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                    Email = row.Field<string>("Email"),
                    Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                    Usuario = new Usuario
                    {
                        IdUsuario = row.Field<int>("IdUsuario"),
                        NombreUsuario = row.Field<string>("Usuario")
                    }
                };
                directores.Add(director);
            }

            return directores;
        }

        public Director GetDirectorPorId(int idDirector)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, D.IdDirector, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, D.NumeroPersonal, D.Estatus FROM Director AS D LEFT JOIN Persona AS P ON D.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON D.IdUsuario = U.IdUsuario WHERE IdDirector = @IdDirector;");
            comandoSQL.AgregarParámetro("@IdDirector", idDirector);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un director con el ID de Director proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_DIRECTOR_CON_ID_DIRECTOR);
            }

            var row = dataTable.Rows[0];

            var director = new Director
            {
                IdDirector = row.Field<int>("IdDirector"),
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            return director;
        }

        public Director GetDirectorPorIdUsuario(int idUsuario)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, D.IdDirector, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, D.NumeroPersonal, D.Estatus FROM Director AS D LEFT JOIN Persona AS P ON D.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON D.IdUsuario = U.IdUsuario WHERE U.IdUsuario = @IdUsuario;");
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un director con el ID de Usuario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_DIRECTOR_CON_ID_USUARIO);
            }

            var row = dataTable.Rows[0];

            var director = new Director
            {
                IdDirector = row.Field<int>("IdDirector"),
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            if (director.Estatus == Estatus.Inactivo)
            {
                throw new BusinessLogicException(
                    "El director " + director.GetNombreCompleto() + " está inactivo en el sistema.",
                    BusinessLogicExceptionCode.DIRECTOR_INACTIVO);
            }

            return director;
        }

        public bool HayAlMenosUnDirectorActivo()
        {
            var comandoSQL = CrearComandoSQL("SELECT COUNT(*) AS Cantidad FROM Director WHERE Estatus = 'Activo';");
            var dataTable = RealizarConsulta(comandoSQL);
            if (dataTable.Rows.Count < 1)
            {
                return false;
            }

            var row = dataTable.Rows[0];
            var count = row.Field<int>("Cantidad");
            return count > 0;
        }


        /// <summary>
        ///     Deshabilitar todos los directores excepto el nuevo Director registrado
        /// </summary>
        /// <param name="idDirector"> Director se actualzia a Estatus Activo</param>
        private void DeshabilitarTodosLosDirectoresExcepto(int idDirector)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Director SET Estatus = @EstatusInactivo WHERE Estatus = @EstatusActivo AND NOT IdDirector = @IdDirector;");
            comandoSQL.AgregarParámetro("@EstatusInactivo", Estatus.Inactivo.ToString());
            comandoSQL.AgregarParámetro("@EstatusActivo", Estatus.Activo.ToString());
            comandoSQL.AgregarParámetro("@IdDirector", idDirector);

            EjecutarComando(comandoSQL);
        }
    }
}