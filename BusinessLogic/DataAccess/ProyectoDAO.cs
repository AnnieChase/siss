﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class ProyectoDAO : BaseDAO, IProyectoDAO
    {
        public ProyectoDAO()
        {
        }

        public ProyectoDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarProyecto(Proyecto proyecto)
        {
            proyecto.Validar("Error(es) al validar el proyecto:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PROYECTO);

            var transacciónSql = GetTransacción();

            var horarioDao = new HorarioDAO(transacciónSql);

            var comandoSQL = CrearComandoSQL("INSERT INTO Proyecto " +
                                             "(Nombre, " +
                                             "EstudiantesSolicitados, " +
                                             "UsuarioDirecto, " +
                                             "UsuarioIndirecto, " +
                                             "NombreResponsable, " +
                                             "CargoResponsable, " +
                                             "Descripcion, " +
                                             "ObjetivoGeneral, " +
                                             "ObjetivoInmediato, " +
                                             "Metodologia, " +
                                             "ObjetivoMediato, " +
                                             "Recursos, " +
                                             "Requisitos, " +
                                             "FuncionActividad, " +
                                             "Responsabilidades, " +
                                             "DuracionSemanas, " +
                                             "Actividad, " +
                                             "Mes, " +
                                             "Direccion, " +
                                             "Estatus)" +
                                             " VALUES " +
                                             "(Nombre = @Nombre, EstudiantesSolicitados = @EstudiantesSolicitados, UsuarioDirecto = @UsuarioDirecto, " +
                                             "UsuarioIndirecto = @UsuarioIndirecto, NombreResponsable = @NombreResponsable, CargoResponsable = @CargoResponsable, Descripcion = @Descripcion, ObjetivoGeneral = @ObjetivoGeneral, " +
                                             "ObjetivoInmediato = @ObjetivoInmediato, Metodologia = @Metodologia, ObjetivoMediato = @ObjetivoMediato, Recursos = @Recursos, Requisitos = @Requisitos, FuncionActividad = @FuncionActividad, " +
                                             "Responsabilidades = @Responsabilidades, DuracionSemanas = @DuracionSemanas, Actividad = @Actividad, Mes = @Mes, Direccion = @Direccion, Estatus = @Estatus);");

            comandoSQL.AgregarParámetro("@IdProyecto", proyecto.IdProyecto);
            comandoSQL.AgregarParámetro("@Nombre", proyecto.NombreProyecto);
            comandoSQL.AgregarParámetro("@EstudiantesSolicitados", proyecto.EstudiantesSolicitados);
            comandoSQL.AgregarParámetro("@Descripcion", proyecto.DescripciónGeneral);
            comandoSQL.AgregarParámetro("@ObjetivoGeneral", proyecto.ObjetivosGenerales);
            comandoSQL.AgregarParámetro("@ObjetivoInMediato", proyecto.ObjetivosInmediatos);
            comandoSQL.AgregarParámetro("@Metodologia", proyecto.Metodología);
            comandoSQL.AgregarParámetro("@ObjetivoMediato", proyecto.ObjetivosInmediatos);
            comandoSQL.AgregarParámetro("@URecursos", proyecto.Recursos);
            comandoSQL.AgregarParámetro("@Responsabilidades", proyecto.Responsabilidades);
            comandoSQL.AgregarParámetro("@DuracionSemanas", proyecto.Duración);
            comandoSQL.AgregarParámetro("@Actividad", proyecto.ActividadesFunciones);
            comandoSQL.AgregarParámetro("@Estatus", proyecto.Estatus.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                throw new BusinessLogicException(
                    "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                    code, businessLogicException);
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el proyecto.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_PROYECTO);
            }

            var idProyecto = comandoSQL.GetÚltimoIdInsertado();

            /*if (proyecto.HorarioServicio != null)
                foreach (var horario in proyecto.HorarioServicio)
                    horarioDao.AgregarHorarioProyecto(horario, idProyecto);*/

            return idProyecto;
        }

        public void EditarProyecto(Proyecto proyecto)
        {
            proyecto.Validar("Error(es) al validar el proyecto:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PROYECTO);

            var transacciónSQL = GetTransacción();

            var proyectoDao = new ProyectoDAO(transacciónSQL);
            proyectoDao.EditarProyecto(proyecto);

            var comandoSQL = CrearComandoSQL(
                "UPDATE Proyecto SET Nombre = @Nombre, EstudiantesSolicitados = @EstudiantesSolicitados, UsuarioDirecto = @UsuarioDirecto," +
                "UsuarioIndirecto = @UsuarioIndirecto, NombreResponsable = @NombreResponsable, CargoResponsable = @CargoResponsable, Descripción = @Descripcion, ObjetivoGeneral = @ObjetivoGeneral," +
                "ObjetivoInmediato = @ObjetivoInmediato, Metodologia = @Metodologia, ObjetivoMediato = @ObjetivoMediato, Recuros = @Recursos, Requisitos = @Requisitos, FuncionActividad = @FuncionActividad," +
                "Responsabilidades = @Responsabilidades, DuracionSemanas = @DuracionSemanas, Actividad = @Actividad, Mes = @Mes, Direccion = @Direccion, Estatus = @Estatus WHERE IdProyecto = @IdProyecto;");

            comandoSQL.AgregarParámetro("@Nombre", proyecto.NombreProyecto);
            comandoSQL.AgregarParámetro("@EstudiantesSolicitados", proyecto.EstudiantesSolicitados);
            comandoSQL.AgregarParámetro("@Descripcion", proyecto.DescripciónGeneral);
            comandoSQL.AgregarParámetro("@ObjetivoGeneral", proyecto.ObjetivosGenerales);
            comandoSQL.AgregarParámetro("@ObjetivoInMediato", proyecto.ObjetivosInmediatos);
            comandoSQL.AgregarParámetro("@Metodologia", proyecto.Metodología);
            comandoSQL.AgregarParámetro("@ObjetivoMediato", proyecto.ObjetivosInmediatos);
            comandoSQL.AgregarParámetro("@URecursos", proyecto.Recursos);
            comandoSQL.AgregarParámetro("@Responsabilidades", proyecto.Responsabilidades);
            comandoSQL.AgregarParámetro("@DuracionSemanas", proyecto.Duración);
            comandoSQL.AgregarParámetro("@Actividad", proyecto.ActividadesFunciones);
            comandoSQL.AgregarParámetro("@Estatus", proyecto.Estatus.ToString());

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el proyecto.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_PROYECTO);
            }

            RealizarCommit();
        }

        public List<Proyecto> GetProyectos()
        {
            var proyectosL = new List<Proyecto>();

            var comandoSQL = CrearComandoSQL("SELECT * FROM Proyecto;");
            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var proyecto = new Proyecto
                {
                    NombreProyecto = row.Field<string>("Nombre"),
                    EstudiantesSolicitados = row.Field<int>("EstudiantesSolicitados"),
                    DescripciónGeneral = row.Field<string>("Descripcion"),
                    ObjetivosGenerales = row.Field<string>("ObjetivoGeneral"),
                    ObjetivosInmediatos = row.Field<string>("ObjetivoInmediato"),
                    Metodología = row.Field<string>("Metodologia"),
                    ObjetivosMediatos = row.Field<string>("ObjetivoMediato"),
                    Recursos = row.Field<string>("Recursos"),
                    Responsabilidades = row.Field<string>("Responsabilidades"),
                    Duración = row.Field<string>("Duracion"),
                    ActividadesFunciones = row.Field<string>("Actividad"),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Mes"))
                };

                proyectosL.Add(proyecto);
            }

            return proyectosL;
        }

        public Proyecto GetByIdProyecto(int idProyecto)
        {
            var comandoSQL = CrearComandoSQL("SELECT TOP 1 * FROM Proyecto WHERE IdProyecto=@idProyecto;");
            comandoSQL.AgregarParámetro("@idProyecto", idProyecto);
            var dataTable = RealizarConsulta(comandoSQL);
            var row = dataTable.Rows[0];

            var proyecto = new Proyecto
            {
                NombreProyecto = row.Field<string>("Nombre"),
                EstudiantesSolicitados = row.Field<int>("EstudiantesSolicitados"),
                DescripciónGeneral = row.Field<string>("Descripcion"),
                ObjetivosGenerales = row.Field<string>("ObjetivoGeneral"),
                ObjetivosInmediatos = row.Field<string>("ObjetivoInmediato"),
                Metodología = row.Field<string>("Metodologia"),
                ObjetivosMediatos = row.Field<string>("ObjetivoMediato"),
                Recursos = row.Field<string>("Recursos"),
                Responsabilidades = row.Field<string>("Responsabilidades"),
                Duración = row.Field<string>("Duracion"),
                ActividadesFunciones = row.Field<string>("Actividad"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Mes"))
            };

            return proyecto;
        }

        public void CambiarEstatus(int idProyecto, Estatus estatus)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Proyecto SET Estatus = @Estatus WHERE IdProyecto = @IdProyecto");
            comandoSQL.AgregarParámetro("@Estatus", estatus.ToString());
            comandoSQL.AgregarParámetro("@IdOrganizacion", idProyecto);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
                throw new BusinessLogicException("No se pudo cambiar el estatus del Proyecto.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_PROYECTO);
        }
    }
}