﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class HorarioDAO : BaseDAO, IHorarioDAO
    {
        public HorarioDAO()
        {
        }

        public HorarioDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarHorarioTécnicoAcadémico(Horario horario, int idTécnicoAcadémico)
        {
            var idHorario = AgregarHorario(horario);

            var comandoSQL = CrearComandoSQL(
                "INSERT INTO HorarioTecnicoAcademico (IdTecnicoAcademico, IdHorario) VALUES" +
                "(@IdTecnicoAcademico,@IdHorario);");

            comandoSQL.AgregarParámetro("@IdTecnicoAcademico", idTécnicoAcadémico);
            comandoSQL.AgregarParámetro("@IdHorario", idHorario);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se vincular el horario con el técnico académico.",
                    BusinessLogicExceptionCode.FALLO_AL_VINCULAR_HORARIO_CON_TECNICO_ACADEMICO);
            }

            return idHorario;
        }

        public int AgregarHorarioProyecto(Horario horario, int idProyecto)
        {
            var idHorario = AgregarHorario(horario);

            var comandoSQL = CrearComandoSQL("INSERT INTO HorarioProyectoEstablecido (IdProyecto, IdHorario) VALUES" +
                                             "(@IdProyecto, @IdHorario);");

            comandoSQL.AgregarParámetro("@IdProyecto", idProyecto);
            comandoSQL.AgregarParámetro("@IdHorario", idHorario);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo vincular el horario con el proyecto.",
                    BusinessLogicExceptionCode.FALLO_AL_VINCULAR_HORARIO_CON_PROYECTO);
            }

            return idHorario;
        }

        public void EditarHorario(Horario horario)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Horario SET Dia = @Dia, HoraInicio = @HoraInicio, HoraFinal = @HoraFinal, Ubicacion = @Ubicacion WHERE IdHorario = @IdHorario;");
            comandoSQL.AgregarParámetro("@Dia", horario.Día);
            comandoSQL.AgregarParámetro("@HoraInicio", horario.HoraInicio);
            comandoSQL.AgregarParámetro("@HoraFinal", horario.HoraFinal);
            comandoSQL.AgregarParámetro("@Ubicacion", horario.Ubicación);
            comandoSQL.AgregarParámetro("@IdHorario", horario.IdHorario);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el horario.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_HORARIO);
            }
        }

        public List<Horario> GetHorariosTécnicoAcadémico(int idTécnicoAcadémico)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "SELECT H.IdHorario, Dia, HoraInicio, HoraFinal, Ubicacion FROM HorarioTecnicoAcademico AS TAH LEFT JOIN Horario AS H ON TAH.IdHorario = H.IdHorario WHERE IdTecnicoAcademico = @IdTécnicoAcadémico;");
            comandoSQL.AgregarParámetro("@IdTécnicoAcadémico", idTécnicoAcadémico);

            var dataTable = RealizarConsulta(comandoSQL);

            var horarios = new List<Horario>();

            foreach (DataRow row in dataTable.Rows)
            {
                var horario = new Horario
                {
                    IdHorario = row.Field<int>("IdHorario"),
                    Día = (DíaSemana) Enum.Parse(typeof(DíaSemana), row.Field<string>("Dia")),
                    HoraInicio = row.Field<string>("HoraInicio"),
                    HoraFinal = row.Field<string>("HoraFinal"),
                    Ubicación = row.Field<string>("Ubicacion")
                };
                horarios.Add(horario);
            }

            return horarios;
        }

        public List<Horario> GetHorariosProyecto(int idProyecto)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "SELECT H.IdHorario, Dia, HoraInicio, HoraFinal, Ubicacion FROM HorarioProyectoEstablecido AS HPE LEFT JOIN Horario AS H ON HPE.IdHorario = H.IdHorario WHERE IdProyecto = @IdProyecto;");
            comandoSQL.AgregarParámetro("@idProyecto", idProyecto);

            var dataTable = RealizarConsulta(comandoSQL);

            var horarios = new List<Horario>();

            foreach (DataRow row in dataTable.Rows)
            {
                var horario = new Horario
                {
                    IdHorario = row.Field<int>("IdHorario"),
                    Día = (DíaSemana) Enum.Parse(typeof(DíaSemana), row.Field<string>("Dia")),
                    HoraInicio = row.Field<string>("HoraInicio"),
                    HoraFinal = row.Field<string>("HoraFinal"),
                    Ubicación = row.Field<string>("Ubicacion")
                };
                horarios.Add(horario);
            }

            return horarios;
        }

        public void EliminarHorarioTécnicoAcadémico(int idHorario)
        {
            var comandoSQL = CrearComandoSQL("DELETE FROM HorarioTecnicoAcademico WHERE IdHorario = @IdHorario;");

            comandoSQL.AgregarParámetro("@IdHorario", idHorario);

            var resultado = EjecutarComando(comandoSQL);

            if (resultado > 0)
            {
                resultado = EliminarHorario(idHorario);
            }

            if (resultado == 0)
            {
                throw new BusinessLogicException("No existe horario con el ID de horario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_HORARIO_CON_ID_HORARIO);
            }
        }

        public void EliminarHorarioProyecto(int idHorario)
        {
            var comandoSQL = CrearComandoSQL("DELETE FROM HorarioProyectoEstablecido WHERE IdHorario = @IdHorario;");

            comandoSQL.AgregarParámetro("@IdHorario", idHorario);

            var resultado = EjecutarComando(comandoSQL);

            if (resultado > 0)
            {
                resultado = EliminarHorario(idHorario);
            }

            if (resultado == 0)
            {
                throw new BusinessLogicException("No existe horario con el ID de horario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_HORARIO_CON_ID_HORARIO);
            }
        }

        /// <summary>
        ///     Agrega un nuevo horario en la base de datos
        /// </summary>
        /// <param name="horario">Horario a agregar en la base de datos</param>
        /// <returns>Id del Horario agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        private int AgregarHorario(Horario horario)
        {
            horario.Validar("Error(es) al validar el horario:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_HORARIO);

            var comandoSQL = CrearComandoSQL("INSERT INTO Horario (Dia, HoraFinal, HoraInicio, Ubicacion) VALUES " +
                                             "(@Dia, @HoraFinal, @HoraInicio, @Ubicacion);");

            comandoSQL.AgregarParámetro("@Dia", horario.Día.ToString());
            comandoSQL.AgregarParámetro("@HoraFinal", horario.HoraFinal);
            comandoSQL.AgregarParámetro("@HoraInicio", horario.HoraInicio);
            comandoSQL.AgregarParámetro("@Ubicacion", horario.Ubicación);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el horario.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_HORARIO);
            }

            var idHorario = comandoSQL.GetÚltimoIdInsertado();

            return idHorario;
        }

        /// <summary>
        ///     Elimina un horario existente de la base de datos
        /// </summary>
        /// <param name="idHorario">El id del horario a borrar</param>
        /// <returns>El número de filas afectadas por la operación</returns>
        private int EliminarHorario(int idHorario)
        {
            var comandoSQL = CrearComandoSQL("DELETE FROM Horario WHERE IdHorario = @IdHorario;");

            comandoSQL.AgregarParámetro("@IdHorario", idHorario);

            var resultado = EjecutarComando(comandoSQL);

            return resultado;
        }
    }
}