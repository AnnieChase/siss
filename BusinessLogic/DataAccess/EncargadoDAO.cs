﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class EncargadoDAO : BaseDAO, IEncargadoDAO
    {
        public EncargadoDAO()
        {
        }

        public EncargadoDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarEncargado(Encargado encargado)
        {
            encargado.Validar("Error(es) al validar al encargado: ",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);

            var idPersona = personaDao.AgregarPersona(encargado);

            var comandoSQL = CrearComandoSQL("INSERT INTO Encargado " +
                                             "(IdPersona," +
                                             " Cargo, " +
                                             "ExtensionTelefonica, " +
                                             "Estatus, IdOrganizacion)" +
                                             " VALUES(@IdPersona, @Cargo, @ExtensionTelefonica, @Estatus, @IdOrganizacion);");

            comandoSQL.AgregarParámetro("@IdPersona", idPersona);
            comandoSQL.AgregarParámetro("@Cargo", encargado.Cargo);
            comandoSQL.AgregarParámetro("@ExtensionTelefonica", encargado.ExtensiónTelefónica);
            comandoSQL.AgregarParámetro("@Estatus", encargado.Estatus.ToString());
            comandoSQL.AgregarParámetro("@IdOrganizacion", encargado.IdOrganización);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el encargado.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_ENCARGADO);
            }

            var idEncargado = comandoSQL.GetÚltimoIdInsertado();

            RealizarCommit();

            return idEncargado;
        }

        public void CambiarEstatus(int idEncargado, Estatus estatus)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Encargado SET Estatus = @Estatus WHERE IdEncargado = @IdEncargado");
            comandoSQL.AgregarParámetro("@Estatus", estatus.ToString());
            comandoSQL.AgregarParámetro("@IdEncargado", idEncargado);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo cambiar el estatus del encargado.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_ENCARGADO);
            }
        }

        public void EditarEncargado(Encargado encargado)
        {
            encargado.Validar("Error(es) al validar el encargado: ",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ENCARGADO);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);

            personaDao.EditarPersona(encargado);

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Encargado SET Cargo = @Cargo, ExtensionTelefonica = @ExtensionTelefonica, Estatus = @Estatus, IdOrganizacion = @IdOrganizacion WHERE IdEncargado = @IdEncargado;");

            comandoSQL.AgregarParámetro("@Cargo", encargado.Cargo);
            comandoSQL.AgregarParámetro("@ExtensionTelefonica", encargado.ExtensiónTelefónica);
            comandoSQL.AgregarParámetro("@Estatus", encargado.Estatus.ToString());
            comandoSQL.AgregarParámetro("@IdOrganizacion", encargado.IdOrganización);
            comandoSQL.AgregarParámetro("@IdEncargado", encargado.IdEncargado);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el encargado.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_ENCARGADO);
            }

            RealizarCommit();
        }

        public List<Encargado> GetEncargados()
        {
            var encargados = new List<Encargado>();

            var comandoSQL = CrearComandoSQL(
                "SELECT P.IdPersona, " +
                "IdEncargado, " +
                "Nombre, " +
                "ApellidoPaterno, " +
                "ApellidoMaterno, " +
                "P.Email, " +
                "Genero, " +
                "Cargo, " +
                "E.Estatus," +
                "ExtensionTelefonica," +
                "O.IdOrganizacion, NombreOrganizacion " +
                "FROM Encargado AS E " +
                "LEFT JOIN Persona AS P " +
                "ON E.IdPersona = P.IdPersona LEFT JOIN Organizacion AS O ON E.IdOrganizacion = O.IdOrganizacion; ");

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var encargado = new Encargado
                {
                    IdEncargado = row.Field<int>("IdEncargado"),
                    IdPersona = row.Field<int>("IdPersona"),
                    Nombre = row.Field<string>("Nombre"),
                    ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                    ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                    Email = row.Field<string>("Email"),
                    Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                    Cargo = row.Field<string>("Cargo"),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                    ExtensiónTelefónica = row.Field<string>("ExtensionTelefonica"),
                    IdOrganización = row.Field<int>("IdOrganizacion"),
                    NombreOrganización = row.Field<string>("NombreOrganizacion")
                };
                encargados.Add(encargado);
            }

            return encargados;
        }

        public Encargado GetEncargadosPorId(int idEncargado)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT P.IdPersona, Nombre, ApellidoPaterno, ApellidoMaterno, Genero, Email, IdEncargado, Cargo, ExtensionTelefonica, Estatus, IdOrganizacion FROM Encargado AS E LEFT JOIN Persona " +
                "AS P ON P.IdPersona = E.IdPersona WHERE IdEncargado = @IdEncargado;");
            comandoSQL.AgregarParámetro("IdEncargado", idEncargado);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un Encargado con el ID de Encargado proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_ENCARGADO_CON_ID_ENCARGADO);
            }

            var datarow = dataTable.Rows[0];

            var encargado = new Encargado
            {
                IdEncargado = datarow.Field<int>("IdEncargado"),
                IdPersona = datarow.Field<int>("IdPersona"),
                Nombre = datarow.Field<string>("Nombre"),
                ApellidoPaterno = datarow.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = datarow.Field<string>("ApellidoMaterno"),
                Género = (Género) Enum.Parse(typeof(Género), datarow.Field<string>("Genero")),
                Email = datarow.Field<string>("Email"),
                Cargo = datarow.Field<string>("Cargo"),
                ExtensiónTelefónica = datarow.Field<string>("ExtensionTelefonica"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), datarow.Field<string>("Estatus")),
                IdOrganización = datarow.Field<int>("IdOrganizacion")
            };

            return encargado;
        }
    }
}