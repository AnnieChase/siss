﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using Database;

namespace BusinessLogic.DataAccess
{
    public class ReporteDAO : BaseDAO, IReporteDAO
    {
        public ReporteDAO()
        {
        }

        public ReporteDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarReporte(Reporte reporte)
        {
            var comandoSQL = CrearComandoSQL("INSERT INTO Reporte (FechaEntrega,HorasSS,NumeroReporte) " +
                                             "VALUES(@FechaEntrega,@horasSS,@numeroReporte);");
            comandoSQL.AgregarParámetro("@FechaEntrega", reporte.FechaEntrega.ToString("MM/dd/yy"));
            comandoSQL.AgregarParámetro("@horasSS", reporte.HorasServicioSocial);
            comandoSQL.AgregarParámetro("@numeroReporte", reporte.NumeroReporte);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el reporte.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_REPORTE);
            }

            var idReporte = comandoSQL.GetÚltimoIdInsertado();

            return idReporte;
        }

        public void EditarReporte(Reporte reporte)
        {
            var comandoSQL = CrearComandoSQL("UPDATE Reporte" +
                                             "SET FechaEntrega = @FechaEntrega, HorasSS = @HorasSS, NumeroReporte = @NumeroReporte" +
                                             "WHERE IdReporte = @idReporte;");
            comandoSQL.AgregarParámetro("@FechaEntrega", reporte.FechaEntrega.ToString("MM/dd/yyyy"));
            comandoSQL.AgregarParámetro("@HorasSS", reporte.HorasServicioSocial);
            comandoSQL.AgregarParámetro("@NumeroReporte", reporte.NumeroReporte);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el reporte",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_REPORTE);
            }
        }

        public List<Reporte> GetReportes()
        {
            var reportes = new List<Reporte>();

            var comandoSQL = CrearComandoSQL(
                "SELECT Rp.FechaEntrega, " +
                "Rp.HorasSS, " +
                "Rp.NumeroReporte " +
                "FROM Reporte AS Rp; "
            );

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var reporte = new Reporte
                {
                    FechaEntrega = row.Field<DateTime>("FechaEntrega"),
                    HorasServicioSocial = row.Field<int>("HorasSS"),
                    NumeroReporte = row.Field<int>("NumeroReporte")
                };
                reportes.Add(reporte);
            }

            return reportes;
        }
    }
}