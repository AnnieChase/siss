﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class EstudianteDAO : BaseDAO, IEstudianteDAO
    {
        public EstudianteDAO()
        {
        }

        public EstudianteDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarEstudiante(Estudiante estudiante)
        {
            if (estudiante.Usuario != null)
            {
                estudiante.Usuario.TipoUsuario = TipoUsuario.Estudiante;
            }

            estudiante.Validar("Error(es) al validar el estudiante:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            var usuarioDao = new UsuarioDAO(transacciónSql);

            var idPersona = personaDao.AgregarPersona(estudiante);
            var idUsuario = usuarioDao.AgregarUsuario(estudiante.Usuario);

            var comandoSQL = CrearComandoSQL(
                "INSERT INTO Estudiante (IdPersona, IdUsuario, Matricula, ProgramaEducativo, Bloque, Seccion, PeriodoEscolar) VALUES(@IdPersona, @IdUsuario, @Matricula, @ProgramaEducativo, @Bloque, @Seccion, @PeriodoEscolar)");

            comandoSQL.AgregarParámetro("@IdPersona", idPersona);
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);
            comandoSQL.AgregarParámetro("@Matricula", estudiante.Matrícula);
            comandoSQL.AgregarParámetro("@ProgramaEducativo", estudiante.ProgramaEducativo.ToString());
            comandoSQL.AgregarParámetro("@Bloque", estudiante.Bloque.ToString());
            comandoSQL.AgregarParámetro("@Seccion", estudiante.Sección);
            comandoSQL.AgregarParámetro("@PeriodoEscolar", estudiante.PeriodoEscolar);

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO:
                        throw new BusinessLogicException("No pueden existir dos estudiantes con la misma matrícula.",
                            BusinessLogicExceptionCode.NO_PUEDEN_EXISTIR_DOS_ESTUDIANTES_CON_MISMA_MATRÍCULA);
                    case BusinessLogicExceptionCode.VALORES_DEMASIADO_LARGOS:
                        throw new BusinessLogicException(
                            "No se pudo agregar el estudiante. Hay valores demasiado largos.",
                            BusinessLogicExceptionCode.VALORES_DEMASIADO_LARGOS);
                    case BusinessLogicExceptionCode.VALORES_FALTANTES:
                        throw new BusinessLogicException(
                            "No se pudo agregar el estudiante. Hay Valores en Faltantes.",
                            BusinessLogicExceptionCode.VALORES_FALTANTES);
                    default:
                        throw new BusinessLogicException(
                            "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                            BusinessLogicExceptionCode.FALLO_AL_AGREGAR_ESTUDIANTE);
                }
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el estudiante.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_ESTUDIANTE);
            }

            var idEstudiante = comandoSQL.GetÚltimoIdInsertado();

            RealizarCommit();

            return idEstudiante;
        }

        public void EditarEstudiante(Estudiante estudiante)
        {
            estudiante.Validar("Error(es) al validar el estudiante:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ESTUDIANTE);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            personaDao.EditarPersona(estudiante);

            if (!string.IsNullOrEmpty(estudiante.Usuario.Contraseña))
            {
                var usuarioDao = new UsuarioDAO(transacciónSql);
                usuarioDao.EditarUsuario(estudiante.Usuario);
            }

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Estudiante SET Bloque = @Bloque, Seccion = @Seccion, EstatusEstudiante = @EstatusEstudiante, PeriodoEscolar = @PeriodoEscolar WHERE IdEstudiante = @IdEstudiante");
            comandoSQL.AgregarParámetro("@Bloque", estudiante.Bloque.ToString());
            comandoSQL.AgregarParámetro("@Seccion", estudiante.Sección);
            comandoSQL.AgregarParámetro("@EstatusEstudiante", estudiante.EstatusEstudiante.ToString());
            comandoSQL.AgregarParámetro("@PeriodoEscolar", estudiante.PeriodoEscolar);
            comandoSQL.AgregarParámetro("@IdEstudiante", estudiante.IdEstudiante);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el estudiante.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_ESTUDIANTE);
            }

            RealizarCommit();
        }

        public void CambiarEstatus(int idEstudiante, EstatusEstudiante nuevoEstatusEstudiante)
        {
            var estatusActualEstudiante = GetEstatusActualEstudiante(idEstudiante);

            if (estatusActualEstudiante == nuevoEstatusEstudiante)
            {
                return;
            }

            if (estatusActualEstudiante == EstatusEstudiante.Espera && nuevoEstatusEstudiante == EstatusEstudiante.Baja)
            {
                throw new BusinessLogicException("No puedes cambiar el estatus de un estudiante a Espera.",
                    BusinessLogicExceptionCode.NO_SE_PUEDE_DAR_DE_BAJA_A_UN_ESTUDIANTE_EN_ESPERA);
            }

            if (estatusActualEstudiante == EstatusEstudiante.Rechazado ||
                estatusActualEstudiante == EstatusEstudiante.Baja)
            {
                throw new BusinessLogicException(
                    "No puedes cambiar el estatus de un estudiante rechazado o dado de baja.",
                    BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_RECHAZADO_O_BAJA);
            }

            if (nuevoEstatusEstudiante == EstatusEstudiante.Espera)
            {
                throw new BusinessLogicException("No puedes cambiar el estatus de un estudiante a Espera.",
                    BusinessLogicExceptionCode.NO_SE_PUEDE_CAMBIAR_ESTATUS_DE_ESTUDIANTE_A_ESPERA);
            }

            if (estatusActualEstudiante == EstatusEstudiante.Aceptado &&
                nuevoEstatusEstudiante != EstatusEstudiante.Baja)
            {
                throw new BusinessLogicException("Solo puedes dar de baja a un estudiante aceptado.",
                    BusinessLogicExceptionCode.SOLO_PUEDES_DAR_DE_BAJA_A_UN_ESTUDIANTE_ACEPTADO);
            }

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Estudiante SET EstatusEstudiante = @EstatusEstudiante WHERE IdEstudiante = @IdEstudiante");
            comandoSQL.AgregarParámetro("@EstatusEstudiante", nuevoEstatusEstudiante.ToString());
            comandoSQL.AgregarParámetro("@IdEstudiante", idEstudiante);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo cambiar el estatus del estudiante.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_ESTUDIANTE);
            }
        }

        public List<Estudiante> GetEstudiantes()
        {
            var estudiantes = new List<Estudiante>();

            var comandoSQL = CrearComandoSQL(
                "SELECT P.IdPersona, " +
                "U.IdUsuario, " +
                "U.Usuario, U.TipoUsuario, " +
                "ET.IdEstudiante, " +
                "P.Nombre, " +
                "P.ApellidoPaterno, " +
                "P.ApellidoMaterno, " +
                "P.Email, " +
                "P.Genero, " +
                "ET.Matricula, " +
                "ET.ProgramaEducativo, " +
                "ET.Bloque, " +
                "ET.Seccion, " +
                "ET.EstatusEstudiante, " +
                "ET.PeriodoEscolar " +
                "FROM Estudiante AS ET " +
                "LEFT JOIN Persona AS P " +
                "ON ET.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U " +
                "ON ET.IdUsuario = U.IdUsuario;");

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var estudiante = CrearEstudiante(row);
                estudiantes.Add(estudiante);
            }

            return estudiantes;
        }

        public Estudiante GetEstudiantePorId(int idEstudiante)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, U.TipoUsuario, IdEstudiante, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, Matricula, ProgramaEducativo, Bloque, Seccion, EstatusEstudiante, PeriodoEscolar FROM Estudiante AS E LEFT JOIN Persona AS P ON E.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON E.IdUsuario = U.IdUsuario WHERE IdEstudiante = @IdEstudiante;");
            comandoSQL.AgregarParámetro("@IdEstudiante", idEstudiante);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un estudiante con el ID de Estudiante proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_ESTUDIANTE);
            }

            var row = dataTable.Rows[0];

            var estudiante = CrearEstudiante(row);

            return estudiante;
        }

        public Estudiante GetEstudiantePorIdUsuario(int idUsuario)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, U.TipoUsuario, E.IdEstudiante, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, Matricula, ProgramaEducativo, Bloque, Seccion, EstatusEstudiante, PeriodoEscolar FROM Estudiante AS E LEFT JOIN Persona AS P ON E.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON E.IdUsuario = U.IdUsuario WHERE U.IdUsuario = @IdUsuario;");
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un estudiante con el ID de Usuario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_USUARIO);
            }

            var row = dataTable.Rows[0];

            var estudiante = CrearEstudiante(row);

            switch (estudiante.EstatusEstudiante)
            {
                case EstatusEstudiante.Espera:
                    throw new BusinessLogicException("Aún estás en espera de ser aceptado en el sistema.",
                        BusinessLogicExceptionCode.ESTUDIANTE_EN_ESPERA);
                case EstatusEstudiante.Rechazado:
                    throw new BusinessLogicException("Has sido rechazado en el sistema.",
                        BusinessLogicExceptionCode.ESTUDIANTE_RECHAZADO);
                case EstatusEstudiante.Baja:
                    throw new BusinessLogicException("Estás dado de baja en el sistema.",
                        BusinessLogicExceptionCode.ESTUDIANTE_BAJA);
            }

            return estudiante;
        }

        private EstatusEstudiante GetEstatusActualEstudiante(int idEstudiante)
        {
            var comandoSQL =
                CrearComandoSQL("SELECT EstatusEstudiante FROM Estudiante WHERE IdEstudiante = @IdEstudiante");
            comandoSQL.AgregarParámetro("@IdEstudiante", idEstudiante);

            var dataTable = RealizarConsulta(comandoSQL);
            if (dataTable.Rows.Count == 0)
            {
                throw new BusinessLogicException("No existe un estudiante con el IdEstudiante proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_ESTUDIANTE_CON_ID_ESTUDIANTE);
            }

            var row = dataTable.Rows[0];

            return (EstatusEstudiante) Enum.Parse(typeof(EstatusEstudiante),
                row.Field<string>("EstatusEstudiante"));
        }

        private Estudiante CrearEstudiante(DataRow row)
        {
            var estudiante = new Estudiante
            {
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                Matrícula = row.Field<string>("Matricula"),
                ProgramaEducativo =
                    (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo), row.Field<string>("ProgramaEducativo")),
                Bloque = (Bloque) Enum.Parse(typeof(Bloque), row.Field<string>("Bloque")),
                Sección = (Sección) Enum.Parse(typeof(Sección), row.Field<string>("Seccion")),
                PeriodoEscolar = row.Field<string>("PeriodoEscolar"),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                EstatusEstudiante =
                    (EstatusEstudiante) Enum.Parse(typeof(EstatusEstudiante), row.Field<string>("EstatusEstudiante")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario"),
                    TipoUsuario = (TipoUsuario) Enum.Parse(typeof(TipoUsuario), row.Field<string>("TipoUsuario"))
                }
            };

            if (row.Table.Columns.Contains("IdEstudiante"))
            {
                estudiante.IdEstudiante = row.Field<int>("IdEstudiante");
            }

            return estudiante;
        }
    }
}