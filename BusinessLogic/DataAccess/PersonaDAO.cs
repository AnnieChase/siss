﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class PersonaDAO : BaseDAO, IPersonaDAO
    {
        public PersonaDAO()
        {
        }

        public PersonaDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarPersona(Persona persona)
        {
            persona.Validar("Error(es) al validar la persona:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA);

            var comandoSQL = CrearComandoSQL(
                "INSERT INTO Persona (Nombre, ApellidoPaterno, ApellidoMaterno, Email, Genero) VALUES(@Nombre, @ApellidoPaterno, @ApellidoMaterno, @Email, @Genero)");

            comandoSQL.AgregarParámetro("@Nombre", persona.Nombre);
            comandoSQL.AgregarParámetro("@ApellidoPaterno", persona.ApellidoPaterno);
            comandoSQL.AgregarParámetro("@ApellidoMaterno", persona.ApellidoMaterno);
            comandoSQL.AgregarParámetro("@Email", persona.Email);
            comandoSQL.AgregarParámetro("@Genero", persona.Género.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                throw new BusinessLogicException(
                    "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_PERSONA);
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar la persona.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_PERSONA);
            }

            var idPersona = comandoSQL.GetÚltimoIdInsertado();

            return idPersona;
        }

        public void EditarPersona(Persona persona)
        {
            persona.Validar("Error(es) al validar la persona:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_PERSONA);

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Persona SET Nombre = @Nombre, ApellidoPaterno = @ApellidoPaterno, ApellidoMaterno = @ApellidoMaterno, Email = @Email, Genero = @Genero WHERE IdPersona = @IdPersona;");

            comandoSQL.AgregarParámetro("@IdPersona", persona.IdPersona);
            comandoSQL.AgregarParámetro("@Nombre", persona.Nombre);
            comandoSQL.AgregarParámetro("@ApellidoPaterno", persona.ApellidoPaterno);
            comandoSQL.AgregarParámetro("@ApellidoMaterno", persona.ApellidoMaterno);
            comandoSQL.AgregarParámetro("@Email", persona.Email);
            comandoSQL.AgregarParámetro("@Genero", persona.Género.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                throw new BusinessLogicException(
                    "Hubo un error al realizar los cambios en la base de datos. [" + code + "]",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_PERSONA);
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar la persona.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_PERSONA);
            }
        }
    }
}