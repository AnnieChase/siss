﻿using System;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Util;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class UsuarioDAO : BaseDAO, IUsuarioDAO
    {
        public UsuarioDAO()
        {
        }

        public UsuarioDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarUsuario(Usuario usuario)
        {
            usuario.Validar("Error(es) al validar el usuario:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO);

            var comandoSQL =
                CrearComandoSQL(
                    "INSERT INTO Usuario (Usuario, Contraseña, TipoUsuario) VALUES(@Usuario, @Contraseña, @TipoUsuario);");

            comandoSQL.AgregarParámetro("@Usuario", usuario.NombreUsuario);
            comandoSQL.AgregarParámetro("@Contraseña", Criptógrafo.Encriptar(usuario.Contraseña));
            comandoSQL.AgregarParámetro("@TipoUsuario", usuario.TipoUsuario.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO:
                        throw new BusinessLogicException(
                            "No puede haber dos usuarios con el mismo nombre de usuario.",
                            BusinessLogicExceptionCode
                                .NO_PUEDEN_EXISTIR_DOS_USUARIOS_CON_MISMO_NOMBRE_DE_USUARIO);
                    default:
                        throw new BusinessLogicException(
                            "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                            BusinessLogicExceptionCode.FALLO_AL_AGREGAR_USUARIO);
                }
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el usuario.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_USUARIO);
            }

            var idUsuario = comandoSQL.GetÚltimoIdInsertado();

            return idUsuario;
        }

        public void EditarUsuario(Usuario usuario)
        {
            usuario.Validar("Error(es) al validar el usuario:", BusinessLogicExceptionCode.ERROR_AL_VALIDAR_USUARIO);

            var comandoSQL =
                CrearComandoSQL("UPDATE Usuario SET Contraseña = @Contraseña WHERE IdUsuario = @IdUsuario;");

            comandoSQL.AgregarParámetro("@Contraseña", Criptógrafo.Encriptar(usuario.Contraseña));
            comandoSQL.AgregarParámetro("@IdUsuario", usuario.IdUsuario);

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                throw new BusinessLogicException(
                    "Hubo un error al realizar los cambios en la base de datos. [" + code + "]",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_USUARIO);
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo modificar el usuario.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_USUARIO);
            }
        }

        public Usuario GetUsuarioConCredenciales(string nombreUsuario, string contraseña)
        {
            var comandoSQL =
                CrearComandoSQL("SELECT TOP 1 * FROM Usuario WHERE Usuario = @Usuario AND Contraseña = @Contraseña");
            comandoSQL.AgregarParámetro("@Usuario", nombreUsuario);
            comandoSQL.AgregarParámetro("@Contraseña", Criptógrafo.Encriptar(contraseña));

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                if (ExisteUsuarioConUsuario(nombreUsuario))
                {
                    throw new BusinessLogicException("El usuario está bien, pero la contraseña no. ¿Por qué eres así?",
                        BusinessLogicExceptionCode.CONTRASEÑA_INVÁLIDA);
                }

                throw new BusinessLogicException("El usuario y la contraseña introducidos son inválidos.",
                    BusinessLogicExceptionCode.CREDENCIALES_INVÁLIDAS);
            }

            var row = dataTable.Rows[0];

            return new Usuario
            {
                IdUsuario = row.Field<int>("IdUsuario"),
                NombreUsuario = row.Field<string>("Usuario"),
                TipoUsuario = (TipoUsuario) Enum.Parse(typeof(TipoUsuario), row.Field<string>("TipoUsuario"))
            };
        }

        /// <summary>
        ///     Se verifica la existencia de un Usuario en el sistema
        /// </summary>
        /// <param name="usuario">El nombre de usuario con el cual se verificará la existencia de un Usuario</param>
        /// <returns>Regresa verdadero si existe un usuario con el nombre de usuario proporcionado</returns>
        private bool ExisteUsuarioConUsuario(string usuario)
        {
            var comandoSQL = CrearComandoSQL("SELECT TOP 1 Usuario FROM Usuario WHERE Usuario = @Usuario");
            comandoSQL.AgregarParámetro("@Usuario", usuario);

            var dataTable = RealizarConsulta(comandoSQL);

            return dataTable.Rows.Count == 1;
        }
    }
}