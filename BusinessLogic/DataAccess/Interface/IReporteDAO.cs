﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Reporte
    /// </summary>
    internal interface IReporteDAO
    {
        /// <summary>
        ///     Agrega un Reporte Mensual a la base de datos
        /// </summary>
        /// <param name="reporte">El reporte mensual a agregar</param>
        /// <returns>Id del Reporte mensual agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarReporte(Reporte reporte);

        /// <summary>
        ///     Edita un Reporte y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="reporte">El Reporte mensual que se va a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarReporte(Reporte reporte);

        /// <summary>
        ///     Recupera todos los Reportes existentes de la base de datos
        /// </summary>
        /// <returns>La lista que contiene todos los Reportes recuperados</returns>
        List<Reporte> GetReportes();
    }
}