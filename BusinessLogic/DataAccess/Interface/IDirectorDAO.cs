﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Director
    /// </summary>
    internal interface IDirectorDAO
    {
        /// <summary>
        ///     Agrega un Director a la base de datos
        /// </summary>
        /// <param name="director">El Director a agregar</param>
        /// <returns>Id del Director agragado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        /// <exception cref="SqlException"></exception>
        int AgregarDirector(Director director);

        /// <summary>
        ///     Edita el registro de un director. También edita el registro de su persona y usuario.
        /// </summary>
        /// <param name="director">El director que modificará su registro.</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarDirector(Director director);

        /// <summary>
        ///     Cambia el Estatus de un director a Activo y deshabilita los demás.
        /// </summary>
        /// <param name="idDirector">Id del director a habilitar</param>
        void HabilitarDirector(int idDirector);

        /// <summary>
        ///     Recupera los directores que se encuentren en la base de datos
        /// </summary>
        /// <returns>
        ///     Lista con los directores recuperados
        /// </returns>
        List<Director> GetDirectores();

        /// <summary>
        ///     Recupera un Director a partir del IdDirector proporcionado
        /// </summary>
        /// <param name="idDirector">IdDirector que se usa para buscar al Director</param>
        /// <returns>Director resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Director GetDirectorPorId(int idDirector);

        /// <summary>
        ///     Obtiene un Director a partir del nombre de usuario dado
        /// </summary>
        /// <param name="idUsuario">IdUsuario que se usa para buscar el IdDirector</param>
        /// <returns>Director resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Director GetDirectorPorIdUsuario(int idUsuario);

        /// <summary>
        ///     Determina si existe al menos un Director con estatus "Activo" en la base de datos
        /// </summary>
        /// <returns>True si hay más de 0 directores activos</returns>
        bool HayAlMenosUnDirectorActivo();
    }
}