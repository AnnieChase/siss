﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Estudiante
    /// </summary>
    internal interface IEstudianteDAO
    {
        /// <summary>
        ///     Agrega un nuevo Estudiante a la base de datos
        /// </summary>
        /// <param name="estudiante"></param>
        /// <returns>El IdEStudiante del Estudiante agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarEstudiante(Estudiante estudiante);

        /// <summary>
        ///     Obtiene un Estudiante específico según el idUsuario dado
        /// </summary>
        /// <param name="estudiante"></param>
        /// <returns>Estudiante obtenido</returns>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarEstudiante(Estudiante estudiante);

        void CambiarEstatus(int idEstudiante, EstatusEstudiante nuevoEstatusEstudiante);

        /// <summary>
        ///     Recupera todos los Estudiantes de la base de datos
        /// </summary>
        /// <returns>Lista de Estudiantes recuperados</returns>
        List<Estudiante> GetEstudiantes();

        /// <summary>
        ///     Recupera todos los estudiantes de la base de datos
        /// </summary>
        /// <param name="idEstudiante"></param>
        /// <returns>Lista con los Estudiantes recuperados</returns>
        Estudiante GetEstudiantePorId(int idEstudiante);

        /// <summary>
        ///     Obtiene un Estudiante a partir del nombre de usuario dado
        /// </summary>
        /// <param name="idUsuario">IdUsuario que se usa para buscar el IdEstudiante</param>
        /// <returns>Estudiante resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Estudiante GetEstudiantePorIdUsuario(int idUsuario);
    }
}