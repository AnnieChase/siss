﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de TecnicoAcademico
    /// </summary>
    internal interface ITécnicoAcadémicoDAO
    {
        /// <summary>
        ///     Agrega un Técnico Académico a la base de datos
        /// </summary>
        /// <param name="técnicoAcadémico">Técnico Académico que se va a agregar</param>
        /// <returns>Id del Técnico Académico que fue agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        /// <exception cref="SqlException"></exception>
        int AgregarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico);

        /// <summary>
        ///     Edita un Técnico Académico y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="técnicoAcadémico">Técnico Académico que se va a editar</param>
        /// <returns>Id del Técnico Académico que fue editado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico);

        /// <summary>
        ///     Cambia el Estatus de un Técnico Académico en la base de datos.
        /// </summary>
        /// <param name="idTécnicoAcadémico">Id del Técnico Académico</param>
        /// <param name="estatus">El nuevo Estatus</param>
        void CambiarEstatus(int idTécnicoAcadémico, Estatus estatus);

        /// <summary>
        ///     Obtiene todos los Técnicos Académicos registrados en al base de datos
        /// </summary>
        /// <returns>La lista de Técnicos Académicos</returns>
        /// <exception cref="BusinessLogicException"></exception>
        List<TécnicoAcadémico> GetTécnicosAcadémicos();

        TécnicoAcadémico GetTécnicoAcadémicoPorId(int idTécnicoAcadémico);

        /// <summary>
        ///     Obtiene un Técnico Académico específico según el idUsuario dado
        /// </summary>
        /// <param name="idUsuario">idUsuario que se usa para buscar el idTécnicoAcadémico</param>
        /// <returns>Técnico Académico resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        TécnicoAcadémico GetTécnicoAcadémicoPorIdUsuario(int idUsuario);
    }
}