﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Expendiente
    /// </summary>
    internal interface IExpedienteDAO
    {
        /// <summary>
        ///     Se agrega un nuevo Expediente a la base de datos
        /// </summary>
        /// <param name="expediente">Expediente a agregar</param>
        /// <returns>Id del Expediente agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarExpediente(Expediente expediente);

        /// <summary>
        ///     Edita un Expediente y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="expediente">El expediente a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarExpediente(Expediente expediente);

        /// <summary>
        ///     Recupera todos los Expedientes de la base de datos
        /// </summary>
        /// <returns>Lista con los Expedientes recuperados</returns>
        List<Expediente> GetExpedientes();
    }
}