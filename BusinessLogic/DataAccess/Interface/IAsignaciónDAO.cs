﻿using System.Collections.Generic;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Iterfaz para el DAO de Asignación
    /// </summary>
    internal interface IAsignaciónDAO
    {
        /// <summary>
        ///     Agrega una nueva Aseignación entre un Estudiante y Proyecto a la base de datos
        /// </summary>
        /// <param name="estudiante">Estudiante a asignar a Proyecto</param>
        /// <param name="proyecto">Proyecto que se asignará a Estudiante</param>
        /// <returns>Id de la Asignación realizada</returns>
        int AgregarAsignación(Estudiante estudiante, Proyecto proyecto);

        /// <summary>
        ///     Recupera todas las Asignaciones de la base de datos
        /// </summary>
        /// <returns>Lista con las Asignaciones Recuperadas</returns>
        List<Asignación> GetAsignaciones();
    }
}