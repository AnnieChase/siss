﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Encargado
    /// </summary>
    internal interface IEncargadoDAO
    {
        /// <summary>
        ///     Agrega un Encargado a la base de datos
        /// </summary>
        /// <param name="encargado">El encargado a agregar</param>
        /// <returns>Id del Encargado agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        /// <exception cref="SqlException"></exception>
        int AgregarEncargado(Encargado encargado);

        /// <summary>
        ///     Edita un Encargado y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="encargado">Encargado a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarEncargado(Encargado encargado);

        /// <summary>
        ///     Recupera todos los Encargados de la base de datos
        /// </summary>
        /// <returns>Lista de Encargados recuperados</returns>
        List<Encargado> GetEncargados();

        /// <summary>
        ///     Recupera todos los encargados de la base de datos
        /// </summary>
        /// <param name="idEncargado"></param>
        /// <returns>Lista con los Encargados recuperados</returns>
        Encargado GetEncargadosPorId(int idEncargado);

        /// <summary>
        ///     Cambia el Estatus de un Encargado en la base de datos.
        /// </summary>
        /// <param name="idEncargado">Id del Encargado</param>
        /// <param name="estatus">El nuevo Estatus</param>
        void CambiarEstatus(int idEncargado, Estatus estatus);
    }
}