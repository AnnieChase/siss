﻿using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    internal interface IPersonaDAO
    {
        /// <summary>
        ///     Agrega un Persona a la base de datos
        /// </summary>
        /// <param name="persona">Persona que se va a agregar</param>
        /// <returns>Id de Persona que fue agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarPersona(Persona persona);

        /// <summary>
        ///     Edita una Persona y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="persona">Persona que se va a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarPersona(Persona persona);
    }
}