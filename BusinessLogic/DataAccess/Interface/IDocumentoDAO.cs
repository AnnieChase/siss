﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Documento
    /// </summary>
    internal interface IDocumentoDAO
    {
        /// <summary>
        ///     Agrega un nuevo Documento y lo asocia a un Expediente en la base de datos
        /// </summary>
        /// <param name="documento">Documento a agregar</param>
        /// <param name="idExpediente">IdExpediente a asociar el documento</param>
        /// <returns>Id del Documento agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarDocumento(Documento documento, int idExpediente);

        /// <summary>
        ///     Edita un Documento y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="documento">Docuemtno a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarDocumento(Documento documento);

        /// <summary>
        ///     Recupera todos los documentos de la base de datos
        /// </summary>
        /// <returns>Lista con los Documetos recuperados</returns>
        List<Documento> GetDocumentos();
    }
}