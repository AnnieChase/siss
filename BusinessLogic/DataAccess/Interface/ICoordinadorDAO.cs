﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    internal interface ICoordinadorDAO
    {
        /// <summary>
        ///     Agrega un nuevo coordinador a la base de datos
        /// </summary>
        /// <param name="coordinador">Coordinador a agregar</param>
        /// <returns>IdCoordinador del Coordinador agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        /// <exception cref="SqlException"></exception>
        int AgregarCoordinador(Coordinador coordinador);

        /// <summary>
        ///     Edita el registro de un Coordinador. También edita el registro de su persona y usuario.
        /// </summary>
        /// <param name="coordinador">Coordinador a editar</param>
        void EditarCoordinador(Coordinador coordinador);

        /// <summary>
        ///     Cambia el Estatus de un coordinador en la base de datos. Si el Estatus es Activo, deshabilita a todos
        ///     los demás coordinadores del Programa Educativo.
        /// </summary>
        /// <param name="coordinador">El coordinador a modificar</param>
        /// <param name="estatus">El Estatus nuevo</param>
        void CambiarEstatus(Coordinador coordinador, Estatus estatus);

        /// <summary>
        ///     Recupera todos los coordinadores de la base de datos
        /// </summary>
        /// <returns>Lista con los coordinadores recuperados</returns>
        List<Coordinador> GetCoordinadores();

        /// <summary>
        ///     Recupera un coordinador a partir de su Id proporcionado
        /// </summary>
        /// <param name="idCoordinador">IdCoordinador que se usa para buscar y recueprar el Coordinador</param>
        /// <returns>Coordinador recuperado de la base de datos</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Coordinador GetCoordinadorPorId(int idCoordinador);

        /// <summary>
        ///     Obtiene un Coordinador específico según el idUsuario dado
        /// </summary>
        /// <param name="idUsuario">idUsuario que se usa para buscar el idCoordinador</param>
        /// <returns>Coordinador resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Coordinador GetCoordinadorPorIdUsuario(int idUsuario);

        /// <summary>
        ///     Verifica si existe un Coordinador con estatus "Activo" en el programa educativo indicado
        /// </summary>
        /// <param name="programaEducativo">Programa educativo del cual se buscará un Coordinador activo</param>
        /// <param name="idActualCoordinador">[Opcional] Id del coordinador que será excento de la búsqueda</param>
        /// <returns>Devuelve True si existe un coordinador activo en el programa indicado</returns>
        bool HayUnCoordinadorActivoEn(ProgramaEducativo programaEducativo, int idActualCoordinador = 0);
    }
}