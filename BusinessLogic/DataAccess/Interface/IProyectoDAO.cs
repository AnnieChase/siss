﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Proyecto
    /// </summary>
    internal interface IProyectoDAO
    {
        /// <summary>
        ///     Agrega un nuevo Proyecto a la base de datos
        /// </summary>
        /// <param name="proyecto">Proyecto que se va a agregar</param>
        /// <returns>Id del Proyecto que fue agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarProyecto(Proyecto proyecto);

        /// <summary>
        ///     Edita un Proyecto y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="proyecto">Proyecto a editar</param>
        void EditarProyecto(Proyecto proyecto);

        /// <summary>
        ///     Recupera todos los proyectos que se encuentren en la base de datos
        /// </summary>
        /// <returns>Lista con los proyectos encontrados</returns>
        List<Proyecto> GetProyectos();

        /// <summary>
        ///     Obtiene un Proyecto específico de acuerdo al IdProyecto proporcionado
        /// </summary>
        /// <param name="idProyecto">IdProyecto que se utiliza para buscar el Proyecto</param>
        /// <returns>Proyecto resultante de la búsqueda</returns>
        Proyecto GetByIdProyecto(int idProyecto);

        /// <summary>
        ///     Cambia el Estatus de un Poryecto en la base de datos.
        /// </summary>
        /// <param name="idProyecto">Id de la Organización</param>
        /// <param name="estatus">El nuevo Estatus</param>
        void CambiarEstatus(int idProyecto, Estatus estatus);
    }
}
