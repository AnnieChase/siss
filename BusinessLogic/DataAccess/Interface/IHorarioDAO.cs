﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Horario
    /// </summary>
    internal interface IHorarioDAO
    {
        /// <summary>
        ///     Agrega un nuevo Horario de Técnico Académico a la base de datos
        /// </summary>
        /// <param name="horario">Horario a agregar</param>
        /// <param name="idTécnicoAcadémico">Id del Técnico académico al cual pertenece el horario</param>
        /// <returns>Id del Horario agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarHorarioTécnicoAcadémico(Horario horario, int idTécnicoAcadémico);

        /// <summary>
        ///     Agrega un nuevo Horario de Proyecto a la base de datos
        /// </summary>
        /// <param name="horario">Horario a agregar</param>
        /// <param name="idProyecto">Id del proyecto al cual pertenece el horario</param>
        /// <returns>Id del Horario agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarHorarioProyecto(Horario horario, int idProyecto);

        /// <summary>
        ///     Edita el registro de un horario
        /// </summary>
        /// <param name="horario">Horario a registrar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarHorario(Horario horario);

        /// <summary>
        ///     Recupera los Horarios de un técnico académico
        /// </summary>
        /// <param name="idTécnicoAcadémico">Id del técnico académico del cual se recuperan los horarios</param>
        /// <returns>Lista de los horarios de técnico académico recuperados</returns>
        List<Horario> GetHorariosTécnicoAcadémico(int idTécnicoAcadémico);

        /// <summary>
        ///     Recupera los Horarios de un Proyecto
        /// </summary>
        /// <param name="idProyecto">Id del proyecto del cual se recuperan los horarios</param>
        /// <returns>Lista de los horarios de proyecto recuperados</returns>
        List<Horario> GetHorariosProyecto(int idProyecto);

        /// <summary>
        ///     Elimina el Horario de un técnico académico
        /// </summary>
        /// <param name="idHorario">El idHorario del Horario a eliminar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EliminarHorarioTécnicoAcadémico(int idHorario);

        /// <summary>
        ///     Elimina el Horario de un proyecto
        /// </summary>
        /// <param name="idHorario">El idHorario del Horario a eliminar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EliminarHorarioProyecto(int idHorario);
    }
}