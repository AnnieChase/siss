﻿using System.Collections.Generic;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Organización
    /// </summary>
    internal interface IOrganizaciónDAO
    {
        /// <summary>
        ///     Agrega una organización a la base de datos
        /// </summary>
        /// <param name="organización">La organización a agregar</param>
        /// <returns>Id de la organización agregada</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarOrganización(Organización organización);

        /// <summary>
        ///     Edita el registro de una Organización
        /// </summary>
        /// <param name="organización">Organización a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarOrganización(Organización organización);

        /// <summary>
        ///     Recupera todas las organizaciones que se encuentran en la base de datos
        /// </summary>
        /// <returns>Lista de organizaciones recuperadas</returns>
        /// <exception cref="BusinessLogicException"></exception>
        List<Organización> GetOrganizaciones();

        /// <summary>
        ///     Recupera una Organización específica a partir de un idOrganizacion proporcionado
        /// </summary>
        /// <param name="idOrganización">IdOrganizacion que se va a usar para buscar la Organizacion</param>
        /// <returns>Organización resultante de la búsqueda</returns>
        Organización GetOrganizaciónPorId(int idOrganización);

        /// <summary>
        ///     Determina si existe al menos una Organiación con estatus "Activo" en la base de datos
        /// </summary>
        /// <returns>Regresa verdadero si hay más de 0 organizaciones activas</returns>
        bool HayAlMenosUnaOrganización();

        /// <summary>
        ///     Cambia el Estatus de una Organización en la base de datos.
        /// </summary>
        /// <param name="idOrganización">Id de la Organización</param>
        /// <param name="estatus">El nuevo Estatus</param>
        void CambiarEstatus(int idOrganización, Estatus estatus);
    }
}