using BusinessLogic.Excepción;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
    /// <summary>
    ///     Interfaz del DAO de Usuario
    /// </summary>
    public interface IUsuarioDAO
    {
        /// <summary>
        ///     Agrega un Usuario a la base de datos
        /// </summary>
        /// <param name="usuario">Usuario que se va a agregar</param>
        /// <returns>Id del Usuario que fue agregado</returns>
        /// <exception cref="BusinessLogicException"></exception>
        int AgregarUsuario(Usuario usuario);

        /// <summary>
        ///     Edita un Usuario y guarda los cambios en la base de datos
        /// </summary>
        /// <param name="usuario">Usuario que se va a editar</param>
        /// <exception cref="BusinessLogicException"></exception>
        void EditarUsuario(Usuario usuario);

        /// <summary>
        ///     Obtiene un usuario con sus credenciales (id,usuario y tipo de usuario)
        /// </summary>
        /// <param name="nombreUsuario">Nombre del usuario a buscar</param>
        /// <param name="contraseña">Contraseña correspondiente al usuario que se busca</param>
        /// <returns>El Usuario resultante de la búsqueda</returns>
        /// <exception cref="BusinessLogicException"></exception>
        Usuario GetUsuarioConCredenciales(string nombreUsuario, string contraseña);
    }
}