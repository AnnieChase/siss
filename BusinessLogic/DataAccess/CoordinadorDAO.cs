﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class CoordinadorDAO : BaseDAO, ICoordinadorDAO
    {
        public CoordinadorDAO()
        {
        }

        public CoordinadorDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarCoordinador(Coordinador coordinador)
        {
            if (coordinador.Usuario != null)
            {
                coordinador.Usuario.TipoUsuario = TipoUsuario.Coordinador;
            }

            coordinador.Validar("Error(es) al validar el coordinador:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_COORDINADOR);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            var usuarioDao = new UsuarioDAO(transacciónSql);

            var idPersona = personaDao.AgregarPersona(coordinador);
            var idUsuario = usuarioDao.AgregarUsuario(coordinador.Usuario);

            var comandoSQL = transacciónSql.CrearComandoSQL(
                "INSERT INTO Coordinador (IdPersona, IdUsuario, NumeroPersonal, Estatus, ProgramaEducativo)" +
                "VALUES(@idPersona, @idUsuario, @NumeroPersonal, @Estatus, @ProgramaEducativo);");

            comandoSQL.AgregarParámetro("@idPersona", idPersona);
            comandoSQL.AgregarParámetro("@idUsuario", idUsuario);
            comandoSQL.AgregarParámetro("@NumeroPersonal", coordinador.NúmeroPersonal);
            comandoSQL.AgregarParámetro("@Estatus", coordinador.Estatus.ToString());
            comandoSQL.AgregarParámetro("@ProgramaEducativo", coordinador.ProgramaEducativo.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                switch (code)
                {
                    case BusinessLogicExceptionCode.ÍNDICE_ÚNICO_DUPLICADO:
                        throw new BusinessLogicException(
                            "No puede haber dos coordinadores con el mismo número personal.",
                            BusinessLogicExceptionCode
                                .NO_PUEDEN_EXISTIR_DOS_COORDINADORES_CON_MISMO_NUMERO_PERSONAL);
                    default:
                        throw new BusinessLogicException(
                            "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                            BusinessLogicExceptionCode.ERROR_GENÉRICO_DE_BASE_DE_DATOS);
                }
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el coordinador.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_COORDINADOR);
            }

            var idCoordinador = comandoSQL.GetÚltimoIdInsertado();

            DeshabilitarCoordinadoresActivosEn(coordinador.ProgramaEducativo, idCoordinador);

            RealizarCommit();

            return idCoordinador;
        }

        public void EditarCoordinador(Coordinador coordinador)
        {
            coordinador.Validar("Error(es) al validar el coordinador:",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_COORDINADOR);

            var transacciónSql = GetTransacción();

            var personaDao = new PersonaDAO(transacciónSql);
            personaDao.EditarPersona(coordinador);

            if (!string.IsNullOrEmpty(coordinador.Usuario.Contraseña))
            {
                var usuarioDao = new UsuarioDAO(transacciónSql);
                usuarioDao.EditarUsuario(coordinador.Usuario);
            }

            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Coordinador SET ProgramaEducativo = @ProgramaEducativo, Estatus = @Estatus WHERE IdCoordinador = @IdCoordinador;");
            comandoSQL.AgregarParámetro("@ProgramaEducativo", coordinador.ProgramaEducativo.ToString());
            comandoSQL.AgregarParámetro("@Estatus", coordinador.Estatus.ToString());
            comandoSQL.AgregarParámetro("@IdCoordinador", coordinador.IdCoordinador);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el coordinador.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_COORDINADOR);
            }

            if (coordinador.Estatus == Estatus.Activo)
            {
                DeshabilitarCoordinadoresActivosEn(coordinador.ProgramaEducativo, coordinador.IdCoordinador);
            }

            RealizarCommit();
        }

        public void CambiarEstatus(Coordinador coordinador, Estatus estatus)
        {
            var transacciónSQL = GetTransacción();
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Coordinador SET Estatus = @Estatus WHERE IdCoordinador = @IdCoordinador");
            comandoSQL.AgregarParámetro("@Estatus", estatus.ToString());
            comandoSQL.AgregarParámetro("@IdCoordinador", coordinador.IdCoordinador);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo cambiar el estatus del coordinador.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_COORDINADOR);
            }

            if (estatus == Estatus.Activo)
            {
                DeshabilitarCoordinadoresActivosEn(coordinador.ProgramaEducativo, coordinador.IdCoordinador);
            }

            RealizarCommit();
        }

        public List<Coordinador> GetCoordinadores()
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT P.IdPersona, U.IdUsuario, Usuario, IdCoordinador, Nombre, ApellidoPaterno, ApellidoMaterno, Email, Genero, NumeroPersonal, ProgramaEducativo, Estatus FROM Coordinador AS C LEFT JOIN Persona AS P ON C.IdPersona = P.IdPersona LEFT JOIN Usuario AS U ON C.IdUsuario = U.IdUsuario;");

            var dataTable = RealizarConsulta(comandoSQL);

            var coordinadores = new List<Coordinador>();

            foreach (DataRow row in dataTable.Rows)
            {
                var coordinador = new Coordinador
                {
                    IdPersona = row.Field<int>("IdPersona"),
                    Nombre = row.Field<string>("Nombre"),
                    ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                    ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                    Email = row.Field<string>("Email"),
                    Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                    IdCoordinador = row.Field<int>("IdCoordinador"),
                    NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                    ProgramaEducativo = (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo),
                        row.Field<string>("ProgramaEducativo")),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                    Usuario = new Usuario
                    {
                        IdUsuario = row.Field<int>("IdUsuario"),
                        NombreUsuario = row.Field<string>("Usuario")
                    }
                };
                coordinadores.Add(coordinador);
            }

            return coordinadores;
        }

        public Coordinador GetCoordinadorPorId(int idCoordinador)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, C.IdCoordinador, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, C.NumeroPersonal, C.Estatus, C.ProgramaEducativo FROM Coordinador AS C LEFT JOIN Persona AS P ON C.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON C.IdUsuario = U.IdUsuario WHERE IdCoordinador = @IdCoordinador;");
            comandoSQL.AgregarParámetro("@IdCoordinador", idCoordinador);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un coordinador con el ID de Coordinador proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_COORDINADOR_CON_ID_COORDINADOR);
            }

            var row = dataTable.Rows[0];

            var coordinador = new Coordinador
            {
                IdCoordinador = row.Field<int>("IdCoordinador"),
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                ProgramaEducativo =
                    (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo), row.Field<string>("ProgramaEducativo")),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            return coordinador;
        }

        public Coordinador GetCoordinadorPorIdUsuario(int idUsuario)
        {
            var comandoSQL = CrearComandoSQL(
                "SELECT TOP 1 P.IdPersona, U.IdUsuario, U.Usuario, C.IdCoordinador, P.Nombre, P.ApellidoPaterno, P.ApellidoMaterno, P.Email, " +
                "P.Genero, C.NumeroPersonal, C.ProgramaEducativo, C.Estatus FROM Coordinador AS C LEFT JOIN Persona AS P ON C.IdPersona = P.IdPersona " +
                "LEFT JOIN Usuario AS U ON C.IdUsuario = U.IdUsuario WHERE U.IdUsuario = @IdUsuario;");
            comandoSQL.AgregarParámetro("@IdUsuario", idUsuario);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe un coordinador con el ID de Usuario proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_COORDINADOR_CON_ID_USUARIO);
            }

            var row = dataTable.Rows[0];

            var coordinador = new Coordinador
            {
                IdPersona = row.Field<int>("IdPersona"),
                Nombre = row.Field<string>("Nombre"),
                ApellidoPaterno = row.Field<string>("ApellidoPaterno"),
                ApellidoMaterno = row.Field<string>("ApellidoMaterno"),
                Email = row.Field<string>("Email"),
                Género = (Género) Enum.Parse(typeof(Género), row.Field<string>("Genero")),
                IdCoordinador = row.Field<int>("IdCoordinador"),
                NúmeroPersonal = row.Field<string>("NumeroPersonal"),
                ProgramaEducativo =
                    (ProgramaEducativo) Enum.Parse(typeof(ProgramaEducativo), row.Field<string>("ProgramaEducativo")),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus")),
                Usuario = new Usuario
                {
                    IdUsuario = row.Field<int>("IdUsuario"),
                    NombreUsuario = row.Field<string>("Usuario")
                }
            };

            if (coordinador.Estatus == Estatus.Inactivo)
            {
                throw new BusinessLogicException(
                    "El coordinador " + coordinador.GetNombreCompleto() + " está inactivo en el sistema.",
                    BusinessLogicExceptionCode.COORDINADOR_INACTIVO);
            }

            return coordinador;
        }

        public bool HayUnCoordinadorActivoEn(ProgramaEducativo programaEducativo, int idCoordinadorActual = 0)
        {
            ComandoSQL comandoSQL;
            if (idCoordinadorActual > 0)
            {
                comandoSQL =
                    CrearComandoSQL(
                        "SELECT COUNT(*) AS Cantidad FROM Coordinador WHERE Estatus = 'Activo' AND ProgramaEducativo = @ProgramaEducativo AND NOT IdCoordinador = @IdCoordinador;");
                comandoSQL.AgregarParámetro("@IdCoordinador", idCoordinadorActual);
            }
            else
            {
                comandoSQL =
                    CrearComandoSQL(
                        "SELECT COUNT(*) AS Cantidad FROM Coordinador WHERE Estatus = 'Activo' AND ProgramaEducativo = @ProgramaEducativo;");
            }

            comandoSQL.AgregarParámetro("@ProgramaEducativo", programaEducativo.ToString());

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count < 1)
            {
                return false;
            }

            var row = dataTable.Rows[0];

            var count = row.Field<int>("Cantidad");

            return count > 0;
        }

        private void DeshabilitarCoordinadoresActivosEn(ProgramaEducativo programaEducativo, int idNuevoCoordinador)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Coordinador SET Estatus = @EstatusInactivo WHERE ProgramaEducativo = @ProgramaEducativo AND Estatus = @EstatusActivo AND NOT IdCoordinador = @IdCoordinador;");
            comandoSQL.AgregarParámetro("@ProgramaEducativo", programaEducativo.ToString());
            comandoSQL.AgregarParámetro("@EstatusInactivo", Estatus.Inactivo.ToString());
            comandoSQL.AgregarParámetro("@EstatusActivo", Estatus.Activo.ToString());
            comandoSQL.AgregarParámetro("@IdCoordinador", idNuevoCoordinador);

            EjecutarComando(comandoSQL);
        }
    }
}