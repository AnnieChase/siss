﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using Database;

namespace BusinessLogic.DataAccess
{
    public class ExpedienteDAO : BaseDAO, IExpedienteDAO
    {
        public ExpedienteDAO()
        {
        }

        public ExpedienteDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarExpediente(Expediente expediente)
        {
            var comandoSQL = CrearComandoSQL("INSERT INTO Expediente(FechaCreacionExpediente)" +
                                             "VALUES(@FechaCreacionExpediente);");
            comandoSQL.AgregarParámetro("@FechaCreacionExpediente",
                expediente.FechaCreaciónExpediente.ToString("MM/dd/yyyy"));

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar el expediente.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_EXPEDIENTE);
            }

            var idExpediente = comandoSQL.GetÚltimoIdInsertado();

            return idExpediente;
        }

        public void EditarExpediente(Expediente expediente)
        {
            var comandoSQL = CrearComandoSQL("UPDATE Expediente" +
                                             "SET FechaCreacionExpediente = @FechaCreacionExpediente" +
                                             "WHERE IdExpediente = @IdExpediente;");

            comandoSQL.AgregarParámetro("@FechaCreacionExpediente", expediente.FechaCreaciónExpediente);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar el expediente.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_EXPEDIENTE);
            }
        }

        public List<Expediente> GetExpedientes()
        {
            var expedientes = new List<Expediente>();

            var comandoSQL = CrearComandoSQL(
                "SELECT FechaCreacionExpediente " +
                "FROM Expediente; "
            );

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var expediente = new Expediente
                {
                    FechaCreaciónExpediente = row.Field<DateTime>("FechaCreacionExpediente")
                };
                expedientes.Add(expediente);
            }

            return expedientes;
        }
    }
}