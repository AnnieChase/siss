﻿using System;
using System.Collections.Generic;
using BusinessLogic.Model;


namespace BusinessLogic.DataAccess
{
    interface IEncargadoDAO
    {
        int AgregarEncargado(Encargado encargado);
        List<Encargado> GetEncargados(int IdEncargado);
        void EditarEncargado(Encargado encargado);
        void DarBajaEncargado(Encargado encargado);
    }
}
