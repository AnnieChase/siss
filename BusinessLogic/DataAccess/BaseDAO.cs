﻿using System;
using System.Data;
using System.Data.SqlClient;
using BusinessLogic.Excepción;
using Database;

namespace BusinessLogic.DataAccess
{
    /// <summary>
    ///     Clase base para todos los DAOs.
    /// </summary>
    public abstract class BaseDAO
    {
        /// <summary>
        ///     Indica si hay una Transacción SQL pendiente.
        /// </summary>
        private bool _transacciónPendiente;

        /// <summary>
        ///     Transacción SQL que permite realizar commandos SQL sin afectar la base de datos.
        /// </summary>
        private TransacciónSQL _transacciónSql;

        protected BaseDAO()
        {
        }

        protected BaseDAO(TransacciónSQL transacciónSql)
        {
            _transacciónSql = transacciónSql;
            _transacciónPendiente = true;
        }

        /// <summary>
        ///     Devuelve una Transacción. La crea si es necesario.
        /// </summary>
        /// <returns>Una Transacción SQL.</returns>
        internal TransacciónSQL GetTransacción()
        {
            if (_transacciónSql == null)
            {
                try
                {
                    _transacciónSql = new TransacciónSQL();
                    _transacciónPendiente = false;
                }
                catch (SqlException sqlException)
                {
                    var businessLogicException = new BusinessLogicException(sqlException);
                    throw new BusinessLogicException(
                        "Hubo un error al acceder a la base de datos. Por favor, revisa el archivo de registro.",
                        BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONEXIÓN_CON_BASE_DE_DATOS, sqlException);
                }
                catch (ArgumentException argumentException)
                {
                    var businessLogicException = new BusinessLogicException(
                        "Es posible que el connectionString tenga el formato incorrecto.",
                        BusinessLogicExceptionCode.CONNECTION_STRING_POSIBLE_FORMATO_INCORRECTO);
                    throw new BusinessLogicException(
                        "Hubo un error al acceder a la base de datos. Por favor, revisa el archivo de registro.",
                        BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONEXIÓN_CON_BASE_DE_DATOS, argumentException);
                }
            }

            return _transacciónSql;
        }

        /// <summary>
        ///     Realiza el commit de una Transacción si no existe una transacción pendiente. Esto quiere decir, que si
        ///     el DAO se inicializó con una Transacción, esta misma deberá realizar su commit desde donde se creó.
        /// </summary>
        internal void RealizarCommit()
        {
            if (!_transacciónPendiente && _transacciónSql != null)
            {
                _transacciónSql.Confirmar();
                _transacciónSql = null;
            }
        }

        /// <summary>
        ///     Crea un ComandoSQL. La asigna a una Transacción si existe.
        /// </summary>
        /// <param name="query">La instrucción SQL.</param>
        /// <returns>Un ComandoSQL</returns>
        internal ComandoSQL CrearComandoSQL(string query)
        {
            ComandoSQL comandoSql;

            try
            {
                if (_transacciónSql != null)
                {
                    comandoSql = _transacciónSql.CrearComandoSQL(query);
                }
                else
                {
                    comandoSql = new ComandoSQL(query);
                }
            }
            catch (SqlException sqlException)
            {
                var businessLogicException = new BusinessLogicException(sqlException);
                throw new BusinessLogicException(
                    "Hubo un error al acceder a la base de datos. Por favor, revisa el archivo de registro.",
                    BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONEXIÓN_CON_BASE_DE_DATOS, sqlException);
            }
            catch (ArgumentException argumentException)
            {
                var businessLogicException = new BusinessLogicException(
                    "Es posible que el connectionString tenga el formato incorrecto.",
                    BusinessLogicExceptionCode.CONNECTION_STRING_POSIBLE_FORMATO_INCORRECTO);
                throw new BusinessLogicException(
                    "Hubo un error al acceder a la base de datos. Por favor, revisa el archivo de registro.",
                    BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONEXIÓN_CON_BASE_DE_DATOS, argumentException);
            }

            return comandoSql;
        }

        /// <summary>
        ///     Ejecuta un comando SQL.
        /// </summary>
        /// <param name="comandoSQL">Comando SQL a ejecutar</param>
        /// <returns>Número de filas afectadas por el comando</returns>
        /// <exception cref="BusinessLogicException"></exception>
        internal int EjecutarComando(ComandoSQL comandoSQL)
        {
            try
            {
                return comandoSQL.Ejecutar();
            }
            catch (SqlException sqlException)
            {
                throw new BusinessLogicException(sqlException);
            }
        }

        /// <summary>
        ///     Realiza una consulta a la base de datos con el ComandoSQL proporcionado.
        /// </summary>
        /// <param name="comandoSQL">Comando SQL que contiene la consulta con sus parámetros</param>
        /// <returns>Una tabla de datos con los resultados</returns>
        /// <exception cref="BusinessLogicException"></exception>
        internal DataTable RealizarConsulta(ComandoSQL comandoSQL)
        {
            try
            {
                return comandoSQL.Consultar();
            }
            catch (SqlException sqlException)
            {
                var businessLogicException = new BusinessLogicException(sqlException);
                throw new BusinessLogicException(sqlException.Message,
                    BusinessLogicExceptionCode.ERROR_AL_REALIZAR_CONSULTA, sqlException);
            }
        }
    }
}