﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Excepción;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using BusinessLogic.Validación;
using Database;

namespace BusinessLogic.DataAccess
{
    public class OrganizaciónDAO : BaseDAO, IOrganizaciónDAO
    {
        public OrganizaciónDAO()
        {
        }

        public OrganizaciónDAO(TransacciónSQL transacciónSql) : base(transacciónSql)
        {
        }

        public int AgregarOrganización(Organización organización)
        {
            organización.Validar("Error(es) al validar organización: ",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ORGANIZACION);

            var comandoSQL = CrearComandoSQL(
                "INSERT INTO Organizacion (NombreOrganizacion, Ciudad, DireccionOrganizacion, Email, EntidadFederativa, Sector, Telefono, Estatus)" +
                "VALUES(@NombreOrganizacion, @Ciudad, @DireccionOrganizacion, @Email, @EntidadFederativa, @Sector, @Telefono, @Estatus);");

            comandoSQL.AgregarParámetro("@NombreOrganizacion", organización.NombreOrganización);
            comandoSQL.AgregarParámetro("@Ciudad", organización.Ciudad);
            comandoSQL.AgregarParámetro("@DireccionOrganizacion", organización.DirecciónOrganización);
            comandoSQL.AgregarParámetro("@Email", organización.Email);
            comandoSQL.AgregarParámetro("@EntidadFederativa", organización.EntidadFederativa);
            comandoSQL.AgregarParámetro("@Sector", organización.Sector);
            comandoSQL.AgregarParámetro("@Telefono", organización.Teléfono);
            comandoSQL.AgregarParámetro("@Estatus", organización.Estatus.ToString());

            int result;
            try
            {
                result = EjecutarComando(comandoSQL);
            }
            catch (BusinessLogicException businessLogicException)
            {
                var code = businessLogicException.GetCode();
                throw new BusinessLogicException(
                    "Hubo un error al realizar la conexión a la base de datos. [" + code + "]",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_ORGANIZACION);
            }

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo agregar la organización.",
                    BusinessLogicExceptionCode.FALLO_AL_AGREGAR_ORGANIZACION);
            }

            var idOrganizacion = comandoSQL.GetÚltimoIdInsertado();

            return idOrganizacion;
        }

        public void CambiarEstatus(int idOrganización, Estatus estatus)
        {
            var comandoSQL =
                CrearComandoSQL(
                    "UPDATE Organizacion SET Estatus = @Estatus WHERE IdOrganizacion = @IdOrganizacion");
            comandoSQL.AgregarParámetro("@Estatus", estatus.ToString());
            comandoSQL.AgregarParámetro("@IdOrganizacion", idOrganización);

            var result = EjecutarComando(comandoSQL);
            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo cambiar el estatus del encargado.",
                    BusinessLogicExceptionCode.FALLO_AL_CAMBIAR_ESTATUS_ORGANIZACIÓN);
            }
        }

        public void EditarOrganización(Organización organización)
        {
            organización.Validar("Error(es) al validar organización: ",
                BusinessLogicExceptionCode.ERROR_AL_VALIDAR_ORGANIZACION);

            var comandoSQL = CrearComandoSQL("UPDATE Organizacion " +
                                             "SET  NombreOrganizacion = @NombreOrganizacion, Ciudad = @Ciudad, DireccionOrganizacion = @DireccionOrganizacion, Email = @Email, " +
                                             "EntidadFederativa = @EntidadFederativa, Sector = @Sector, Telefono = @Telefono, Estatus = @Estatus " +
                                             "WHERE idOrganizacion = @idOrganizacion;");

            comandoSQL.AgregarParámetro("@NombreOrganizacion", organización.NombreOrganización);
            comandoSQL.AgregarParámetro("@Ciudad", organización.Ciudad);
            comandoSQL.AgregarParámetro("@DireccionOrganizacion", organización.DirecciónOrganización);
            comandoSQL.AgregarParámetro("@Email", organización.Email);
            comandoSQL.AgregarParámetro("@EntidadFederativa", organización.EntidadFederativa.ToString());
            comandoSQL.AgregarParámetro("@Sector", organización.Sector);
            comandoSQL.AgregarParámetro("@Telefono", organización.Teléfono);
            comandoSQL.AgregarParámetro("@Estatus", organización.Estatus.ToString());
            comandoSQL.AgregarParámetro("@idOrganizacion", organización.IdOrganización);

            var result = EjecutarComando(comandoSQL);

            if (result == 0)
            {
                throw new BusinessLogicException("No se pudo editar la organización.",
                    BusinessLogicExceptionCode.FALLO_AL_EDITAR_ORGANIZACION);
            }
        }

        public List<Organización> GetOrganizaciones()
        {
            var organizaciones = new List<Organización>();

            var comandoSQL = CrearComandoSQL(
                "SELECT OG.IdOrganizacion, " +
                "OG.NombreOrganizacion, " +
                "OG.Ciudad, " +
                "OG.DireccionOrganizacion, " +
                "OG.Email, " +
                "OG.EntidadFederativa, " +
                "OG.Sector, " +
                "OG.Telefono, " +
                "OG.Estatus " +
                "FROM Organizacion AS OG ; "
            );

            var dataTable = RealizarConsulta(comandoSQL);

            foreach (DataRow row in dataTable.Rows)
            {
                var organizacion = new Organización
                {
                    IdOrganización = row.Field<int>("IdOrganizacion"),
                    NombreOrganización = row.Field<string>("NombreOrganizacion"),
                    Ciudad = row.Field<string>("Ciudad"),
                    DirecciónOrganización = row.Field<string>("DireccionOrganizacion"),
                    Email = row.Field<string>("Email"),
                    EntidadFederativa = (EntidadFederativa) Enum.Parse(typeof(EntidadFederativa),
                        row.Field<string>("EntidadFederativa")),
                    Sector = row.Field<string>("Sector"),
                    Teléfono = row.Field<string>("Telefono"),
                    Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus"))
                };
                organizaciones.Add(organizacion);
            }

            return organizaciones;
        }

        public Organización GetOrganizaciónPorId(int idOrganización)
        {
            var comandoSQL = CrearComandoSQL("SELECT TOP 1 OG.idOrganizacion, " +
                                             "OG.NombreOrganizacion, " +
                                             "OG.Ciudad, " +
                                             "OG.DireccionOrganizacion, " +
                                             "OG.Email, " +
                                             "OG.EntidadFederativa, " +
                                             "OG.Sector, " +
                                             "OG.Telefono," +
                                             "OG.Estatus " +
                                             "FROM Organizacion AS OG ; ");

            comandoSQL.AgregarParámetro("@IdOrganizacion", idOrganización);

            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count <= 0)
            {
                throw new BusinessLogicException("No existe una Organización con el ID de Organización proporcionado.",
                    BusinessLogicExceptionCode.NO_EXISTE_ORGANIZACION_CON_ID_ORGANIZACION);
            }

            var row = dataTable.Rows[0];

            var organizacion = new Organización
            {
                IdOrganización = row.Field<int>("IdOrganizacion"),
                NombreOrganización = row.Field<string>("NombreOrganizacion"),
                Ciudad = row.Field<string>("Ciudad"),
                DirecciónOrganización = row.Field<string>("DireccionOrganizacion"),
                Email = row.Field<string>("Email"),
                EntidadFederativa =
                    (EntidadFederativa) Enum.Parse(typeof(EntidadFederativa), row.Field<string>("EntidadFederativa")),
                Sector = row.Field<string>("Sector"),
                Teléfono = row.Field<string>("Telefono"),
                Estatus = (Estatus) Enum.Parse(typeof(Estatus), row.Field<string>("Estatus"))
            };

            return organizacion;
        }

        public bool HayAlMenosUnaOrganización()
        {
            var comandoSQL = CrearComandoSQL("SELECT COUNT(*) AS Cantidad FROM Organizacion WHERE Estatus = 'Activo';");
            var dataTable = RealizarConsulta(comandoSQL);

            if (dataTable.Rows.Count < 1)
            {
                return false;
            }

            var row = dataTable.Rows[0];

            var count = row.Field<int>("Cantidad");

            return count > 0;
        }
    }
}